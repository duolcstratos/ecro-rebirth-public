// Copyright (c) Adelays - Licensed under GNU GPL
// adelays@ragnawork.com
// http://ragnawork.com

#define HERCULES_CORE

#include "adelays.h"

#include "map/battle.h"
#include "map/map.h"
#include "map/pc.h"
#include "map/skill.h"
#include "common/cbasetypes.h"
#include "common/db.h"
#include "common/memmgr.h"
#include "common/nullpo.h"
#include "common/showmsg.h"
#include "common/strlib.h"
#include "common/timer.h"
#include "common/utils.h"

#include <math.h>
#include <string.h>

#ifdef ADELAYS
struct adelays_job_delay {
	int class_id;
	int option;
	int sex;
	float per;
};

struct adelays_skill_db {
	int adelays_delay150;
	int adelays_delay190;
	int adelays_harddelay150;
	int adelays_harddelay190;
	int adelays_force_harddelay;
	int adelays_harddelay_chain_max;
	int adelays_harddelay_is_double;
	int adelays_option;
	int adelays_allow_hitlock;
	int adelays_ignoredelay;
};

struct adelays_item_db {
	int nameid;
	int option;
};

struct adelays_item_db adelays_item_db[100];
struct adelays_job_delay adelays_job_delays[100];
struct adelays_skill_db adelays_skill_db[MAX_SKILL_DB];

struct adelays_config adelays_config;

const struct adelays_data {
	const char *str;
	int *val;
	int defval;
	int min;
	int max;
} adelays_data[] = {
	{ "adelays_tol", &adelays_config.adelays_tol, 100, 1, INT_MAX, },
	{ "adelays_adelays_hitlock_time_tolerance", &adelays_config.adelays_hitlock_time_tolerance, 200, 0, INT_MAX, },

	{ "adelays_fixeddelay_tol", &adelays_config.adelays_fixeddelay_tol, 30, 0, INT_MAX, },

	{ "adelays_incr", &adelays_config.adelays_incr, 1, 0, INT_MAX, },
	{ "adelays_hitlock_incr", &adelays_config.adelays_hitlock_incr, 1, 0, INT_MAX, },
	{ "adelays_max_hitlock_count", &adelays_config.adelays_max_hitlock_count, 3, 0, INT_MAX, },

	{ "adelays_reset_time", &adelays_config.adelays_reset_time, 1000000, 0, INT_MAX, },
	{ "adelays_reduce_hitlock_on_correct", &adelays_config.adelays_reduce_hitlock_on_correct, 1, 0, INT_MAX, },
	{ "adelays_reduce_hitlock_on_hitlock", &adelays_config.adelays_reduce_hitlock_on_hitlock, 1, 0, INT_MAX, },
	{ "adelays_enable", &adelays_config.adelays_enable, 1, 0, INT_MAX, },
	{ "adelays_reduce_hackcount_on_correct", &adelays_config.adelays_reduce_hackcount_on_correct, 0, 0, INT_MAX, },
	{ "adelays_reduce_hackcount_on_hitlock", &adelays_config.adelays_reduce_hackcount_on_hitlock, 1, 0, INT_MAX, },
	{ "adelays_buffer_time", &adelays_config.adelays_buffer_time, 50, 0, INT_MAX, },
	{ "adelays_buffer_size", &adelays_config.adelays_buffer_size, 2, 0, INT_MAX, },
	{ "adelays_max_infixed", &adelays_config.adelays_max_infixed, 20, 0, INT_MAX, },
	{ "adelays_scroll_fix", &adelays_config.adelays_scroll_fix, 1, 0, INT_MAX, },
	{ "adelays_jauge_kept_multiplicator_on_new_skill_without_delay", &adelays_config.adelays_jauge_kept_multiplicator_on_new_skill_without_delay, 4, 0, INT_MAX, },
	{ "adelays_jauge_kept_multiplicator", &adelays_config.adelays_jauge_kept_multiplicator, 3, 0, INT_MAX, },
	{ "adelays_fix_delay_multiplicator", &adelays_config.adelays_fix_delay_multiplicator, 5, 0, INT_MAX, },
	{ "adelays_jauge_augmentation_multiplicator", &adelays_config.adelays_jauge_augmentation_multiplicator, 3, 0, INT_MAX, },
	{ "adelays_jauge_to_decrease_multiplicator", &adelays_config.adelays_jauge_to_decrease_multiplicator, 2, 0, INT_MAX, },

	{ "adelays_max_hackcount_to_get_jauge", &adelays_config.adelays_max_hackcount_to_get_jauge, 1, 0, INT_MAX, },
	{ "adelays_acceptable_multiplicator_for_jauge", &adelays_config.adelays_acceptable_multiplicator_for_jauge, 2, 0, INT_MAX, },
	{ "adelays_delay_multiplicator_to_reduce_hackcount", &adelays_config.adelays_delay_multiplicator_to_reduce_hackcount, 3, 0, INT_MAX, },
	{ "adelays_max_infixed_to_get_jauge", &adelays_config.adelays_max_infixed_to_get_jauge, 1, 0, INT_MAX, },
	{ "adelays_allow_buffer_on_fixed", &adelays_config.adelays_allow_buffer_on_fixed, 0, 0, INT_MAX, },
	{ "adelays_max_aspd", &adelays_config.adelays_max_aspd, 190, 0, INT_MAX, },
	{ "adelays_jauge_added_after_cutanim", &adelays_config.adelays_jauge_added_after_cutanim, 20, 0, INT_MAX, },
	{ "adelays_min_jauge_multiplicator", &adelays_config.adelays_min_jauge_multiplicator, 4, 0, INT_MAX, },
	{ "adelays_allow_harddelays_on_doubles_skills", &adelays_config.adelays_allow_harddelays, 1, 0, INT_MAX, },
	{ "adelays_put_hard_limit_on_all_skills", &adelays_config.adelays_put_hard_limit_on_all_skills, 1, 0, INT_MAX, },
	{ "adelays_hard_chain_limit", &adelays_config.adelays_hard_chain_limit, 4, 0, INT_MAX, },
	{ "adelays_sit_delay_bodyreloc", &adelays_config.adelays_sit_delay_bodyreloc, 2000, 0, INT_MAX, },
};

static int adelays_set_value(const char *w1, const char *w2) {
	int val = config_switch(w2);

	int i;
	ARR_FIND(0, ARRAYLENGTH(adelays_data), i, strcmpi(w1, adelays_data[i].str) == 0);
	if (i == ARRAYLENGTH(adelays_data))
		return 0; // not found

	if (val < adelays_data[i].min || val > adelays_data[i].max) {
		ShowWarning("Value for setting '%s': %s is invalid (min:%i max:%i)! Defaulting to %i...\n", w1, w2, adelays_data[i].min, adelays_data[i].max, adelays_data[i].defval);
		val = adelays_data[i].defval;
	}

	*adelays_data[i].val = val;
	return 1;
}

int adelays_init(void) {
	adelays_battle_config_read();
	adelays_read_db();

	return 0;
}

int adelays_battle_config_read(void) {
	char line[1024], w1[1024], w2[1024];
	FILE *fp;

	adelays_set_defaults();

	fp = fopen("conf/map/battle/adelays.conf", "r");
	if (fp == NULL) {
		ShowError("File not found: %s\n", "conf/map/battle/adelays.conf");
	} else {
		while (fgets(line, sizeof(line), fp)) {
			if (line[0] == '/' && line[1] == '/')
				continue;
			if (sscanf(line, "%1023[^:]:%1023s", w1, w2) != 2)
				continue;
			if (adelays_set_value(w1, w2) == 0)
				ShowWarning("Unknown setting '%s' in file %s\n", w1, "conf/map/battle/adelays.conf");
		}
		fclose(fp);
	}
	return 0;
}

void adelays_set_defaults(void) {
	int i;
	for (i = 0; i < ARRAYLENGTH(adelays_data); i++)
		*adelays_data[i].val = adelays_data[i].defval;
}

float adelays_get_version(void) {
	return 4.3f;
}

void adelays_set_1hitlock(struct map_session_data *sd) {
	sd->adelays_state.hitlock = max(1, sd->adelays_state.hitlock);
}

void adelays_skill_failed(struct map_session_data *sd) {
	sd->adelays_state.last_skill_failed = 1;
}

void adelays_set_hitlock(struct map_session_data *sd) {
	sd->adelays_state.hitlock += adelays_config.adelays_hitlock_incr;
	sd->adelays_state.hitlock = min(adelays_config.adelays_max_hitlock_count, sd->adelays_state.hitlock);
}

void adelays_set_hitlock_until(struct map_session_data *sd, int64 tick) {
	sd->adelays_state.hitlock_until = tick + adelays_config.adelays_hitlock_time_tolerance;
}

int adelays_can_ignore_delay_on_skill(int id) {
	if (!adelays_get_ignore_delay(id))
		return 0;
	return 1;
}

int adelays_get_ignore_delay(int id) {
	return adelays_skill_db[skill->get_index(id)].adelays_ignoredelay;
}

int adelays_can_hard_delay_on_skill(int id) {
	if (!adelays_config.adelays_allow_harddelays)
		return 0;
	if (!adelays_get_force_harddelay(id))
		return 0;
	return 1;
}

int adelays_get_harddelay150(int id) {
	return adelays_skill_db[skill->get_index(id)].adelays_harddelay150;
}

int adelays_get_harddelay190(int id) {
	return adelays_skill_db[skill->get_index(id)].adelays_harddelay190;
}

int adelays_get_force_harddelay(int id) {
	return adelays_skill_db[skill->get_index(id)].adelays_force_harddelay;
}

int adelays_get_hard_delay_chain_count(int id) {
	return adelays_skill_db[skill->get_index(id)].adelays_harddelay_chain_max;
}

int adelays_get_delay150(int id) {
	return adelays_skill_db[skill->get_index(id)].adelays_delay150;
}

int adelays_get_delay190(int id) {
	return adelays_skill_db[skill->get_index(id)].adelays_delay190;
}

int adelays_get_option(int id) {
	return adelays_skill_db[skill->get_index(id)].adelays_option;
}

int adelays_get_allow_hitlock(int id) {
	return adelays_skill_db[skill->get_index(id)].adelays_allow_hitlock;
}

int adelays_read_db(void) {

	ShowStatus("             _      _                   _  _   _  _   \n");
	ShowStatus("    /\\      | |    | |                 | || | | || |  \n");
	ShowStatus("   /  \\   __| | ___| | __ _ _   _ ___  | || |_| || |_ \n");
	ShowStatus("  / /\\ \\ / _` |/ _ \\ |/ _` | | | / __| |__   _|__   _|\n");
	ShowStatus(" / ____ \\ (_| |  __/ | (_| | |_| \\__ \\    | |_   | |  \n");
	ShowStatus("/_/    \\_\\__,_|\\___|_|\\__,_|\\__, |___/    |_(_)  |_|  \n");
	ShowStatus("                             __/ |                    \n");
	ShowStatus("                            |___/                     \n");
	ShowStatus("\n");
	ShowStatus("\n");
	ShowStatus("<Adelays> : http://adelays.net \n");
	ShowStatus("<Adelays> : .....Loading..... \n");
	ShowStatus("<Adelays> : State of Adelays : %d \n", adelays_config.adelays_enable);

	char line[1024], skill_name[200];
	int delay_at150, delay_at190, option, doubles, count = 0, class_id, per, sex, nameid, itemoption, allow_hitlock, chain_max;
	memset(adelays_skill_db, 0, sizeof(struct adelays_skill_db) * MAX_SKILL_DB);
	memset(adelays_job_delays, 0, sizeof(struct adelays_job_delay) * 100);
	memset(adelays_item_db, 0, sizeof(struct adelays_item_db) * 100);

	// Read skills.
	FILE *file = fopen("db/adelays_skill_delays.txt", "r");
	count = 0;

	while (fgets(line, sizeof(line), file)) {

		if (line[0] == '/' && line[1] == '/')
			continue;

		if (sscanf(line, "%23[^,],%d,%d,%d,%d", skill_name, &delay_at150, &delay_at190, &option, &allow_hitlock) < 5)
			continue;

		if (adelays_setskill_delays(skill_name, delay_at150, delay_at190, option, allow_hitlock) == 0) {
			ShowInfo("<Adelays> : Ignored line in file adelays_skill_delays.txt (%s). \n", skill_name);
		} else {
			if (adelays_config.adelays_put_hard_limit_on_all_skills == 1) {
				adelays_set_skill_hard_delays(skill_name, delay_at150 * adelays_config.adelays_hard_chain_limit, delay_at190 * adelays_config.adelays_hard_chain_limit, adelays_config.adelays_hard_chain_limit, 0);
			}
			count++;
		}
	}

	fclose(file);

	ShowStatus("<Adelays> : Adelays Skill Delays Configuration Ready For %d Skills.\n", count);
	// Read skills hard delays.

	file = fopen("db/adelays_hard_delays.txt", "r");
	count = 0;
	while (fgets(line, sizeof(line), file)) {

		if (line[0] == '/' && line[1] == '/')
			continue;

		if (sscanf(line, "%23[^,],%d,%d,%d,%d", skill_name, &delay_at150, &delay_at190, &chain_max, &doubles) < 5)
			continue;

		if (adelays_set_skill_hard_delays(skill_name, delay_at150, delay_at190, chain_max, doubles) == 0) {
			ShowInfo("<Adelays> : Ignored line in file adelays_skills_hard_delays.txt (%s). \n", skill_name);
		} else {
			count++;
		}
	}

	ShowStatus("<Adelays> : Adelays Hard Skill Delays Configuration Ready For %d Skills.\n", count);
	// Read items delays.

	file = fopen("db/adelays_item_delays.txt", "r");
	count = 0;

	while (fgets(line, sizeof(line), file)) {
		if (line[0] == '/' && line[1] == '/')
			continue;

		if (sscanf(line, "%d,%d", &nameid, &itemoption) < 2)
			continue;
		if (adelays_set_item_delays(count, nameid, itemoption) == 0) {
			ShowInfo("<Adelays> : Ignored line in file adelays_item_delays.txt (%d). \n", nameid);
		} else {
			count++;
		}
	}

	ShowStatus("<Adelays> : Adelays Items Delays Configuration Ready For %d Items.\n", count);

	file = fopen("db/adelays_job_delays.txt", "r");
	count = 0;

	while (fgets(line, sizeof(line), file)) {

		if (line[0] == '/' && line[1] == '/')
			continue;

		if (sscanf(line, "%d,%d,%d,%d", &option, &class_id, &sex, &per) < 4)
			continue;

		adelays_set_job_delay(count, option, class_id, sex, per);
		count++;
	}

	ShowStatus("<Adelays> : Adelays Jobs Adjustements Configuration Read For %d Jobs. \n", count);

	file = fopen("db/adelays_ignore_delays.txt", "r");
	count = 0;

	while (fgets(line, sizeof(line), file)) {
		if (line[0] == '/' && line[1] == '/')
			continue;

		if (sscanf(line, "%23s\r\n", skill_name) < 1)
			continue;

		adelays_set_skill_ignore_delay(skill_name);
		count++;
	}

	ShowStatus("<Adelays> : Adelays Ignored After-Cast Delays Configuration Read For %d Skills. \n", count);

	ShowStatus("<Adelays> : Thank you for Using Adelays.\n");
	return 0;
}

int adelays_set_skill_ignore_delay(char* skill_name) {
	int id = skill->name2id(skill_name);
	int idx = skill->get_index(id);
	if (!id || idx < 0)
		return 0;

	adelays_skill_db[idx].adelays_ignoredelay = 1;
	return 1;
}

int adelays_setskill_delays(char *skill_name, int delay_at150, int delay_at190, int option, int allow_hitlock) {
	int id = skill->name2id(skill_name);
	int idx = skill->get_index(id);
	if (!id || idx < 0)
		return 0;

	adelays_skill_db[idx].adelays_delay150 = delay_at150;
	adelays_skill_db[idx].adelays_delay190 = delay_at190;
	adelays_skill_db[idx].adelays_harddelay150 = delay_at150;
	adelays_skill_db[idx].adelays_harddelay190 = delay_at190;
	adelays_skill_db[idx].adelays_force_harddelay = 0;
	adelays_skill_db[idx].adelays_harddelay_chain_max = 1;
	adelays_skill_db[idx].adelays_option = option;
	adelays_skill_db[idx].adelays_allow_hitlock = allow_hitlock;

	return 1;
}

int adelays_set_skill_hard_delays(char *skill_name, int delay_at150, int delay_at190, int chain_max, int doubles) {
	int id = skill->name2id(skill_name);
	int idx = skill->get_index(id);
	if (!id || idx < 0)
		return 0;

	adelays_skill_db[idx].adelays_harddelay150 = delay_at150;
	adelays_skill_db[idx].adelays_harddelay190 = delay_at190;
	adelays_skill_db[idx].adelays_harddelay_chain_max = chain_max;
	adelays_skill_db[idx].adelays_force_harddelay = 1;
	adelays_skill_db[idx].adelays_harddelay_is_double = doubles;

	return 1;
}

int adelays_set_item_delays(int count, int nameid, int option) {
	if (count >= 100)
		return 0;
	adelays_item_db[count].nameid = nameid;
	adelays_item_db[count].option = option;

	//ShowInfo("Set Item Delays of %s To (%d, %d) \n", nameid,option);

	return 1;
}

int adelays_set_job_delay(int count, int option, int class_id, int sex, int per) {
	if (count >= 100)
		return 0;

	adelays_job_delays[count].class_id = class_id;
	adelays_job_delays[count].option = option;
	adelays_job_delays[count].per = per / 100.0;
	adelays_job_delays[count].sex = sex;

	//ShowInfo("Set Job Delays of %d-%d-%d To %d\% \n", class_id, sex, option, per);

	return 1;
}

int adelays_get_fix_delay(unsigned short amotion, int skillid) {
	int acceptable_animation_delay;
	int max_aspd, min_aspd, at150, at190;
	int option = 1;
	float a, b;
	max_aspd = 190;
	min_aspd = 150;

	// **** Calculate the acceptable Delay
	option = adelays_get_option(skillid);
	if (option > 0) {
		at150 = adelays_get_delay150(skillid);
		at190 = adelays_get_delay190(skillid);

		// Formula calculation (a * x + b)
		a = (at190 - at150) / (max_aspd - min_aspd);
		b = -((a * max_aspd) - at190);

		// Correct delay difference
		acceptable_animation_delay = (a * amotion) + b;
		acceptable_animation_delay += adelays_config.adelays_tol;

	} else {
		// If it can not be calculated, just use the server delay value.
		acceptable_animation_delay = battle_config.min_skill_delay_limit;
	}

	return acceptable_animation_delay + min(150, acceptable_animation_delay / 5);

}

int adelays_can_use_item(int nameid, int64 cannact_tick) {
	if (adelays_config.adelays_scroll_fix < 1)
		return 1;

	int count = 0;
	while ((adelays_item_db[count].nameid != 0) && (count < 100)) {
		if ((adelays_item_db[count].nameid == nameid) && (adelays_item_db[count].option == 1)) {
			if (DIFF_TICK(timer->gettick(), cannact_tick) < 0) {
				return 0;
			}
		}
		count++;
	}

	return 1;
}

void adelays_dataset(struct map_session_data *sd) {
	struct adelays_state *adstate = &sd->adelays_state;
	adstate->skill_tick = timer->gettick();
	adstate->last_skill_id = 0;
	adstate->hackcount = 0;
	adstate->hitlock = 0;
	adstate->hitlock_until = timer->gettick();
	adstate->infixed = 0;
	adstate->showinfo = 0;
	adstate->jauge = 0;
	adstate->showinfo = 0;
	adstate->allowed_reduction = 0;
	adstate->expected_hard_delay = 0;
	adstate->missing_delay = 0;
	adstate->previous_client_delay = 0;
	adstate->last_client_delay = 0;
	adstate->hard_ignore = 0;
}

int adelays_calculate_acceptable_delay(int skill_id, int amotion) {
	int max_aspd, min_aspd, at150, at190;
	float a, b;
	int acceptable_animation_delay;

	max_aspd = 190;
	min_aspd = 150;

	at150 = adelays_get_delay150(skill_id);
	at190 = adelays_get_delay190(skill_id);

	// Formula calculation (a * x + b)
	a = (at190 - at150) / (max_aspd - min_aspd);
	b = -((a * max_aspd) - at190);

	// Correct delay difference
	acceptable_animation_delay = (a * amotion) + b;
	acceptable_animation_delay += adelays_config.adelays_tol;

	return acceptable_animation_delay;
}

int adelays_get_harddelay(int skill_id, int amotion, int skillamount) {

	int max_skill_amount = adelays_get_hard_delay_chain_count(skill_id);
	if (skillamount == 0) {
		skillamount = max_skill_amount;
	}

	float rapport = skillamount / (float)max_skill_amount;

	int max_aspd, min_aspd, at150, at190;
	float a, b;
	int harddelay;

	max_aspd = 190;
	min_aspd = 150;

	at150 = adelays_get_harddelay150(skill_id);
	at190 = adelays_get_harddelay190(skill_id);

	a = (at190 - at150) / (max_aspd - min_aspd);
	b = -((a * max_aspd) - at190);

	// Correct delay difference
	harddelay = (a * amotion) + b;

	if (adelays_skill_has_doubles(skill_id)) {
		// For special skills, ignore the min skill delay.
		return harddelay * rapport;
	}
	// For regular skills, add the min skill delay.
	return (harddelay * rapport) + battle_config.min_skill_delay_limit * skillamount;
}

void adelays_calculate_missing_delay(int skillid, struct map_session_data *sd) {
	struct adelays_state *adstate = &sd->adelays_state;

	// Calculate the missing delay for the usage of a specified skill.
	// We know the count of usage of that skil, and the accumulated delay for that skill.

	if (adstate->chain_count == 0) {
		adstate->missing_delay = 0;
		return;
	}

	if (!adelays_can_hard_delay_on_skill(skillid)) {
		adstate->missing_delay = 0;
		return;
	}

	adstate->missing_delay = adelays_get_harddelay(skillid, adstate->last_amotion, adstate->chain_count) - adstate->accumulated_delay;

	if (adstate->missing_delay < 0) {
		adstate->missing_delay = 0;
	}

}

static void adelays_set_expected_first_chain_hard_delay(int skillid, struct map_session_data *sd, int amotion) {
	int chain_doubles = adelays_get_hard_delay_chain_count(skillid) - 1;
	sd->adelays_state.expected_hard_delay = adelays_get_harddelay(skillid, amotion, 0) - ((battle_config.min_skill_delay_limit + 50) * chain_doubles);
}

#if 0
// TODO: This function is unused
static void adelays_set_expected_regular_hard_delay(int skillid, struct map_session_data *sd, int amotion) {
	int chain_count = adelays_get_hard_delay_chain_count(skillid);
	sd->adelays_state.expected_hard_delay = adelays_get_harddelay(skillid, amotion, 0) / chain_count;
}
#endif // 0

static int adelays_apply_class_adjustement(int skill_id, int acceptable_animation_delay, short class_, unsigned char sex, unsigned int is_riding) {
	int count = 0;
	int option = adelays_get_option(skill_id);

	// **** Class Adjustements
	count = 0;
	while ((adelays_job_delays[count].option != 0) && (count < 100)) {
		if ((adelays_job_delays[count].class_id == class_) && (adelays_job_delays[count].sex == sex) && (option == adelays_job_delays[count].option)) {
			acceptable_animation_delay *= adelays_job_delays[count].per;
			break;
		}
		count++;
	}

	// **** Peco adjustements (Only for paladins) ----

	switch (class_) {
		// Paladin are faster on magical attack without peco.
	case JOB_PALADIN:
	case JOB_PALADIN2:
		if (!is_riding && (option == 2)) {
			acceptable_animation_delay *= 0.66;
		}
		break;

		// Knight and lord knights are faster on magical attacks with peco.
	case JOB_KNIGHT:
	case JOB_KNIGHT2:
	case JOB_LORD_KNIGHT:
	case JOB_LORD_KNIGHT2:
		if (is_riding && (option == 2)) {
			acceptable_animation_delay *= 0.70;
		}
		break;
	}

	return acceptable_animation_delay;
}

int adelays_skill_has_doubles(int skill_id) {
	return adelays_skill_db[skill_id].adelays_harddelay_is_double == 1;
}

// TODO: This function is unused
int adelays_is_sit_allowed(struct map_session_data *sd) {
	if (sd->adelays_state.last_skill_id == MO_BODYRELOCATION) {
		if (DIFF_TICK(timer->gettick(), sd->adelays_state.skill_tick) < 2000) {
			return 0;
		}
	}

	return 1;
}

int adelays_process_record(int64 tick, int64 *cannact_tick, struct map_session_data *sd) {
	struct adelays_state *adstate = &sd->adelays_state;
	int response = 0;
	int skill_id = adstate->last_skill_id;
	int clientdelay = adstate->last_client_delay;

	// -------------------------
	// Check the fixed delays.
	// -------------------------

	if (adstate->infixed > 0) {
		// Reduce the fixed delays everyskills.
		adstate->infixed -= 1;

		// If there are still fixed delays, set the next skill delay.
		if (adstate->infixed > 0) {
			(*cannact_tick) = max((*cannact_tick), tick + adelays_get_fix_delay(adstate->last_amotion, adstate->previous_skill_id) * adstate->infixed);
		}
	} else {
		// Place the player in fixed mode if he abuses.
		if ((adstate->hackcount > 0) && (adstate->jauge == 0)) {
			adstate->infixed = min(adelays_config.adelays_max_infixed, adstate->hackcount);

			// Set the next delay to fixed one.
			(*cannact_tick) = max((*cannact_tick), tick + adelays_get_fix_delay(adstate->last_amotion, adstate->previous_skill_id) * adstate->infixed);
		}
	}

	// -------------------------
	// Check the hard delays.
	// -------------------------

	// Apply the hard delay.
	adstate->last_hard = 0;

	if (!adstate->hard_ignore) {
		if (adelays_can_hard_delay_on_skill(skill_id)) {
			int full_hard_delay = adelays_get_harddelay(skill_id, adstate->last_amotion, 0);
			int missing_delay = 0;

			// Set the accumulated delay.

			if ((skill_id != adstate->previous_skill_id) || (clientdelay > full_hard_delay)) {
				// If the skill is different or that there was a pause bigger than the full hard delay:
				adstate->chain_count = 1;

				if (adelays_skill_has_doubles(skill_id)) {
					// Set the accumulated delay to a big value comprised in the hard delay. The chain still begins..
					//adstate->accumulated_delay = (full_hard_delay / (adelays_get_hard_delay_chain_count(skill_id)/2)) - battle_config.min_skill_delay_limit;
					adelays_get_harddelay(skill_id, adstate->last_amotion, 3);
				} else {
					// Set the accumulated delay to the normal value for 1 skill..
					adstate->accumulated_delay = adelays_get_harddelay(skill_id, adstate->last_amotion, 1);
				}
				adstate->previous_client_delay = adstate->accumulated_delay;

			} else {
				// The chain of skill is continuing. Increase it and save the accumulated delay.

				// Check if the delay is correct (If we expect a specific delay).
				// Only for specifics skills like AV or SB.
				if (adstate->expected_hard_delay > 0) {
					// Here, we expect a specific delay.
					if (adelays_skill_has_doubles(skill_id) && clientdelay < adstate->expected_hard_delay * 0.50) {
						// Here the delay is way too far from what we expected.
						// We cancel the skill and put an hard delay.
						adstate->chain_count--;
						strcpy(adstate->last_result, "CANCELLED");
						missing_delay = (adstate->expected_hard_delay - clientdelay) + (adstate->expected_hard_delay - (clientdelay * 2)) * 0.2;
						(*cannact_tick) = max((*cannact_tick) + missing_delay, tick + missing_delay);
						adstate->last_hard = missing_delay;
						response = 2;
					} else if (clientdelay < adstate->expected_hard_delay) {
						adstate->chain_count--;
						strcpy(adstate->last_result, "CANCELLED");
						missing_delay = (adstate->expected_hard_delay - clientdelay) + (adstate->expected_hard_delay - clientdelay) * 0.2;
						(*cannact_tick) = max((*cannact_tick) + missing_delay, tick + missing_delay);
						adstate->last_hard = missing_delay;
						response = 2;
					}
				}

				adstate->chain_count++;
				adstate->accumulated_delay += clientdelay;
				adstate->previous_client_delay = clientdelay;
			}

			// Calculate the missing delay.
			adelays_calculate_missing_delay(skill_id, sd);

			// Calculate the next expected hard delay.
			adstate->expected_hard_delay = 0;

			if (adelays_time_to_hard(sd, skill_id)) {
				// If the chain count reached the maximum, we add the missing delay as an hard delay, and setup the expected next delay.
				missing_delay = adstate->missing_delay + adstate->missing_delay * 0.33;

				// This is only for display
				adstate->last_hard = missing_delay;
				// -----------

				// We increase the canact tick with the missing delay.
				(*cannact_tick) = max((*cannact_tick) + missing_delay, tick + missing_delay);

				// We reset the adelays values.
				adstate->chain_count = 0;
				adstate->accumulated_delay = 0;

				// Then we set the first chain delay.
				if (adelays_skill_has_doubles(skill_id)) {
					adelays_set_expected_first_chain_hard_delay(skill_id, sd, adstate->last_amotion);
				} else {
#if 0
					if (missing_delay > 0) {
						adstate->expected_hard_delay = missing_delay + adelays_get_harddelay(skill_id, adstate->last_amotion, 1);
					}
#endif // 0
				}
			}
		}
	}

	// -------------------------
	// File Log System.
	// -------------------------
	if (adstate->log == 1) {
		// Save infos in a log. file.
		FILE *fp;
		time_t rawtime;
		time(&rawtime);
		char *timestr = asctime(localtime(&rawtime));
		timestr[strlen(timestr) - 1] = 0;

		fp = fopen("conf/adelays_log", "a+");
		fprintf(fp, "[%s] %s : SD:%04d CD:%04d HTC:%d HKC:%02d BUF:%02d DEX:%d HD:%04dms HDC:%d [%s] %s \n",
			timestr,
			sd->status.name,
			adstate->last_server_delay,
			adstate->last_client_delay,
			adstate->hitlock,
			adstate->hackcount,
			adstate->jauge,
			adstate->last_amotion,
			adstate->last_hard,
			adstate->chain_count,
			skill->get_name(adstate->last_skill_id),
			adstate->last_result);
		fclose(fp);

		// Also show info in console.
		ShowInfo("AD:%04d SD:%04d CD:%04d HTC:%d HKC:%02d BUF:%02d ASP:%d HD:%04dms HDC:%d [%s] %c%c \n",
			adstate->last_acceptable_delay,
			adstate->last_server_delay,
			adstate->last_client_delay,
			adstate->hitlock,
			adstate->hackcount,
			adstate->jauge,
			adstate->last_amotion,
			adstate->last_hard,
			adstate->chain_count,
			skill->get_name(adstate->last_skill_id),
			adstate->last_result[0],
			adstate->last_result[1]);
	}

	return response;

}

int adelays_get_log_message(char *message, struct map_session_data *sd) {
	struct adelays_state *adstate = &sd->adelays_state;

	sprintf(message, "SD:%04d CD:%04d HT:%d HK:%02d BUF:%02d ASPD:%03d HD:%04dms HDC:%d [%s] %s",
		adstate->last_server_delay,
		adstate->last_client_delay,
		adstate->hitlock,
		adstate->hackcount,
		adstate->jauge,
		adstate->last_amotion,
		adstate->last_hard,
		adstate->chain_count,
		skill->get_name(adstate->last_skill_id),
		adstate->last_result);

	return 0;
}

int adelays_record_skill_delay(int skillid, int delayfix, int cooldown, struct map_session_data *sd, int64 tick) {
	struct adelays_state *adstate = &sd->adelays_state;
	int client_total_delay, server_totaldelay, acceptable_animation_delay, next_acceptable_animation_delay;
	int option = 1, oldskilloption = 1, amotion = 0, diff;
	int allow_hitlock = 1;
	int t;
	int oldskill_id;

	// Adelays is disabled. Return default data.
	if (adelays_config.adelays_enable != 1) {
		return 1;
	}

	// Make sure the last skill ID is set to a normal value.
	if (adstate->last_skill_id == 0) {
		adstate->last_skill_id = skillid;
	}

	// Keep a trace of the previous skill.
	oldskill_id = adstate->last_skill_id;

	// Ignores after-cast delay.
	if (adelays_can_ignore_delay_on_skill(skillid)) {
		amotion = cap_value((2000 - sd->battle_status.amotion) / 10, 150, adelays_config.adelays_max_aspd);
		adstate->last_amotion = amotion;
		adstate->last_acceptable_delay = 0;
		adstate->last_client_delay = 0;
		adstate->last_server_delay = 0;
		adstate->previous_skill_id = oldskill_id;
		adstate->last_skill_id = skillid;
		adstate->skill_tick = tick;
		adstate->hackcount = 0;
		adstate->infixed = 0;
		adstate->hitlock = 0;
		adstate->jauge = 0;
		adstate->hard_ignore = 0;
		return 1;
	}

	// Calculate Total Server Delay
	server_totaldelay = max(delayfix, cooldown);

	// Calculate Total Client Delay
	client_total_delay = DIFF_TICK32(tick, adstate->skill_tick);

	// Make sure delays can not be negative.
	if ((client_total_delay < 0) || (server_totaldelay < 0)) {
		// Impossible case, but who knows.
		return 1;
	}

	// Get the normal ASPD of the player
	amotion = cap_value((2000 - sd->battle_status.amotion) / 10, 150, adelays_config.adelays_max_aspd);
	adstate->last_amotion = amotion;


	// Reset fixed and hack count in case of big pause
	if (client_total_delay >= adelays_config.adelays_reset_time) {
		adstate->hackcount = 0;
		adstate->infixed = 0;
		adstate->hitlock = 0;
		adstate->jauge = 0;
	}

	// Also reset hitlock in case of big pause.
	if (client_total_delay >= 5000) {
		if (adstate->hitlock > 1) {
			adstate->hitlock = 1;
		}
	}

	// ---------- V 3.2 Detection System -------------

	//By default, assume the skill is correct.
	strcpy(adstate->last_result, "CORRECT");

	// If we are in fixed mode, it's fixed by default.
	if (adstate->infixed > 0) {
		strcpy(adstate->last_result, "FIXED");
	}

	// Calculate the acceptable Delay
	oldskilloption = adelays_get_option(oldskill_id);
	option = adelays_get_option(skillid);
	allow_hitlock = adelays_get_allow_hitlock(skillid);

	// If both skills have delays then calculate the delay between them.
	if ((option > 0) && (oldskilloption > 0)) {
		acceptable_animation_delay = adelays_calculate_acceptable_delay(oldskill_id, amotion);
		next_acceptable_animation_delay = adelays_calculate_acceptable_delay(skillid, amotion);

		// Reduct the jauge on skill change.
		// If it's a new skill but both skills have delays, keep a little jauge:
		if (oldskill_id != skillid) {
			// Make sure the jauge is not too big for the new skill..
			if (adstate->jauge > (next_acceptable_animation_delay * adelays_config.adelays_buffer_size))
				adstate->jauge = next_acceptable_animation_delay * adelays_config.adelays_buffer_size;
		}
	} else {
		// Here, the previous or current skill is a nodelay skill. (That only work if the current skill allow cutanim).

		// Reduct the jauge on skill change.
		// If it's a new skill but one doesn't have delay. Reduce it.
		if (oldskill_id != skillid) {

			adstate->jauge = adstate->jauge / adelays_config.adelays_jauge_kept_multiplicator_on_new_skill_without_delay;
		}
		// If the acceptable delay can not be calculated, just use the server delay value.
		acceptable_animation_delay = 0;

		if ((oldskilloption == 0) && option > 0) {
			// If only the previous skill is nodelay, then it has cut the anim of the current skill.
			strcpy(adstate->last_result, "CUTANIM");

			// In that case, we add a little jauge in case, because it can causes hacks sometimes.
			if (adstate->last_skill_id != skillid) {
				adstate->jauge = adelays_config.adelays_jauge_added_after_cutanim;
			}
		} else {
			// This is a nodelay skill. Just write it for information.
			strcpy(adstate->last_result, "NODELAY");
		}
	}

	// Apply the class adjustements.
	acceptable_animation_delay = adelays_apply_class_adjustement(skillid, acceptable_animation_delay, sd->status.class, sd->status.sex, sd->sc.option & OPTION_RIDING);

	// Store the acceptable delay.
	adstate->last_acceptable_delay = acceptable_animation_delay;
	adstate->last_client_delay = client_total_delay;
	adstate->last_server_delay = server_totaldelay;

	// Prepare Tick for next round.
	adstate->previous_skill_id = oldskill_id;
	adstate->last_skill_id = skillid;
	adstate->skill_tick = tick;

	// Do not ignore hard delays by default.
	adstate->hard_ignore = 0;
	diff = acceptable_animation_delay - client_total_delay;

	// Check if the delay is correct or not
	if ((option > 0)
	 && (oldskilloption > 0)
	 && (server_totaldelay < acceptable_animation_delay)
	 && (client_total_delay < acceptable_animation_delay)
	) {
		// Here the delay is not correct.
		if (adstate->last_skill_failed > 0) {
			strcpy(adstate->last_result, "FAIL");
			adstate->hard_ignore = 1;
		} else if ((adstate->jauge > 0) && (adstate->jauge >= (diff / adelays_config.adelays_jauge_augmentation_multiplicator))) { // We try to augment it with the jauge.
			// Reduction Worked.

			// Depending on the difference, we should remove more or less.
			if (diff < 10) {
				diff *= 4;
			} else if (diff < acceptable_animation_delay / 10) {
				diff *= 6;
			} else if (diff < acceptable_animation_delay / 8) {
				diff *= 3;
			} else if (diff < acceptable_animation_delay / 6) {
				diff *= 2;
			} else if (diff < acceptable_animation_delay / 4) {
				// No change here.
			} else {
				diff *= 0.5;
			}

			adstate->jauge = max(0, adstate->jauge - (diff / adelays_config.adelays_jauge_to_decrease_multiplicator));

			//client_total_delay = acceptable_animation_delay;
			strcpy(adstate->last_result, "BUFFER");

			// In case its a hitlock, just specify it. saying BUFFER is not really informative.
			if ((adstate->hitlock > 0) && allow_hitlock) {
				strcpy(adstate->last_result, "HITLK/DCING");
			}
		} else {
			// Augmentation Didn't Work.
			adstate->jauge = 0;

			// Let's check if there could be an hitlock (if the skill allow that).
			if ((adstate->hitlock > 0 || DIFF_TICK(tick, adstate->hitlock_until) < 0) && allow_hitlock) {

				// Allow reduction of hackcount if hitlock is big.
				if ((adstate->hitlock >= 2) && (adstate->hackcount) >= 1) {
					adstate->hackcount -= 1;
				}

				// **** Here there is an Hitlock.
				if (adstate->hitlock > 0) {
					adstate->hitlock -= adelays_config.adelays_reduce_hitlock_on_hitlock;
					adstate->hitlock = max(0, adstate->hitlock);
				}

				adstate->hard_ignore = 1;
				strcpy(adstate->last_result, "HITLK/DCING");

				// That can be a skill chain.
				if ((DIFF_TICK(tick, adstate->hitlock_until) < 0)) {
					strcpy(adstate->last_result, "CHAIN");
				}
			} else {
				// **** Here this is not Hitlock... It's an HACK.
				strcpy(adstate->last_result, "HACK");
				adstate->hackcount += adelays_config.adelays_incr;
				adstate->hackcount = min(adstate->hackcount, adelays_config.adelays_max_infixed);
			}

			// If there is was hitlock but that the skill didn't allow it, reduce the hitlock count by 1 too.
			if (!allow_hitlock && adstate->hitlock > 0) {
				adstate->hitlock -= adelays_config.adelays_reduce_hitlock_on_hitlock;
				adstate->hitlock = max(0, adstate->hitlock);
			}
		}
	} else {
		// Here the delay is correct.

		if ((option > 0) && (oldskilloption > 0)) {
			// Reduce Hackcount if it is a verified skill.

			// Only reduce the hackcount it's some kind of a pause between two skills.
			if (adstate->infixed < 1) {
				// Reduce the hackcount depending the time the skill took to be run.
				if (client_total_delay < (acceptable_animation_delay * 3)) {
					adstate->hackcount -= ceil(client_total_delay / max(10, max(server_totaldelay, acceptable_animation_delay)));

				} else {
					adstate->hackcount -= 1;
				}

				// **** Always reduce this value (normally = 0)
				adstate->hackcount -= adelays_config.adelays_reduce_hackcount_on_correct;
			}

			// ** If the hack count is small, put a "jauge".
			// ** We add a jauge because sometimes players cast a skill very fast multiple time.
			// ** It doesn't apply if they are in fixed delays mode.
			// ** This jauge should not b
			if (adstate->hackcount <= adelays_config.adelays_max_hackcount_to_get_jauge && adstate->infixed < adelays_config.adelays_max_infixed_to_get_jauge) {
				if (client_total_delay < acceptable_animation_delay * adelays_config.adelays_acceptable_multiplicator_for_jauge + adelays_config.adelays_buffer_time) {
					// Augment from 1 on every correct skill.
					t = client_total_delay - acceptable_animation_delay;

					if (t > 0) {

						// Make sure the jauge given is at least a interresting value. Avoid hacks when big ping.

						if (t < (acceptable_animation_delay / 10))
							adstate->jauge += max(acceptable_animation_delay / adelays_config.adelays_min_jauge_multiplicator, t);
						else if (t < (acceptable_animation_delay / 5))
							adstate->jauge += max(acceptable_animation_delay / adelays_config.adelays_min_jauge_multiplicator, t * 1.5);
						else if (t < (acceptable_animation_delay / 2))
							adstate->jauge += max(acceptable_animation_delay / adelays_config.adelays_min_jauge_multiplicator, t * 1.8);
						else
							adstate->jauge += max(acceptable_animation_delay / adelays_config.adelays_min_jauge_multiplicator, t * 2);
					}
				} else if (client_total_delay > ((acceptable_animation_delay * adelays_config.adelays_acceptable_multiplicator_for_jauge) + adelays_config.adelays_buffer_time)) {
					// If there was a big pause, reset the jauge to a small value. (Avoid getting stuck in hack at second skill).
					adstate->jauge = acceptable_animation_delay / adelays_config.adelays_jauge_kept_multiplicator;
				}
			} else if ((adstate->infixed >= adelays_config.adelays_max_infixed_to_get_jauge) && adelays_config.adelays_allow_buffer_on_fixed) {
				// Allow a buffer to be given in fixed mode.
				t = max(0, client_total_delay - adelays_get_fix_delay(amotion, oldskill_id));
				adstate->jauge += t;
			}

			// Make sure the jauge is not too big.
			if (adstate->jauge > (acceptable_animation_delay * adelays_config.adelays_buffer_size))
				adstate->jauge = acceptable_animation_delay * adelays_config.adelays_buffer_size;
		} else {
			// The skill is a NodelaySkill. Allow Hack count reduction only the first time it's used.

			// ***** Reduce HackCount Once if First Skill Without Delay
			if (adstate->allowed_reduction == 1 && (adstate->infixed < 1)) {
				adstate->hackcount -= adelays_config.adelays_reduce_hackcount_on_hitlock;
				adstate->allowed_reduction = 0;
			}
		}

		// Cap hackcount
		adstate->hackcount = max(0, adstate->hackcount);

		// Reduce hitlock not matter what.
		if (adstate->hitlock > 0) {
			adstate->hitlock -= 1;
		}
		// Cap hitlock.
		adstate->hitlock = max(0, adstate->hitlock);
	}


	// Allow a new future reduction of the hack count on the next hitlock skill in case it's a verified skill.
	if (option > 0) {
		adstate->allowed_reduction = 1;
	}

	adstate->last_skill_failed = 0;

	return 0;
}

int adelays_time_to_hard(struct map_session_data *sd, int skill_id) {
	if (sd->adelays_state.chain_count == adelays_get_hard_delay_chain_count(skill_id)) {
		return true;
	}
	return 0;
}

int adelays_set_damages(struct map_session_data *srcsd, struct map_session_data *dstsd, int skill_id, int64 tick, int sdelay, int damage, int div) {
	if (damage > 0) {
		if (srcsd != NULL) {
			// Some specific condition can make an hitlock on the caster.
			switch (skill_id) {
			case AL_HEAL: // When Heal attacks, it has nodelay.
				adelays_set_1hitlock(srcsd);
			default:
				break;
			}
		}

		if (dstsd != NULL) {
			// If there is a damage target
			adelays_set_hitlock(dstsd);
			if (div > 1)
				adelays_set_hitlock_until(dstsd, tick + sdelay + div * 200);
		}
	}

	return 0;
}

int adelays_is_enabled(void) {
	return (adelays_config.adelays_enable == 1);
}

#endif // ENABLE_ADELAYS
