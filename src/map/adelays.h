// Copyright (c) Adelays - Licensed under GNU GPL
// adelays@ragnawork.com
// http://ragnawork.com

#ifndef MAP_ADELAYS_H
#define MAP_ADELAYS_H

#include "common/hercules.h"

#define ADELAYS
#ifdef ADELAYS

/** Forward Declarations */
struct map_session_data;

/** Structures */
struct adelays_state {
	unsigned int showinfo : 1;
	unsigned int log : 1;
	int last_skill_id;
	int previous_skill_id;
	int hitlock;
	int64 hitlock_until;
	int64 skill_tick;
	int hackcount;
	int allowed_reduction;
	int jauge;
	int infixed;
	int chain_count;
	int accumulated_delay;
	int missing_delay;
	int expected_hard_delay;
	int previous_client_delay;
	int last_client_delay;
	int last_server_delay;
	int last_amotion;
	int last_hard;
	int last_acceptable_delay;
	int last_skill_failed;
	int hard_ignore;
	char last_result[10];
};

extern struct adelays_config {
	int adelays_tol;
	int adelays_hitlock_time_tolerance;
	int adelays_incr;
	int adelays_enable;
	int adelays_max_hitlock_count;
	int adelays_hitlock_incr;
	int adelays_reset_time;
	int adelays_reduce_hitlock_on_correct;
	int adelays_reduce_hitlock_on_hitlock;
	int adelays_reduce_hackcount_on_correct;
	int adelays_reduce_hackcount_on_hitlock;
	int adelays_fixeddelay_tol;
	int adelays_buffer_size;
	int adelays_max_infixed;
	int adelays_buffer_time;
	int adelays_scroll_fix;
	int adelays_jauge_kept_multiplicator;
	int adelays_fix_delay_multiplicator;
	int adelays_jauge_kept_multiplicator_on_new_skill_without_delay;
	int adelays_jauge_augmentation_multiplicator;
	int adelays_jauge_to_decrease_multiplicator;
	int adelays_max_hackcount_to_get_jauge;
	int adelays_acceptable_multiplicator_for_jauge;
	int adelays_delay_multiplicator_to_reduce_hackcount;
	int adelays_max_infixed_to_get_jauge;
	int adelays_jauge_added_after_cutanim;
	int adelays_allow_buffer_on_fixed;
	int adelays_max_aspd;
	int adelays_min_jauge_multiplicator;
	int adelays_allow_harddelays;
	int adelays_put_hard_limit_on_all_skills;
	int adelays_hard_chain_limit;
	int adelays_sit_delay_bodyreloc;
} adelays_config;

int adelays_battle_config_read(void);
void adelays_set_defaults(void);
int adelays_init(void);

int adelays_read_db(void);
int adelays_is_sit_allowed(struct map_session_data *sd);
int adelays_record_skill_delay(int skillid, int delayfix, int cooldown, struct map_session_data *sd, int64 tick);
void adelays_set_1hitlock(struct map_session_data *sd);
void adelays_set_hitlock(struct map_session_data *sd);
void adelays_set_hitlock_until(struct map_session_data *sd, int64 tick);
void adelays_dataset(struct map_session_data *sd);
int adelays_can_use_item(int i, int64 cannact_tick);
float adelays_get_version(void);
int adelays_get_log_message(char *message, struct map_session_data *sd);
int adelays_process_record(int64 tick, int64 *cannact_tick, struct map_session_data *sd);
void adelays_skill_failed(struct map_session_data *sd);
int adelays_set_damages(struct map_session_data *srcsd, struct map_session_data *dstsd, int skill_id, int64 tick, int sdelay, int damage, int div);
int adelays_is_enabled(void);
int adelays_time_to_hard(struct map_session_data *sd, int skill_id);
void adelays_calculate_missing_delay(int skillid, struct map_session_data *sd);
int adelays_get_harddelay(int skill_id, int amotion, int skillamount);
int adelays_calculate_acceptable_delay(int skill_id, int amotion);
int adelays_get_fix_delay(unsigned short amotion, int skillid);
int adelays_skill_has_doubles(int skill_id);
int adelays_set_job_delay(int count, int option, int class_id, int sex, int per);
int adelays_set_item_delays(int count, int nameid, int option);
int adelays_setskill_delays(char *skill_name, int delay_at150, int delay_at190, int option, int allow_hitlock);
int adelays_set_skill_ignore_delay(char *skill_name);

int adelays_get_force_harddelay(int id);
int adelays_get_hard_delay_chain_count(int id);
int adelays_get_harddelay190(int id);
int adelays_get_harddelay150(int id);
int adelays_get_delay190(int id);
int adelays_get_ignore_delay(int id);
int adelays_get_option(int id);
int adelays_get_allow_hitlock(int id);
int adelays_can_hard_delay_on_skill(int id);
int adelays_can_ignore_delay_on_skill(int id);
int adelays_set_skill_hard_delays(char *skill_name, int delay_at150, int delay_at190, int chain_max, int doubles);
#endif
#endif /* MAP_ADELAYS_H */
