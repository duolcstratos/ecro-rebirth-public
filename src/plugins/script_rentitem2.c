//Copyright (c) Mhalicot, licensed under GNU GPL.
// See the LICENSE file
// Hercules Plugin

#include "common/hercules.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common/HPMi.h"
#include "common/timer.h"
#include "map/script.h"
#include "map/status.h"
#include "map/pc.h"
#include "map/clif.h"
#include "map/pet.h"
#include "map/script.h"
#include "map/itemdb.h"

#include "common/HPMDataCheck.h"

/**
 * 1.0 - Initial Script [Mhalicot]
 * 2.0 - Added [AtCommand]rentitem
 **/
HPExport struct hplugin_info pinfo = {
	"script_rentitem2",		// Plugin name
	SERVER_TYPE_MAP,	// Which server types this plugin works with?
	"1.0",				// Plugin version
	HPM_VERSION,		// HPM Version (don't change, macro is automatically updated)
};

ACMD(rentitem) {
	char item_name[100];
	int number = 0, item_id, flag = 0;
	struct item item_tmp;
	struct item_data *item_data;
	int get_count;
	
	memset(item_name, '\0', sizeof(item_name));

	if (!message || !*message || (
		sscanf(message, "\"%99[^\"]\" %d", item_name, &number) < 1 && 
		sscanf(message, "%99s %d", item_name, &number) < 1)) 
		{
 		clif->message(fd, "Please enter an item name or ID (usage: @rentitem <item name/ID> <minutes>).");
		return true;
	}
	
	if (number <= 0)
		number = 1;
	
	if ((item_data = itemdb->search_name(item_name)) == NULL &&
	    (item_data = itemdb->exists(atoi(item_name))) == NULL) {
		clif->message(fd, "Invalid item ID or name.");
		return false;
	}

	item_id = item_data->nameid;
	get_count = number;
	//Check if it's stackable.
	if (itemdb->isstackable2(item_data)) {
		clif->message(fd, "Cannot create rented stackable items.");
		return false;
	}
	if (item_data->type == IT_PETEGG || item_data->type == IT_PETARMOR) {
		clif->message(fd, "Cannot create rented pet eggs or pet armors.");
		return false;
	}

		// if not pet egg
		if (!pet->create_egg(sd, item_id)) {
				number = get_count*60;
				memset(&item_tmp, 0, sizeof(item_tmp));
				item_tmp.nameid = item_id;
				item_tmp.identify = 1;
				item_tmp.expire_time = (unsigned int)(time(NULL) + number);
				item_tmp.bound = 0;

			if ((flag = pc->additem(sd, &item_tmp, 1, LOG_TYPE_COMMAND)))
				clif->additem(sd, 0, 0, flag);
		}

	if (flag == 0)
		clif->message(fd, "Item created.");
	return true;
}

BUILDIN(getequipexpiretick) {
	int i, num;
	TBL_PC* sd;

	sd = script->rid2sd(st);
	if (sd == NULL)
		return true;

	num = script_getnum(st,2) - 1;
	if (num < 0 || num >= ARRAYLENGTH(script->equip)) {
		script_pushint(st,-1);
		return true;
	}

	// get inventory position of item
	i = pc->checkequip(sd,script->equip[num]);
	if (i < 0) {
		script_pushint(st,-1);
		return true;
	}
	
	if (sd->inventory_data[i] != 0 && sd->status.inventory[i].expire_time)
		script_pushint(st, (unsigned int)(sd->status.inventory[i].expire_time - time(NULL)));
	else
		script_pushint(st,0);

	return true;
}

BUILDIN(rentitem2) {
	struct map_session_data *sd;
	struct script_data *data;
	struct item it;
	int seconds;
	int nameid = 0, flag;
	int iden,ref,attr,c1,c2,c3,c4;

	data = script_getdata(st,2);
	script->get_val(st,data);

	if ((sd = script->rid2sd(st)) == NULL)
		return 0;

	if (data_isstring(data)) {
		const char *name = script->conv_str(st,data);
		struct item_data *itd = itemdb->search_name(name);
		if (itd == NULL) {
			ShowError("buildin_rentitem: Nonexistant item %s requested.\n", name);
			return 1;
		}
		nameid = itd->nameid;
	}
	else if (data_isint(data)) {
		nameid = script->conv_num(st,data);
		if (nameid <= 0 || !itemdb->exists(nameid)) {
			ShowError("buildin_rentitem: Nonexistant item %d requested.\n", nameid);
			return 1;
		}
	} else {
		ShowError("buildin_rentitem: invalid data type for argument #1 (%d).\n", data->type);
		return 1;
	}

	seconds = script_getnum(st,3);
	iden=script_getnum(st,4);
	ref=script_getnum(st,5);
	attr=script_getnum(st,6);
	c1=(short)script_getnum(st,7);
	c2=(short)script_getnum(st,8);
	c3=(short)script_getnum(st,9);
	c4=(short)script_getnum(st,10);

	memset(&it, 0, sizeof(it));
	it.nameid = nameid;
	it.identify = iden;
	it.refine=ref;
	it.attribute=attr;
	it.card[0]=(short)c1;
	it.card[1]=(short)c2;
	it.card[2]=(short)c3;
	it.card[3]=(short)c4;
	it.expire_time = (unsigned int)(time(NULL) + seconds);

	if ((flag = pc->additem(sd, &it, 1, LOG_TYPE_SCRIPT))) {
		clif->additem(sd, 0, 0, flag);
		return 1;
	}
	return true;
}

/* run when server starts */
HPExport void plugin_init (void) {
	addAtcommand("rentitem",  rentitem);
	addScriptCommand("rentitem2", "viiiiiiii", rentitem2);
	addScriptCommand("getequipexpiretick", "i", getequipexpiretick);
}
