//===== Hercules Plugin ======================================
//= @whobuy
//===== By: ==================================================
//= by Dastgir/Hercules
//===== Current Version: =====================================
//= 1.0
//===== Description: =========================================
//= Shows the player who is currently buying item(buyingstore)
//===== Changelog: ===========================================
//= v1.0 - Initial Release
//===== Additional Comments: =================================
//= @whobuy ItemName
//===== Repo Link: ===========================================
//= https://github.com/dastgir/HPM-Plugins
//============================================================

#include "common/hercules.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common/HPMi.h"
#include "map/atcommand.h"
#include "map/battle.h"
#include "map/clif.h"
#include "map/itemdb.h"
#include "map/map.h"
#include "map/pc.h"
#include "map/script.h"
#include "common/mapindex.h"
#include "common/nullpo.h"
#include "plugins/HPMHooking.h"
#include "common/HPMDataCheck.h"


HPExport struct hplugin_info pinfo = {
	"@whobuy",
	SERVER_TYPE_MAP,
	"1.0",
	HPM_VERSION,
};

static const char* str_insert_comma(unsigned int number, char* output) {
	char buf[64];
	sprintf(buf, "%d", number);

	int i, j, k;

	j = strlen(buf);
	k = 0;

	for (i = 0; i < j; i++) {
		if ((j - i) % 3 == 0 && i != 0)
			output[k++] = ',';
		output[k++] = buf[i];
	}

	output[k] = '\0';

	return output;
}

ACMD(whobuy) {
	char item_name[100];
	char price_str[15], price_max_str[15];
	int item_id, j, count = 0, sat_num = 0;
	bool flag = 0;		// Show Location on minimap?
	struct map_session_data* pl_sd;
	struct s_mapiterator* iter;
	unsigned int MinPrice = battle->bc->vending_max_value, MaxPrice = 0;
	struct item_data *item_data;
	char output[256];

	memset(item_name, '\0', sizeof(item_name));

	if (!*message || sscanf(message, "%99[^\n]", item_name) < 1) {
		clif->message(fd, "Input item name or ID (use: @whobuy <name or ID>).");
		return false;
	}
	if ((item_data = itemdb->search_name(item_name)) == NULL &&
		(item_data = itemdb->exists(atoi(item_name))) == NULL) {
		clif->message(fd, msg_txt(19)); // Invalid item ID or name.
		return false;
	}

	item_id = item_data->nameid;

	clif->messagecolor_self(sd->fd, 0xffff94, "======== [ID] Item Name (Amount) | Map (X,Y) | Shop Name | Price ========");
	iter = mapit_getallusers();
	for (pl_sd = BL_UCAST(BL_PC, mapit->first(iter)); mapit->exists(iter); pl_sd = BL_UCAST(BL_PC, mapit->next(iter))) {
		if (sd == pl_sd)
			continue;
		if (pl_sd->state.buyingstore) {	// Check if player is autobuying
			for (j = 0; j < pl_sd->buyingstore.slots; j++) {
				if(pl_sd->buyingstore.items[j].nameid == item_id) {
					sprintf(output, "[%d] %s (%d ea.) | %s (%d,%d) | %s | %s z",
						pl_sd->buyingstore.items[j].nameid,
						item_data->jname,
						pl_sd->buyingstore.items[j].amount,
						map->list[pl_sd->bl.m].name,
						pl_sd->bl.x, pl_sd->bl.y,
						pl_sd->message,
						str_insert_comma(pl_sd->buyingstore.items[j].price, price_str));
					if ((unsigned int)pl_sd->buyingstore.items[j].price < MinPrice)
						MinPrice = pl_sd->buyingstore.items[j].price;
					if ((unsigned int)pl_sd->buyingstore.items[j].price > MaxPrice)
						MaxPrice = pl_sd->buyingstore.items[j].price;
					clif->messagecolor_self(sd->fd, 0xffff7b, output);
					count++;
					flag = 1;
				}
			}
			if (flag && pl_sd->mapindex == sd->mapindex) {
				clif->viewpoint(sd, 1, 1, pl_sd->bl.x, pl_sd->bl.y, ++sat_num, 0xFFFFFF);
				flag = 0;
			}
		}
	}
	mapit->free(iter);

	if (count > 0) {
		if (count > 1) {
			if (MinPrice != MaxPrice)
				snprintf(output, CHAT_SIZE_MAX, "======== Found %d ea. Prices from %s z to %s z. ========", count, str_insert_comma(MinPrice, price_str), str_insert_comma(MaxPrice, price_max_str));
			else
				snprintf(output, CHAT_SIZE_MAX, "======== Found %d ea. The price is %s z. ========", count, str_insert_comma(MaxPrice, price_max_str));
		}
		else
			snprintf(output, CHAT_SIZE_MAX, "======== Found 1 ea. The price is %s z. ========", str_insert_comma(MinPrice, price_str));
		clif->messagecolor_self(sd->fd, 0xffff94, output);
	} else
		clif->message(fd, "Nobody buying it now.");

	return true;
}

/* Server Startup */
HPExport void plugin_init(void) {
	addAtcommand("whobuy", whobuy);
}

HPExport void server_online(void) {
	ShowInfo("'%s' Plugin by Dastgir/Hercules. Version '%s'\n", pinfo.name, pinfo.version);
}
