//===== Hercules Plugin ======================================
//= BG Stats
//===== By: ==================================================
//= Dastgir/Hercules
//===== Modded by: ===========================================
//= fTakano
//===== Current Version: =====================================
//= 1.0
//===== Description: =========================================
//= BG Ranking
//============================================================

#include "common/hercules.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common/HPMi.h"
#include "common/mmo.h"
#include "common/socket.h"
#include "common/memmgr.h"
#include "common/nullpo.h"
#include "common/mapindex.h"
#include "common/strlib.h"
#include "common/utils.h"
#include "common/cbasetypes.h"
#include "common/timer.h"
#include "common/showmsg.h"
#include "common/conf.h"
#include "common/db.h"
#include "common/sql.h"

#include "char/inter.h"
#include "char/mapif.h"

#include "map/battle.h"
#include "map/battleground.h"
#include "map/clif.h"
#include "map/chrif.h"
#include "map/log.h"
#include "map/map.h"
#include "map/mob.h"
#include "map/npc.h"
#include "map/pc.h"
#include "map/skill.h"
#include "map/script.h"
#include "map/status.h"
#include "map/unit.h"

#include "plugins/HPMHooking.h"
#include "common/HPMDataCheck.h"

HPExport struct hplugin_info pinfo = {
	"BG Stats",
	SERVER_TYPE_MAP|SERVER_TYPE_CHAR,
	"1.0",
	HPM_VERSION,
};

/**
 * Packet ID's
 */
enum packets_bgstats {
	PACKET_MC_REQ_DATA_BG = 0x4100,
	PACKET_CM_DATA_BG,
	PACKET_MC_SAVE_DATA_BG,
	PACKET_MC_REQ_FAME_DATA_BG,
	PACKET_CM_FAME_DATA_BG,
};

enum bg_monsters {
	MOBID_GARM            = 1252,
	MOBID_RSX_0806        = 1623,
	MOBID_RANDGRIS        = 1751,
	MOBID_GLOOMUNDERNIGHT = 1768,
	MOBID_MOROCC          = 1916,
};

enum bg_modes {
	BG_CQ = 1,
	BG_RUSH,
	BG_TDM,
	BG_CTF,
	BG_SC,
	BG_BOSS,
	BG_DOM,
	BG_EOS,
	BG_TI
};

enum bg_result {
	BG_R_WIN = 0,
	BG_R_TIE,
	BG_R_LOSE
};

#define fame_list_size_bg MAX_FAME_LIST
struct fame_list bg_fame_list[fame_list_size_bg];

void pc_addpoint_bg(struct map_session_data* sd, int count, int type, bool leader, int extra_count);
bool update_bg_ranking(struct map_session_data* sd, int type_);

/**
 * BG Statistics
 **/
struct s_bgstats {
	int score, points, rank_points;
	uint64
		top_damage,
		damage_done,
		damage_received,
		boss_damage;
	unsigned int
		// Triple Inferno
		ti_skulls,
		ti_wins, ti_lost, ti_tie,
		// Tierra Gorge - Eye of the Storm
		eos_flags,
		eos_bases,
		eos_wins, eos_lost, eos_tie,
		// Tierra Gorge - Bossnia
		boss_killed,
		boss_flags,
		boss_wins, boss_lost, boss_tie,
		// Tierra Gorge - Domination
		dom_bases,
		dom_atk_kills,
		dom_def_kills,
		dom_wins, dom_lost, dom_tie,
		// Flavius - Team Deathmatch
		td_kills,
		td_deaths,
		td_wins, td_lost, td_tie,
		// Flavius - Stone Control
		sc_stole,
		sc_captured,
		sc_dropped,
		sc_wins, sc_lost, sc_tie,
		// Flavius - Capture the Flag
		ctf_taken,
		ctf_captured,
		ctf_dropped,
		ctf_wins, ctf_lost, ctf_tie,
		// WoE Arena - Conquest
		cq_emperium_kill,
		cq_barricade_kill,
		cq_gstone_kill,
		cq_wins, cq_lost,
		// WoE Arena - Rush
		ru_captures,
		ru_wins, ru_lost;
	unsigned int
		sp_heal_potions,
		hp_heal_potions,
		yellow_gemstones,
		red_gemstones,
		blue_gemstones,
		poison_bottles,
		acid_demostration,
		acid_demostration_fail,
		support_skills_used,
		healing_done,
		wrong_support_skills_used,
		wrong_healing_done,
		sp_used,
		zeny_used,
		spiritb_used,
		ammo_used,
		kill_count,
		death_count,
		win, lost, tie,
		leader_win, leader_lost, leader_tie,
		deserter, rank_games;
	bool modified;
};

#define map_allowed_bg(m) (map->list[m].flag.battleground)

#define add2limit_bg(modified, a, b, max) \
	do { \
		if( (max - a) < b ) { \
			a = max; \
		} else { \
			a += b; \
		} \
		modified = true; \
	} while(0)

#define sub2limit_bg(modified, a, b, min) \
	do { \
		if( (b + min) > a ) { \
			a = min; \
		} else { \
			a -= b; \
		} \
		modified = true; \
	} while(0)

struct player_data {
	struct s_bgstats bg;
	uint16 skill_id_used;
	bool received;
};

void request_bg_stats_data(int account_id, int char_id) {
	WFIFOHEAD(chrif->fd, 10);
	WFIFOW(chrif->fd, 0) = PACKET_MC_REQ_DATA_BG;
	WFIFOL(chrif->fd, 2) = account_id;
	WFIFOL(chrif->fd, 6) = char_id;
	WFIFOSET(chrif->fd, 10);
}

/**
 * Searches for Memory for BG Stats
 * @server MAP
 * @method search_bg_map
 * @param  sd             Player Session Data
 * @return                Player Data
 */
struct player_data* search_bg_map(struct map_session_data *sd, bool data_received) {
	struct player_data *data;

	if (sd == NULL)
		return NULL;

	data = getFromMSD(sd, 0);

	if (data == NULL) {
		CREATE(data, struct player_data, 1);
		memset(data, 0, sizeof(struct player_data));
		data->bg.modified = false;
		data->received = false;
		addToMSD(sd, data, 0, true);
	}

	if (!data->received && !data_received) {
		request_bg_stats_data(sd->status.account_id, sd->status.char_id);
		return NULL;
	}
	return data;
}

void pc_addpoint_bg(struct map_session_data* sd, int count, int type, bool leader, int extra_count) {
	struct player_data* data;
	char message[CHAT_SIZE_MAX];

	nullpo_retv(sd);

	data = search_bg_map(sd, true);
	if (data == NULL)
		return;

	switch (type) {
		case 0:
			data->bg.points += count;
			sprintf(message, "[ Battleground ] You have been rewarded with %d Battleground rank points. Your point total is %d.", count, data->bg.points);
			clif->messagecolor_self(sd->fd, 0xddcd48, message);
			if (leader && extra_count > 0) {
				data->bg.points += extra_count;
				sprintf(message, "[ Battleground ] You have been rewarded with additional %d Battleground rank points as a Leader. Your point total is %d.", extra_count, data->bg.points);
				clif->messagecolor_self(sd->fd, 0xddcd48, message);
			}
			data->bg.modified = true;
			break;
	}
	update_bg_ranking(sd, (type + 1));
	return;
}

/**
 * clif_parse_LoadEndAck preHooked
 * Requests Data from CharServer.
 * @param sd map_session_data
 * @see 	
 * @server MAP
 **/
void clif_parse_LoadEndAck_pre(int *fd, struct map_session_data **sd_) {
	struct map_session_data* sd = *sd_;

	if (!sd->state.active) { // Character loading is not complete yet!
		return;
	}
	if (sd->state.rewarp) { // Re-warp, just return for now.
		return;
	}
	if (sd->state.connect_new) { // First Time Login
		request_bg_stats_data(sd->status.account_id, sd->status.char_id);
	}
	return;
}

/**
 * Sends the BG Data Back to Map Server
 * @server CHAR
 * @method char_send_bg_data
 * @param  fd                 FD
 * @param  char_id            Character ID
 * @param  account_id         Account ID
 * @param  data               Player Data
 */
void char_send_bg_data(int fd, int char_id, int account_id, struct s_bgstats *bg) {
	unsigned char buf[1024];
	int len = 10 + sizeof(struct s_bgstats);
	WBUFW(buf, 0) = PACKET_CM_DATA_BG;
	WBUFL(buf, 2) = account_id;
	WBUFL(buf, 6) = char_id;
	memcpy(WBUFP(buf, 10), bg, sizeof(struct s_bgstats));

	mapif->send(fd, buf, len);
	return;
}

/**
 * Packet: PACKET_MC_REQ_DATA_BG
 * @server CHAR
 * @method char_load_bgdata_fromsql
 * @param  fd                FD
 */
void char_load_bgdata_fromsql(int fd) {
	struct s_bgstats bg = { 0 };
	int account_id = RFIFOL(fd, 2);
	int char_id = RFIFOL(fd, 6);

	StringBuf buf;
	struct SqlStmt *stmt = SQL->StmtMalloc(inter->sql_handle);

	StrBuf->Init(&buf);
	if (SQL_ERROR == SQL->StmtPrepare(stmt, "SELECT `top_damage`, `damage_done`, `damage_received`, `boss_damage`, `sp_heal_potions`, "
		"`hp_heal_potions`, `yellow_gemstones`, `red_gemstones`, `blue_gemstones`, `poison_bottles`, "
		"`acid_demostration`, `acid_demostration_fail`, `support_skills_used`, `healing_done`, `wrong_support_skills_used`, "
		"`wrong_healing_done`, `sp_used`, `zeny_used`, `spiritb_used`, `ammo_used`, "
		"`kill_count`, `death_count`, `score`, `points`, `win`, "
		"`lost`, `tie`, `leader_win`, `leader_lost`, `leader_tie`, "
		"`deserter`, `rank_games`, `rank_points`, `ti_skulls`, `ti_wins`, "
		"`ti_lost`, `ti_tie`, `eos_flags`, `eos_bases`, `eos_wins`, "
		"`eos_lost`, `eos_tie`, `boss_killed`, `boss_flags`, `boss_wins`, "
		"`boss_lost`, `boss_tie`, `dom_bases`, `dom_atk_kills`, `dom_def_kills`, "
		"`dom_wins`, `dom_lost`, `dom_tie`, `td_kills`, `td_deaths`, "
		"`td_wins`, `td_lost`, `td_tie`, `sc_stole`, `sc_captured`, "
		"`sc_dropped`, `sc_wins`, `sc_lost`, `sc_tie`, `ctf_taken`, "
		"`ctf_captured`, `ctf_dropped`, `ctf_wins`, `ctf_lost`, `ctf_tie`, "
		"`cq_emperium_kill`, `cq_barricade_kill`, `cq_gstone_kill`, `cq_wins`, `cq_lost`, "
		"`ru_captures`, `ru_wins`, `ru_lost` "
		"FROM `char_bg_statistics` WHERE `char_id` = '%d' LIMIT 1", char_id)
		|| SQL_ERROR == SQL->StmtExecute(stmt)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 0, SQLDT_UINT64, &bg.top_damage, sizeof bg.top_damage, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 1, SQLDT_UINT64, &bg.damage_done, sizeof bg.damage_done, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 2, SQLDT_UINT64, &bg.damage_received, sizeof bg.damage_received, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 3, SQLDT_UINT64, &bg.boss_damage, sizeof bg.boss_damage, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 4, SQLDT_UINT, &bg.sp_heal_potions, sizeof bg.sp_heal_potions, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 5, SQLDT_UINT, &bg.hp_heal_potions, sizeof bg.hp_heal_potions, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 6, SQLDT_UINT, &bg.yellow_gemstones, sizeof bg.yellow_gemstones, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 7, SQLDT_UINT, &bg.red_gemstones, sizeof bg.red_gemstones, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 8, SQLDT_UINT, &bg.blue_gemstones, sizeof bg.blue_gemstones, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 9, SQLDT_UINT, &bg.poison_bottles, sizeof bg.poison_bottles, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 10, SQLDT_UINT, &bg.acid_demostration, sizeof bg.acid_demostration, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 11, SQLDT_UINT, &bg.acid_demostration_fail, sizeof bg.acid_demostration_fail, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 12, SQLDT_UINT, &bg.support_skills_used, sizeof bg.support_skills_used, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 13, SQLDT_UINT, &bg.healing_done, sizeof bg.healing_done, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 14, SQLDT_UINT, &bg.wrong_support_skills_used, sizeof bg.wrong_support_skills_used, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 15, SQLDT_UINT, &bg.wrong_healing_done, sizeof bg.wrong_healing_done, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 16, SQLDT_UINT, &bg.sp_used, sizeof bg.sp_used, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 17, SQLDT_UINT, &bg.zeny_used, sizeof bg.zeny_used, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 18, SQLDT_UINT, &bg.spiritb_used, sizeof bg.spiritb_used, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 19, SQLDT_UINT, &bg.ammo_used, sizeof bg.ammo_used, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 20, SQLDT_UINT, &bg.kill_count, sizeof bg.kill_count, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 21, SQLDT_UINT, &bg.death_count, sizeof bg.death_count, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 22, SQLDT_UINT, &bg.score, sizeof bg.score, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 23, SQLDT_UINT, &bg.points, sizeof bg.points, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 24, SQLDT_UINT, &bg.win, sizeof bg.win, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 25, SQLDT_UINT, &bg.lost, sizeof bg.lost, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 26, SQLDT_UINT, &bg.tie, sizeof bg.tie, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 27, SQLDT_UINT, &bg.leader_win, sizeof bg.leader_win, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 28, SQLDT_UINT, &bg.leader_lost, sizeof bg.leader_lost, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 29, SQLDT_UINT, &bg.leader_tie, sizeof bg.leader_tie, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 30, SQLDT_UINT, &bg.deserter, sizeof bg.deserter, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 31, SQLDT_UINT, &bg.rank_games, sizeof bg.rank_games, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 32, SQLDT_UINT, &bg.rank_points, sizeof bg.rank_points, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 33, SQLDT_UINT, &bg.ti_skulls, sizeof bg.ti_skulls, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 34, SQLDT_UINT, &bg.ti_wins, sizeof bg.ti_wins, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 35, SQLDT_UINT, &bg.ti_lost, sizeof bg.ti_lost, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 36, SQLDT_UINT, &bg.ti_tie, sizeof bg.ti_tie, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 37, SQLDT_UINT, &bg.eos_flags, sizeof bg.eos_flags, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 38, SQLDT_UINT, &bg.eos_bases, sizeof bg.eos_bases, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 39, SQLDT_UINT, &bg.eos_wins, sizeof bg.eos_wins, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 40, SQLDT_UINT, &bg.eos_lost, sizeof bg.eos_lost, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 41, SQLDT_UINT, &bg.eos_tie, sizeof bg.eos_tie, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 42, SQLDT_UINT, &bg.boss_killed, sizeof bg.boss_killed, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 43, SQLDT_UINT, &bg.boss_flags, sizeof bg.boss_flags, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 44, SQLDT_UINT, &bg.boss_wins, sizeof bg.boss_wins, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 45, SQLDT_UINT, &bg.boss_lost, sizeof bg.boss_lost, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 46, SQLDT_UINT, &bg.boss_tie, sizeof bg.boss_tie, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 47, SQLDT_UINT, &bg.dom_bases, sizeof bg.dom_bases, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 48, SQLDT_UINT, &bg.dom_atk_kills, sizeof bg.dom_atk_kills, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 49, SQLDT_UINT, &bg.dom_def_kills, sizeof bg.dom_def_kills, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 50, SQLDT_UINT, &bg.dom_wins, sizeof bg.dom_wins, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 51, SQLDT_UINT, &bg.dom_lost, sizeof bg.dom_lost, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 52, SQLDT_UINT, &bg.dom_tie, sizeof bg.dom_tie, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 53, SQLDT_UINT, &bg.td_kills, sizeof bg.td_kills, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 54, SQLDT_UINT, &bg.td_deaths, sizeof bg.td_deaths, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 55, SQLDT_UINT, &bg.td_wins, sizeof bg.td_wins, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 56, SQLDT_UINT, &bg.td_lost, sizeof bg.td_lost, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 57, SQLDT_UINT, &bg.td_tie, sizeof bg.td_tie, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 58, SQLDT_UINT, &bg.sc_stole, sizeof bg.sc_stole, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 59, SQLDT_UINT, &bg.sc_captured, sizeof bg.sc_captured, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 60, SQLDT_UINT, &bg.sc_dropped, sizeof bg.sc_dropped, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 61, SQLDT_UINT, &bg.sc_wins, sizeof bg.sc_wins, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 62, SQLDT_UINT, &bg.sc_lost, sizeof bg.sc_lost, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 63, SQLDT_UINT, &bg.sc_tie, sizeof bg.sc_tie, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 64, SQLDT_UINT, &bg.ctf_taken, sizeof bg.ctf_taken, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 65, SQLDT_UINT, &bg.ctf_captured, sizeof bg.ctf_captured, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 66, SQLDT_UINT, &bg.ctf_dropped, sizeof bg.ctf_dropped, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 67, SQLDT_UINT, &bg.ctf_wins, sizeof bg.ctf_wins, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 68, SQLDT_UINT, &bg.ctf_lost, sizeof bg.ctf_lost, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 69, SQLDT_UINT, &bg.ctf_tie, sizeof bg.ctf_tie, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 70, SQLDT_UINT, &bg.cq_emperium_kill, sizeof bg.cq_emperium_kill, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 71, SQLDT_UINT, &bg.cq_barricade_kill, sizeof bg.cq_barricade_kill, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 72, SQLDT_UINT, &bg.cq_gstone_kill, sizeof bg.cq_gstone_kill, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 73, SQLDT_UINT, &bg.cq_wins, sizeof bg.cq_wins, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 74, SQLDT_UINT, &bg.cq_lost, sizeof bg.cq_lost, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 75, SQLDT_UINT, &bg.ru_captures, sizeof bg.ru_captures, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 76, SQLDT_UINT, &bg.ru_wins, sizeof bg.ru_wins, NULL, NULL)
		|| SQL_ERROR == SQL->StmtBindColumn(stmt, 77, SQLDT_UINT, &bg.ru_lost, sizeof bg.ru_lost, NULL, NULL))
	{
		SqlStmt_ShowDebug(stmt);
		SQL->StmtFree(stmt);
		return;
	}

	if (SQL_SUCCESS != SQL->StmtNextRow(stmt)) {
		bg.score = 2000;
	}

	char_send_bg_data(fd, char_id, account_id, &bg);
	ShowStatus("char_load_bgdata_fromsql: BG Stats Loaded for Character %d.\n", char_id);
	SQL->StmtFree(stmt);
	return;
}

/**
 * Packet: PACKET_CM_DATA_BG
 * @server MAP
 * @method map_bgdata_from_char
 * @param  fd                FD
 */
void map_bgdata_from_char(int fd) {
	int account_id = RFIFOL(fd, 2);
	int char_id = RFIFOL(fd, 6);
	struct map_session_data *sd = NULL;
	struct player_data *data = NULL;
	struct s_bgstats bgstat;

	sd = map->id2sd(account_id);
	memcpy(&bgstat, RFIFOP(fd, 10), sizeof(struct s_bgstats));

	// It is highly possible that Character might log off even before the BG Data has been received to map-server
	if (sd == NULL) {
		ShowError("map_bgdata_from_char: Character %d Found Offline, while packet received.\n", char_id);
		return;
	}

	data = search_bg_map(sd, true);
	if (data == NULL) {
		ShowError("map_bgdata_from_char: Cannot Allocate Memory for bgdata.\n");
		return;
	}

	memcpy(&data->bg, &bgstat, sizeof(struct s_bgstats));
	data->bg.modified = false;
	data->received = true;
	ShowInfo("Received BG Data: Character %d.\n", char_id);
	return;
}

/**
 * Notifies clients in area, that an object is about to use a skill.
 * Sets SkillUsed Counter
 * @see clif_useskill
 * @server MAP
 **/
void bg_record_skill(struct block_list **bl, int *src_id, int *dst_id, int *dst_x, int *dst_y, uint16 *skill_id, uint16 *skill_lv, int *casttime) {
	if ((*bl) != NULL && (*bl)->type == BL_PC) {
		struct map_session_data *sd = BL_CAST(BL_PC, (*bl));
		if (sd != NULL) {
			struct player_data *data = search_bg_map(sd, false);
			if (data != NULL) {
				data->skill_id_used = *skill_id;
			}
		}
	}
}

/**
 * Attack request
 * If type is an ongoing attack
 * Reset's Skill Used variable
 * unit_atttack preHooked.
 * @param src Attacker
 * @param target_id Target
 * @param continuos Attack once or non-stop
 * @see unit_attack
 * @server MAP
 **/
int bg_record_bare_attack(struct block_list **src, int *target_id, int *continuous) {
	if ((*src) != NULL && (*src)->type == BL_PC) {
		struct map_session_data *sd = BL_CAST(BL_PC, (*src));
		if (sd != NULL) {
			struct player_data *data = search_bg_map(sd, false);
			if (data != NULL) {
				data->skill_id_used = 0;
			}
		}
	}
	return 0;
}

/**
 * Records Ammo Used, 
 * battle_consume_ammo PreHooked
 * @see battle_consume_ammo
 * @server MAP
 **/
void bg_record_ammo_spent(struct map_session_data **sd, int *skill_id, int *lv) {
	unsigned int qty = 1;
	struct player_data *data;

	nullpo_retv(*sd);

	if (!battle->bc->arrow_decrement)
		return;

	data = search_bg_map((*sd), false);
	if (*skill_id && *lv) {
		qty = skill->get_ammo_qty(*skill_id, *lv);
		if (!qty)
			qty = 1;
	}
	if ((*sd)->equip_index[EQI_AMMO] >= 0) {
		if (data && (*sd)->bg_id && map_allowed_bg((*sd)->bl.m)) {
			add2limit_bg(data->bg.modified, data->bg.ammo_used, qty, UINT_MAX);
		}
	}
	return;
}

/**
 * Stores Max Damage/Damage
 * @server MAP
 **/
int store_damage_ranking(struct block_list *src, struct block_list *dst, int64 in_damage, int64 in_damage2) {
	struct map_session_data *sd = BL_CAST(BL_PC, src);
	struct player_data *data;
	uint64 max_damage;
	int64 total_damage;

	if (sd == NULL)
		return 0;

	data = search_bg_map(sd, false);
	if (data == NULL) {
		ShowError("store_damage_ranking: BG Data not received from char-server.\n");
		return 0;
	}
	if (map_allowed_bg(sd->bl.m) && sd->bg_id) {
		max_damage = data->bg.top_damage;
		total_damage = in_damage + in_damage2;

		if (total_damage > 0) {
			if (max_damage < (uint64)total_damage) {
				data->bg.top_damage = (uint64)total_damage;
			}
			data->bg.damage_done += total_damage;
			data->bg.modified = true;
			if (dst->type == BL_PC) {
				struct map_session_data *tsd = BL_CAST(BL_PC, dst);
				struct player_data *tdata = search_bg_map(tsd, false);
				if (tsd != NULL && tdata != NULL) {
					tdata->bg.damage_received += total_damage;
					tdata->bg.modified = true;
				}
			}
		}
		return 1;
	}
	return 0;
}

/**
 * Stores Max Damage
 * Also Records Acid Demonstration Use
 * clif_skill_damage preHooked
 * @see clif_skill_damage
 * @server MAP
 **/
int bg_record_max_damage(struct block_list **src_, struct block_list **dst_, int64 *tick, int *sdelay, int *ddelay, int64 *in_damage, int *div, uint16 *skill_id, uint16 *skill_lv, enum battle_dmg_type *type) {
	nullpo_ret(*src_);
	if ((*src_)->type == BL_PC) {
		if (store_damage_ranking((*src_), (*dst_), *in_damage, 0) == 1) {
			struct map_session_data *sd = BL_CAST(BL_PC, (*src_));
			if (sd != NULL) {
				if (*skill_id == CR_ACIDDEMONSTRATION) {
					struct player_data *data = search_bg_map(sd, false);
					if (data != NULL) {
						if (*in_damage > 0) {
							add2limit_bg(data->bg.modified, data->bg.acid_demostration, 1, UINT32_MAX);
						} else {
							add2limit_bg(data->bg.modified, data->bg.acid_demostration_fail, 1, UINT32_MAX);
						}
					}
				}
			}
		}
	}
	return 0;
}

/**
 * Stores Max Damage
 * type: @see enum battle_dmg_type
 * clif_damage PreHooked
 * @see clif_damage
 * @server MAP
 **/
int bg_record_max_damage2(struct block_list **src, struct block_list **dst, int *sdelay, int *ddelay, int64 *in_damage, short *div, enum battle_dmg_type *type, int64 *in_damage2) {
	nullpo_ret(*src);
	nullpo_ret(*dst);

	// ToDo: Should All Damage ranking happen if both are player?
	if ((*src) != (*dst) && (*src)->type == BL_PC && *in_damage > 0) {
		store_damage_ranking((*src), (*dst), *in_damage, *in_damage2);
	}
	return 0;
}

// Pow Alternative
int ipow(int base, int exp) {
    int result = 1;
    while (exp) {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }
    return result;
}

/**
 * Stores Kill Log and records skills too
 * @param killed Data of Player who died
 * @param killer Data of Player who killed other player.
 * @server MAP
 **/
void bg_kill(struct map_session_data *killed, struct map_session_data *killer) {
	struct player_data* killer_data = search_bg_map(killer, false);
	struct player_data* killed_data = search_bg_map(killed, false);
	unsigned int killed_score = 2000, killer_score = 2000;
	if (killed == NULL || killer == NULL || killer == killed)
		return;
	if (map_allowed_bg(killer->bl.m) && killer->bg_id) {
		char killer_name[NAME_LENGTH*2+1];
		char killed_name[NAME_LENGTH*2+1];
		uint16 skill_id = 0;
		int32 elo = 0;		if (killer_data) {
			skill_id = killer_data->skill_id_used;
		}

		SQL->EscapeStringLen(logs->mysql_handle, killer_name, killer->status.name, strnlen(killer->status.name, NAME_LENGTH));
		SQL->EscapeStringLen(logs->mysql_handle, killed_name, killed->status.name, strnlen(killed->status.name, NAME_LENGTH));

		if (SQL_ERROR == SQL->Query(logs->mysql_handle,
									"INSERT INTO `char_bg_kills` (`time`,`killer`,`killer_id`,`killed`,`killed_id`,`map`,`skill`) VALUES (NOW(), '%s', '%d', '%s', '%d', '%s', '%d')",
									killer_name, killer->status.char_id,
									killed_name, killed->status.char_id,
									map->list[killed->bl.m].name, skill_id))
			Sql_ShowDebug(logs->mysql_handle);

		// Guild Ranking - War of Emperium
		// Own Guild Ranking
		// tsd = Killed
		// ssd = Killer
		if (killed_data != NULL)
			killed_score = killed_data->bg.score;
		if (killer_data != NULL)
		killer_score = killer_data->bg.score;
		elo = (int32)cap_value((int32)(50.00 / (1 + ipow(10, (int32)((killer_score - killed_score) / 2000.00)))), INT32_MIN, INT32_MAX);

		if (killer_data != NULL)
			add2limit_bg(killer_data->bg.modified, killer_data->bg.score, elo, 4000);
		if (killed_data != NULL)
			sub2limit_bg(killed_data->bg.modified, killed_data->bg.score, elo, 0);
	}
}

/**
 * pc_dead PreHooked.
 * sd = Killed
 * src = Killer
 * @see pc_dead
 * @server MAP
 **/
int pc_dead_pre(struct map_session_data **sd, struct block_list **src_) {
	struct map_session_data *ssd;
	struct block_list *master = NULL;    ///< Killer

	nullpo_retr(0, sd);

	if ((*src_) != NULL)
		master = battle->get_master(*src_);

	ssd = BL_CAST(BL_PC, master);
	if ((*sd) != NULL && ssd != NULL) {
		struct player_data* data = search_bg_map((*sd), false);
		struct player_data* tdata = search_bg_map(ssd, false);
		if ((*sd) && ssd && map_allowed_bg((*sd)->bl.m)) {
			if ((*sd)->bg_id && data != NULL) {
				add2limit_bg(data->bg.modified, data->bg.death_count, 1, USHRT_MAX);
			}
			if (ssd->bg_id && tdata != NULL) {
				add2limit_bg(tdata->bg.modified, tdata->bg.kill_count, 1, USHRT_MAX);
			}
		}
		bg_kill((*sd), ssd);
	}
	return 1;
}

/**
 * Stores Damage Dealt to Monsters
 * mob_damage PreHooked
 * @see mob_damage
 * @server MAP
 **/
void bg_record_damage2(struct mob_data **md, struct block_list **src, int *damage) {
	struct map_session_data *sd;
	struct player_data *data;
	if (*md == NULL || *src == NULL || (*src)->type != BL_PC || *damage <= 0)
		return;

	sd = BL_CAST(BL_PC, *src);
	data = search_bg_map(sd, false);

	if (sd == NULL || data == NULL || !(map_allowed_bg(sd->bl.m) && sd->bg_id))
		return;

	if ((*md)->bg_id > 0) {
		switch ((*md)->class_) {
		case MOBID_GARM:
		case MOBID_RSX_0806:
		case MOBID_RANDGRIS:
		case MOBID_GLOOMUNDERNIGHT:
		case MOBID_MOROCC:
			add2limit_bg(data->bg.modified, data->bg.boss_damage, *damage, UINT64_MAX);
			break;
		}
	}
	return;
}

/**
 * Mob Dead PreHooked
 * Increases Count of BG Monsters Killed
 * @server MAP
 * @method mob_dead_pre
 * @param  md       MonsterData
 * @param  src      Player
 * @param  type     Type
 * @return          0
 */
int mob_dead_pre(struct mob_data **md, struct block_list **src_, int *type) {
	struct map_session_data *sd = NULL;
	struct player_data *data = NULL;
	if ((*src_) != NULL && (*src_)->type == BL_PC) {
		sd = (struct map_session_data *)(*src_);
		data = search_bg_map(sd, false);
	}
	if (sd == NULL || data == NULL || md == NULL) {
		return 0;
	}
	if (map_allowed_bg(sd->bl.m)) {
		switch ((*md)->class_) {
			case MOBID_EMPELIUM:
				if (stristr(map->list[sd->bl.m].name, "g_cas"))
					add2limit_bg(data->bg.modified, data->bg.cq_emperium_kill, 1, UINT32_MAX);
				else if (stristr(map->list[sd->bl.m].name, "rush_"))
					add2limit_bg(data->bg.modified, data->bg.ru_captures, 1, UINT32_MAX);
				pc_addpoint_bg(sd, 30, 0, false, 0);
				break;
			case MOBID_BARRICADE:
			case MOBID_BARRICADE_:
				add2limit_bg(data->bg.modified, data->bg.cq_barricade_kill, 1, UINT32_MAX);
				pc_addpoint_bg(sd, 1, 0, false, 0);
				break;
			case MOBID_S_EMPEL_1:
			case MOBID_S_EMPEL_2:
				add2limit_bg(data->bg.modified, data->bg.cq_gstone_kill, 1, UINT32_MAX);
				pc_addpoint_bg(sd, 10, 0, false, 0);
				break;
			case MOBID_GARM:
			case MOBID_RSX_0806:
			case MOBID_RANDGRIS:
			case MOBID_GLOOMUNDERNIGHT:
			case MOBID_MOROCC:
				add2limit_bg(data->bg.modified, data->bg.boss_killed, 1, UINT32_MAX);
				pc_addpoint_bg(sd, 25, 0, false, 0);
				break;
			case MOBID_OBJ_NEUTRAL:
				if (sd->bl.m == map->mapname2mapid("bat_a02"))
					add2limit_bg(data->bg.modified, data->bg.eos_flags, 1, UINT32_MAX);
				else if (sd->bl.m == map->mapname2mapid("bat_a03")) {
					add2limit_bg(data->bg.modified, data->bg.boss_flags, 1, UINT32_MAX);
					pc_addpoint_bg(sd, 5, 0, false, 0);
				}
				break;
			case MOBID_OBJ_FLAG_A:
			case MOBID_OBJ_FLAG_B:
				add2limit_bg(data->bg.modified, data->bg.boss_flags, 1, UINT32_MAX);
				break;
		}
	}
	return 0;
}

/**
 * Heals a character. If flag&1, this is forced healing (otherwise stuff like Berserk can block it)
 * Does Not Really Heal, and returns heal amount.
 * @see status_heal
 * @server MAP
 **/
int status_fake_heal(struct block_list *bl, int64 in_hp, int64 in_sp, int flag) {
	struct status_data *st;
	struct status_change *sc;
	int hp,sp;

	st = status->get_status_data(bl);
	if (st == &status->dummy || !st->hp)
		return 0;

	// From here onwards, we consider it a 32-type as the client does not support higher and the value doesn't get through percentage modifiers
	hp = (int)cap_value(in_hp,INT_MIN,INT_MAX);
	sp = (int)cap_value(in_sp,INT_MIN,INT_MAX);

	sc = status->get_sc(bl);
	if (sc && !sc->count)
		sc = NULL;

	if (hp < 0) {
		hp = 0;
	}

	if (hp) {
		if (sc && sc->data[SC_BERSERK]) {
			if (flag&1)
				flag &= ~2;
			else
				hp = 0;
		}
		if ((unsigned int)hp > st->max_hp - st->hp)
			hp = st->max_hp - st->hp;
	}

	if (sp < 0) {
		sp = 0;
	}

	if (sp) {
		if ((unsigned int)sp > (st->max_sp - st->sp))
			sp = st->max_sp - st->sp;
	}

	if (!sp && !hp)
		return 0;

	return (int)(hp+sp);
}

/**
 * Calculate Heal Done to TeamMate and Enemy for Rankings
 * skill_calc_heal PostHooked
 * @see skill_calc_heal
 * @server MAP
 **/
int bg_record_healing(int retVal, struct block_list *src, struct block_list *target, uint16 skill_id, uint16 skill_lv, bool heal) {
	struct map_session_data *sd;
	struct map_session_data *tsd;
	struct status_change *tsc;
	unsigned int heal_capacity = 0;
	int64 heal_amount = retVal;

	if (!heal || retVal <= 0)
		return retVal;

	if (src == NULL || target == NULL || src->type != BL_PC || target->type != BL_PC || src == target || status->isimmune(target))
		return retVal;	sd = BL_CAST(BL_PC, src);
	tsd = BL_CAST(BL_PC, target);

	switch(skill_id) {
		case AB_HIGHNESSHEAL:
			heal_amount = heal_amount * (17 + 3 * (int64)(skill_lv)) / 10;
		case AL_HEAL:
		case HLIF_HEAL:
			break;
		default:
			return retVal;
	}
	tsc = status->get_sc(target);

	if (sd && tsd && sd->status.partner_id == tsd->status.char_id && (sd->job&MAPID_UPPERMASK) == MAPID_SUPER_NOVICE && sd->status.sex == 0)
		heal_amount = heal_amount * 2;

	if (tsc && tsc->data[SC_AKAITSUKI] && heal_amount && skill_id != HLIF_HEAL)
		heal_amount = ~heal_amount + 1;

	heal_capacity = status_fake_heal(target, heal_amount, 0, 0);
	if (heal_capacity > 0) {
		struct player_data *data = search_bg_map(sd, false);

		if (data && map_allowed_bg(sd->bl.m) && sd->bg_id) {
			if (sd->bg_id == tsd->bg_id) {
				add2limit_bg(data->bg.modified, data->bg.healing_done, heal_capacity, UINT_MAX);
			} else {
				add2limit_bg(data->bg.modified, data->bg.wrong_healing_done, heal_capacity, UINT_MAX);
			}
		}
	}
	return retVal;
}

/**
 * Calculate Rankings for All Types of Requirements
 * type&2: consume items (after skill was used)
 * type&1: consume the others (before skill was used)
 * skill_consume_requirement PreHooked
 * @see skill_consume_requirement
 * @server MAP
 **/
int bg_record_requirement(struct map_session_data **sd, uint16 *skill_id, uint16 *skill_lv, short *type) {
	struct skill_condition req;
	struct player_data *data;

	nullpo_ret(*sd);
	data = search_bg_map(*sd, false);

	if (!map_allowed_bg((*sd)->bl.m) || !(*sd)->bg_id || data == NULL)
		return 0;

	req = skill->get_requirement((*sd), *skill_id, *skill_lv);
	if ((*type)&1) {
		switch( *skill_id ) {
			case CG_TAROTCARD:
			case MC_IDENTIFY:
				req.sp = 0;
				break;
			default:
				if ((*sd)->auto_cast_current.type != AUTOCAST_NONE)
					req.sp = 0;
				break;
		}
		if (req.sp > 0) {
			unsigned int sp = req.sp;
			add2limit_bg(data->bg.modified, data->bg.sp_used, sp, UINT_MAX);
		}
		if (req.spiritball > 0) {
			unsigned int sb = req.spiritball;
			add2limit_bg(data->bg.modified, data->bg.spiritb_used, sb, UINT_MAX);
		}
		if (req.zeny > 0 && (*skill_id) != NJ_ZENYNAGE) {
			unsigned int zeny = req.zeny;
			if ((int)(*sd)->status.zeny < req.zeny)
				req.zeny = (*sd)->status.zeny;
			add2limit_bg(data->bg.modified, data->bg.zeny_used, zeny, UINT_MAX);
		}
	}
	return 1;
}

/**
 * Calculate Rankings for Item Requirements
 * pc_delitem PreHooked
 * @see pc_delitem
 * @server MAP
 **/
int bg_record_requirement_item(struct map_session_data **sd, int *n, int *amount, int *type, enum delitem_reason *reason, e_log_pick_type *log_type) {
	struct player_data *data;
	unsigned int amt = *amount;
	nullpo_retr(1, *sd);

	if ((*reason) != DELITEM_SKILLUSE)
		return 1;

	if ((*sd)->status.inventory[*n].nameid == 0 || *amount <= 0 || (*sd)->status.inventory[*n].amount < *amount || (*sd)->inventory_data[*n] == NULL)
		return 1;

	data = search_bg_map(*sd, false);
	if (!map_allowed_bg((*sd)->bl.m) || !(*sd)->bg_id || data == NULL)
		return 1;	switch ((*sd)->status.inventory[*n].nameid) {
		case ITEMID_POISON_BOTTLE:
			add2limit_bg(data->bg.modified, data->bg.poison_bottles, amt, UINT_MAX);
			break;
		case ITEMID_YELLOW_GEMSTONE:
			add2limit_bg(data->bg.modified, data->bg.yellow_gemstones, amt, UINT_MAX);
			break;
		case ITEMID_RED_GEMSTONE:
			add2limit_bg(data->bg.modified, data->bg.red_gemstones, amt, UINT_MAX);
			break;
		case ITEMID_BLUE_GEMSTONE:
			add2limit_bg(data->bg.modified, data->bg.blue_gemstones, amt, UINT_MAX);
			break;
	}
	return 0;
}

/**
 * Calculate HP/SP Potions Used
 * pc_itemheal PreHooked
 * @see pc_itemheal
 * @server MAP
 **/
int bg_record_potion_used(struct map_session_data **sd, int *itemid, int *hp, int *sp) {
	struct player_data *data;

	if (*hp <= 0 && *sp <= 0)
		return 0;

	if (*sd == NULL)
		return 0;

	data = search_bg_map(*sd, false);

	if (!map_allowed_bg((*sd)->bl.m) || !(*sd)->bg_id || data == NULL)
		return 0;

	if (*hp > 0) {
		add2limit_bg(data->bg.modified, data->bg.hp_heal_potions, 1, UINT_MAX);
	}
	if (*sp > 0) {
		add2limit_bg(data->bg.modified, data->bg.sp_heal_potions, 1, UINT_MAX);
	}
	return 1;
}

/**
 * Adds deserter count when player leaves.
 **/
int bg_npc_script_event(int retVal, struct map_session_data *sd, enum npce_event type) {
	struct player_data *data = search_bg_map(sd, true);
	if (data != NULL && type == NPCE_LOGOUT && map->list[sd->bl.m].flag.battleground) {
		data->bg.deserter++;
		data->bg.modified = true;
	}
	return retVal;
}

/**
 * Records Support Skill Used
 * skill_castend_nodamage_id PostHooked
 * @see skill_castend_nodamage_id
 * @server MAP
 **/
int bg_record_support_skill(int retVal, struct block_list *src, struct block_list *bl, uint16 skill_id, uint16 skill_lv, int64 tick, int flag) {
	struct map_session_data *sd, *dstsd;
	struct status_data *tstatus;
	struct player_data *data;

	if (retVal != 0)
		return retVal;

	if (skill_id > 0 && !skill_lv)
		return retVal;

	nullpo_retr(1, src);
	nullpo_retr(1, bl);

	if (src->m != bl->m)
		return retVal;

	sd = BL_CAST(BL_PC, src);
	if (sd == NULL)
		return retVal;

	data = search_bg_map(sd, false);
	if (sd == NULL || data == NULL || !map_allowed_bg(sd->bl.m) || !sd->bg_id)
		return retVal;

	dstsd = BL_CAST(BL_PC, bl);

	// Supportive skills that can't be cast in users with mado
	if (sd && dstsd && pc_ismadogear(dstsd)) {
		switch( skill_id ) {
			case AL_HEAL:
			case AL_INCAGI:
			case AL_DECAGI:
			case AB_RENOVATIO:
			case AB_HIGHNESSHEAL:
				return retVal;
			default:
				break;
		}
	}

	tstatus = status->get_status_data(bl);
	// Check for undead skills that convert a no-damage skill into a damage one. [Skotlex]
	switch (skill_id) {
		case AL_HEAL:
		/**
		 * Arch Bishop
		 **/
		case AB_RENOVATIO:
		case AB_HIGHNESSHEAL:
		case AL_INCAGI:
		case ALL_RESURRECTION:
		case PR_ASPERSIO:
			// Apparently only player casted skills can be offensive like this.
			if (sd && battle->check_undead(tstatus->race,tstatus->def_ele) && skill_id != AL_INCAGI) {
				return retVal;
			}
			break;
		case SO_ELEMENTAL_SHIELD:
		case NPC_SMOKING:
		case MH_STEINWAND:
			return 0;
		case RK_MILLENNIUMSHIELD:
		case RK_CRUSHSTRIKE:
		case RK_REFRESH:
		case RK_GIANTGROWTH:
		case RK_STONEHARDSKIN:
		case RK_VITALITYACTIVATION:
		case RK_STORMBLAST:
		case RK_FIGHTINGSPIRIT:
		case RK_ABUNDANCE:
			if (sd && !pc->checkskill(sd, RK_RUNEMASTERY)) {
				return retVal;
			}
			break;
		default:
			if (skill->castend_nodamage_id_undead_unknown(src, bl, &skill_id, &skill_lv, &tick, &flag)) { // Skill is actually ground placed.
				if (src == bl && skill->get_unit_id(skill_id, skill_lv, 0))
					return retVal;
			}
			break;
	}

	if (skill->get_inf(skill_id)&INF_SUPPORT_SKILL && sd && dstsd && sd != dstsd) {
		if (sd->bg_id == dstsd->bg_id) {
			add2limit_bg(data->bg.modified, data->bg.support_skills_used, 1, UINT_MAX);
		} else {
			add2limit_bg(data->bg.modified, data->bg.wrong_support_skills_used, 1, UINT_MAX);
		}
	}
	return retVal;
}

/**
 * Saves the BG Data
 * Sends PACKET_MC_SAVE_DATA_BG
 * @server MAP
 * @method chrif_save
 * @param  sd         Player Session Data
 * @param  flag       Flag(no Use)
 * @return            0
 */
bool bg_chrif_save_post(bool retVal, struct map_session_data *sd, int flag) {
	struct player_data *data = search_bg_map(sd, false);
	int len = 10 + sizeof(struct s_bgstats);

	if (data == NULL)
		return retVal;

	WFIFOHEAD(chrif->fd, len);
	WFIFOW(chrif->fd, 0) = PACKET_MC_SAVE_DATA_BG;
	WFIFOL(chrif->fd, 2) = sd->status.account_id;
	WFIFOL(chrif->fd, 6) = sd->status.char_id;
	memcpy(WFIFOP(chrif->fd, 10), &data->bg, sizeof(struct s_bgstats));
	WFIFOSET(chrif->fd, len);
	data->bg.modified = false;
	return retVal&true;
}

/**
 * Packet: PACKET_MC_SAVE_DATA_BG
 * @server CHAR
 * @method char_save_bgdata_tosql
 * @param  fd                FD
 */
void char_save_bgdata_tosql(int fd) {
	struct player_data *data = NULL;
	int char_id = RFIFOL(fd, 6);
	struct s_bgstats bg_stats;

	memcpy(&bg_stats, RFIFOP(fd, 10), sizeof(struct s_bgstats));
	if (bg_stats.modified == true) {
		bg_stats.modified = false;
		if (SQL_ERROR == SQL->Query(inter->sql_handle, "REPLACE INTO `char_bg_statistics` (`char_id`, `top_damage`, `damage_done`, `damage_received`, `boss_damage`, "
			"`sp_heal_potions`, `hp_heal_potions`, `yellow_gemstones`, `red_gemstones`, `blue_gemstones`, `poison_bottles`, "
			"`acid_demostration`, `acid_demostration_fail`, `support_skills_used`, `healing_done`, `wrong_support_skills_used`, `wrong_healing_done`, "
			"`sp_used`, `zeny_used`, `spiritb_used`, `ammo_used`, "
			"`kill_count`, `death_count`, `score`, `points`, `win`, `lost`, `tie`, `leader_win`, `leader_lost`, `leader_tie`, `deserter`, `rank_games`, `rank_points`, "
			"`ti_skulls`, `ti_wins`, `ti_lost`, `ti_tie`, `eos_flags`, `eos_bases`, `eos_wins`, `eos_lost`, `eos_tie`, "
			"`boss_killed`, `boss_flags`, `boss_wins`, `boss_lost`, `boss_tie`, "
			"`dom_bases`, `dom_atk_kills`, `dom_def_kills`, `dom_wins`, `dom_lost`, `dom_tie`, "
			"`td_kills`, `td_deaths`, `td_wins`, `td_lost`, `td_tie`, "
			"`sc_stole`, `sc_captured`, `sc_dropped`, `sc_wins`, `sc_lost`, `sc_tie`, "
			"`ctf_taken`, `ctf_captured`, `ctf_dropped`, `ctf_wins`, `ctf_lost`, `ctf_tie`, "
			"`cq_emperium_kill`, `cq_barricade_kill`, `cq_gstone_kill`, `cq_wins`, `cq_lost`, "
			"`ru_captures`, `ru_wins`, `ru_lost`, `week_num`, `year`) "
			"VALUES ('%d', '%"PRIu64"', '%"PRIu64"', '%"PRIu64"', '%"PRIu64"', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u', WEEK(CURDATE()), YEAR(CURDATE()))",
			char_id, bg_stats.top_damage, bg_stats.damage_done, bg_stats.damage_received, bg_stats.boss_damage, 
			bg_stats.sp_heal_potions, bg_stats.hp_heal_potions, bg_stats.yellow_gemstones, bg_stats.red_gemstones, bg_stats.blue_gemstones, bg_stats.poison_bottles, 
			bg_stats.acid_demostration, bg_stats.acid_demostration_fail, bg_stats.support_skills_used, bg_stats.healing_done, bg_stats.wrong_support_skills_used, bg_stats.wrong_healing_done, 
			bg_stats.sp_used, bg_stats.zeny_used, bg_stats.spiritb_used, bg_stats.ammo_used, 
			bg_stats.kill_count, bg_stats.death_count, bg_stats.score, bg_stats.points, bg_stats.win, bg_stats.lost, bg_stats.tie, 
			bg_stats.leader_win, bg_stats.leader_lost, bg_stats.leader_tie, bg_stats.deserter, bg_stats.rank_games, bg_stats.rank_points,
			bg_stats.ti_skulls, bg_stats.ti_wins, bg_stats.ti_lost, bg_stats.ti_tie, bg_stats.eos_flags, bg_stats.eos_bases, bg_stats.eos_wins, bg_stats.eos_lost, bg_stats.eos_tie, 
			bg_stats.boss_killed, bg_stats.boss_flags, bg_stats.boss_wins, bg_stats.boss_lost, bg_stats.boss_tie,
			bg_stats.dom_bases, bg_stats.dom_atk_kills, bg_stats.dom_def_kills, bg_stats.dom_wins, bg_stats.dom_lost, bg_stats.dom_tie,
			bg_stats.td_kills, bg_stats.td_deaths, bg_stats.td_wins, bg_stats.td_lost, bg_stats.td_tie,
			bg_stats.sc_stole, bg_stats.sc_captured, bg_stats.sc_dropped, bg_stats.sc_wins, bg_stats.sc_lost, bg_stats.sc_tie,
			bg_stats.ctf_taken, bg_stats.ctf_captured, bg_stats.ctf_dropped, bg_stats.ctf_wins, bg_stats.ctf_lost, bg_stats.ctf_tie, 
			bg_stats.cq_emperium_kill, bg_stats.cq_barricade_kill, bg_stats.cq_gstone_kill, bg_stats.cq_wins, bg_stats.cq_lost,
			bg_stats.ru_captures, bg_stats.ru_wins, bg_stats.ru_lost)) {
			Sql_ShowDebug(inter->sql_handle);
			ShowError("Error Saving BG Data for Character %d.\n", char_id);
			return;
		}
	}
	ShowStatus("char_save_bgdata_tosql: BG Stats Saved for Character %d.\n", char_id);
	return;
}

BUILDIN(bg_addpoint) {
	struct player_data* data = NULL;
	struct map_session_data* sd = NULL;
	struct battleground_data* bgd = NULL;
	struct s_bgstats bgstat;
	const char* type;
	unsigned int amount;
	if (script_hasdata(st, 4))
		sd = map->id2sd(script_getnum(st, 4));
	else
		sd = script->rid2sd(st);
	bgd = bg->team_search(sd->bg_id);
	data = search_bg_map(sd, false);
	if (bgd == NULL) {
		ShowError("buildin_script_bg_addpoint: Character %s (%d) battleground data not found.\n", sd->status.name, sd->status.char_id);
		return false;
	}
	if (data == NULL) {
		ShowError("buildin_script_bg_addpoint: cannot allocate memory for bgdata.\n");
		return false;
	}
	memcpy(&bgstat, &data->bg, sizeof(struct s_bgstats));
	type = script_getstr(st, 2);
	amount = script_getnum(st, 3);
	if (!strcmpi(type, "eos_flags")) {
		add2limit_bg(data->bg.modified, data->bg.eos_flags, amount, UINT_MAX);
	} else if (!strcmpi(type, "dom_atk_kills")) {
		add2limit_bg(data->bg.modified, data->bg.dom_atk_kills, amount, UINT_MAX);
	} else if (!strcmpi(type, "dom_def_kills")) {
		add2limit_bg(data->bg.modified, data->bg.dom_def_kills, amount, UINT_MAX);
	} else if (!strcmpi(type, "sc_stole")) {
		add2limit_bg(data->bg.modified, data->bg.sc_stole, amount, UINT_MAX);
	} else if (!strcmpi(type, "sc_captured")) {
		add2limit_bg(data->bg.modified, data->bg.sc_captured, amount, UINT_MAX);
	} else if (!strcmpi(type, "sc_dropped")) {
		add2limit_bg(data->bg.modified, data->bg.sc_dropped, amount, UINT_MAX);
	} else if (!strcmpi(type, "ctf_taken")) {
		add2limit_bg(data->bg.modified, data->bg.ctf_taken, amount, UINT_MAX);
	} else if (!strcmpi(type, "ctf_captured")) {
		add2limit_bg(data->bg.modified, data->bg.ctf_captured, amount, UINT_MAX);
	} else if (!strcmpi(type, "ctf_dropped")) {
		add2limit_bg(data->bg.modified, data->bg.ctf_dropped, amount, UINT_MAX);
	} else if (!strcmpi(type, "ti_skulls")) {
		add2limit_bg(data->bg.modified, data->bg.ti_skulls, amount, UINT_MAX);
	}
	data->bg.modified = true;
	return true;
}

BUILDIN(bg_addpoint_area) {
	struct player_data* data = NULL;
	struct map_session_data* sd = NULL;
	struct battleground_data* bgd = NULL;
	struct s_bgstats bgstat;
	const char* type, *map_name;
	unsigned int amount;
	int16 m;
	short x0 = 0, y0 = 0, x1 = 0, y1 = 0;
	int bg_id, i;

	bg_id = script_getnum(st, 2);
	map_name = script_getstr(st, 3);

	if ((bgd = bg->team_search(bg_id)) == NULL || (m = map->mapname2mapid(map_name)) < 0) {
		script_pushint(st, 0);
		return true;
	}

	x0 = script_getnum(st, 4);
	y0 = script_getnum(st, 5);
	x1 = script_getnum(st, 6);
	y1 = script_getnum(st, 7);
	type = script_getstr(st, 8);
	amount = script_getnum(st, 9);

	for (i = 0; i < MAX_BG_MEMBERS; i++) {
		if ((sd = bgd->members[i].sd) == NULL)
			continue;

		if (sd->bl.m != m || sd->bl.x < x0 || sd->bl.y < y0 || sd->bl.x > x1 || sd->bl.y > y1)
			continue;

		data = search_bg_map(sd, false);

		if (data == NULL)
			continue;
		memcpy(&bgstat, &data->bg, sizeof(struct s_bgstats));
		if (!strcmpi(type, "eos_bases")) {
			add2limit_bg(data->bg.modified, data->bg.eos_bases, amount, UINT_MAX);
		} else if (!strcmpi(type, "dom_bases")) {
			add2limit_bg(data->bg.modified, data->bg.dom_bases, amount, UINT_MAX);
		}
		data->bg.modified = true;
	}

	return true;
}

void bg_team_rewards(int bg_id, int nameid, int amount, int kafrapoints, unsigned int add_value, int bg_arena, int bg_result) {
	struct player_data *data = NULL;
	struct s_bgstats bgstat;
	struct map_session_data* sd;
	struct battleground_data* bgd = NULL;
	struct item_data* id;
	struct item it;
	int j, flag;

	if (amount < 1 || (bgd = bg->team_search(bg_id)) == NULL || (id = itemdb->exists(nameid)) == NULL)
		return;

	if (battle->bc->bg_reward_rate != 100) {	// Battlegrounds Reward Rates
		amount = amount * battle->bc->bg_reward_rate / 100;
		kafrapoints = kafrapoints * battle->bc->bg_reward_rate / 100;
	}

	bg_result = cap_value(bg_result, 0, 2);
	memset(&it, 0, sizeof(it));

	if (nameid == ITEMID_BATTLEGROUND_BADGE) {
		it.nameid = nameid;
		it.identify = 1;
	}
	else
		nameid = 0;

	for (j = 0; j < MAX_BG_MEMBERS; j++) {
		if ((sd = bgd->members[j].sd) == NULL)
			continue;

		if (battle->bc->bg_reward_afk && !sd->state.bg_afk)
			continue;

		if (kafrapoints > 0)
			pc->getcash(sd, 0, kafrapoints);

		if (nameid && amount > 0) {
			if ((flag = pc->additem(sd, &it, amount, LOG_TYPE_NPC)))
				clif->additem(sd, 0, 0, flag);
		}

		data = search_bg_map(sd, false);
		if (data == NULL)
			continue;

		memcpy(&bgstat, &data->bg, sizeof(struct s_bgstats));
		switch (bg_result) {
			case BG_R_WIN:
				add2limit_bg(data->bg.modified, data->bg.win, add_value, UINT_MAX);
				if (sd->state.bg_leader)
					add2limit_bg(data->bg.modified, data->bg.leader_win, add_value, UINT_MAX);
				pc_addpoint_bg(sd, 100, 0, sd->state.bg_leader, 25);
				switch (bg_arena) {
					case BG_CQ: add2limit_bg(data->bg.modified, data->bg.cq_wins, add_value, UINT_MAX); break;
					case BG_RUSH: add2limit_bg(data->bg.modified, data->bg.ru_wins, add_value, UINT_MAX); break;
					case BG_TDM: add2limit_bg(data->bg.modified, data->bg.td_wins, add_value, UINT_MAX); break;
					case BG_CTF: add2limit_bg(data->bg.modified, data->bg.ctf_wins, add_value, UINT_MAX); break;
					case BG_SC: add2limit_bg(data->bg.modified, data->bg.sc_wins, add_value, UINT_MAX); break;
					case BG_BOSS: add2limit_bg(data->bg.modified, data->bg.boss_wins, add_value, UINT_MAX); break;
					case BG_DOM: add2limit_bg(data->bg.modified, data->bg.dom_wins, add_value, UINT_MAX); break;
					case BG_EOS: add2limit_bg(data->bg.modified, data->bg.eos_wins, add_value, UINT_MAX); break;
					case BG_TI: add2limit_bg(data->bg.modified, data->bg.ti_wins, add_value, UINT_MAX); break;
				}
				break;
			case BG_R_TIE:
				add2limit_bg(data->bg.modified, data->bg.tie, add_value, UINT_MAX);
				if (sd->state.bg_leader)
					add2limit_bg(data->bg.modified, data->bg.leader_tie, add_value, UINT_MAX);
				pc_addpoint_bg(sd, 75, 0, sd->state.bg_leader, 10);
				switch (bg_arena) {
					case BG_TDM: add2limit_bg(data->bg.modified, data->bg.td_tie, add_value, UINT_MAX); break;
					case BG_CTF: add2limit_bg(data->bg.modified, data->bg.ctf_tie, add_value, UINT_MAX); break;
					case BG_SC: add2limit_bg(data->bg.modified, data->bg.sc_tie, add_value, UINT_MAX); break;
					case BG_BOSS: add2limit_bg(data->bg.modified, data->bg.boss_tie, add_value, UINT_MAX); break;
					case BG_DOM: add2limit_bg(data->bg.modified, data->bg.dom_tie, add_value, UINT_MAX); break;
					case BG_EOS: add2limit_bg(data->bg.modified, data->bg.eos_tie, add_value, UINT_MAX); break;
					case BG_TI: add2limit_bg(data->bg.modified, data->bg.ti_tie, add_value, UINT_MAX); break;
				}
				break;
			case BG_R_LOSE:
				add2limit_bg(data->bg.modified, data->bg.lost, add_value, UINT_MAX);
				if (sd->state.bg_leader)
					add2limit_bg(data->bg.modified, data->bg.leader_lost, add_value, UINT_MAX);
				pc_addpoint_bg(sd, 50, 0, sd->state.bg_leader, 0);
				switch (bg_arena) {
					case BG_CQ: add2limit_bg(data->bg.modified, data->bg.cq_lost, add_value, UINT_MAX); break;
					case BG_RUSH: add2limit_bg(data->bg.modified, data->bg.ru_lost, add_value, UINT_MAX); break;
					case BG_TDM: add2limit_bg(data->bg.modified, data->bg.td_lost, add_value, UINT_MAX); break;
					case BG_CTF: add2limit_bg(data->bg.modified, data->bg.ctf_lost, add_value, UINT_MAX); break;
					case BG_SC: add2limit_bg(data->bg.modified, data->bg.sc_lost, add_value, UINT_MAX); break;
					case BG_BOSS: add2limit_bg(data->bg.modified, data->bg.boss_lost, add_value, UINT_MAX); break;
					case BG_DOM: add2limit_bg(data->bg.modified, data->bg.dom_lost, add_value, UINT_MAX); break;
					case BG_EOS: add2limit_bg(data->bg.modified, data->bg.eos_lost, add_value, UINT_MAX); break;
					case BG_TI: add2limit_bg(data->bg.modified, data->bg.ti_lost, add_value, UINT_MAX); break;
				}
				break;
		}
		data->bg.modified = true;
	}
}

static BUILDIN(bg_reward) {
	int bg_id, nameid, amount, kafrapoints, bg_arena, bg_result;
	unsigned int add_value;
	bg_id = script_getnum(st, 2);
	nameid = script_getnum(st, 3);
	amount = script_getnum(st, 4);
	kafrapoints = script_getnum(st, 5);
	add_value = script_getnum(st, 6);
	bg_arena = script_getnum(st, 7);
	bg_result = script_getnum(st, 8);
	bg_team_rewards(bg_id, nameid, amount, kafrapoints, add_value, bg_arena, bg_result);
	return true;
}

void char_p_fame_data_bg(int fd) {
	int num, size = RFIFOL(fd, 2);
	int total = 0, len = 8;
	int type = RFIFOW(fd, 6);

	if (type == 1) {
		memset(bg_fame_list, 0, sizeof(bg_fame_list));
		for (num = 0; len < size && num < fame_list_size_bg; num++) {
			memcpy(&bg_fame_list[num], RFIFOP(fd, len), sizeof(struct fame_list));
			len += sizeof(struct fame_list);
			total += 1;
		}
		ShowInfo("Received Fame List of '"CL_WHITE"%d"CL_RESET"' characters (BG Ranking).\n", total);
	}
}

int load_playername(int char_id, char* name) {
	char* data;
	size_t len;

	if (SQL_ERROR == SQL->Query(map->mysql_handle, "SELECT `name` FROM `char` WHERE `char_id`='%d'", char_id))
		Sql_ShowDebug(map->mysql_handle);
	else if (SQL_SUCCESS == SQL->NextRow(map->mysql_handle)) {
		SQL->GetData(map->mysql_handle, 0, &data, &len);
		safestrncpy(name, data, NAME_LENGTH);
		return 1;
	} else 
		sprintf(name, "Unknown");

	return 0;
}

bool update_bg_ranking(struct map_session_data* sd, int type_) {
	int cid = sd->status.char_id, points, size, player_pos, fame_pos;
	struct player_data* data = search_bg_map(sd, true);
	struct fame_list *list;
	bool changed = false;

	if (data == NULL)
		return changed;

	switch (type_) {
		case 1:
			points = data->bg.points;
			size = fame_list_size_bg;
			list = bg_fame_list;
			break;
	}

	ARR_FIND(0, size, player_pos, list[player_pos].id == cid);
	ARR_FIND(0, size, fame_pos, list[fame_pos].fame <= points);

	if (player_pos == size && fame_pos == size) {
		// Not on list
	}
	else if (fame_pos == player_pos) {
		// Same Position
		list[player_pos].fame = points;
	} else {
		// Move in the list
		if (player_pos == size) {
			// New ranker
			ARR_MOVE(size - 1, fame_pos, list, struct fame_list);
			list[fame_pos].id = cid;
			list[fame_pos].fame = points;
			load_playername(cid, list[fame_pos].name);
		} else {
			// Already in list
			if (fame_pos == size)
				--fame_pos;
			ARR_MOVE(player_pos, fame_pos, list, struct fame_list);
			list[fame_pos].fame = points;
		}
		changed = true;
	}
	return changed;
}

int bg_fame_receive(void) {
	if (SERVER_TYPE == SERVER_TYPE_CHAR) {
		if (!chrif->isconnected())
			return 0;
		WFIFOHEAD(chrif->fd, 4);
		WFIFOW(chrif->fd, 0) = PACKET_MC_REQ_FAME_DATA_BG;
		WFIFOW(chrif->fd, 2) = 3;
		WFIFOSET(chrif->fd, 4);
	}
	else {
#define min_num(a, b) ((a) < (b) ? (a) : (b))
		int i;
		char *data;
		size_t len;
		memset(bg_fame_list, 0, sizeof(bg_fame_list));
		if (SQL_ERROR == SQL->Query(map->mysql_handle, "SELECT `bg`.`char_id`, `bg`.`points`, `ch`.`name` FROM `char_bg_statistics` bg LEFT JOIN `char` ch ON `ch`.`char_id`=`bg`.char_id WHERE `bg`.`points` > '0' ORDER BY `bg`.`points` DESC LIMIT 0, %d", fame_list_size_bg))
			Sql_ShowDebug(map->mysql_handle);
		for (i = 0; i < fame_list_size_bg && SQL_SUCCESS == SQL->NextRow(map->mysql_handle); ++i) {
			SQL->GetData(map->mysql_handle, 0, &data, NULL); bg_fame_list[i].id = atoi(data);
			SQL->GetData(map->mysql_handle, 1, &data, NULL); bg_fame_list[i].fame = atoi(data);
			SQL->GetData(map->mysql_handle, 2, &data, &len);
			if (data == NULL)
				data = "Name-NULL";
			memcpy(bg_fame_list[i].name, data, min_num(len, NAME_LENGTH));
		}
		SQL->FreeResult(map->mysql_handle);
#undef min_num
	}
	return 1;
}

void update_bg_fame(int *fd) {
	bg_fame_receive();
}

void chrif_on_ready_post(void) {
	bg_fame_receive();
}

ACMD(bgrank) {
	int i;
	char output[CHAT_SIZE_MAX];
	struct player_data* data = search_bg_map(sd, true);
	clif->messagecolor_self(fd, 0xddcd48, "========== BATTLEGROUND RANK ==========");
	for (i = 0; i < MAX_FAME_LIST; i++) {
		if (!bg_fame_list[i].id)
			sprintf(output, "[ %2d ] %-20s :   %d POINT", (i + 1), "None", 0);
		else
			sprintf(output, "[ %2d ] %-20s :   %d POINT", (i + 1), bg_fame_list[i].name, bg_fame_list[i].fame);
		clif->messagecolor_self(fd, 0xebdd66, output);
	}
	clif->messagecolor_self(fd, 0xddcd48, "===========================================");
	sprintf(output, "MY POINT   :   %d POINT", (data == NULL ? 0 : data->bg.points));
	clif->messagecolor_self(fd, 0xebdd66, output);
	clif->messagecolor_self(fd, 0xddcd48, "===========================================");
	return true;
}

/* Server Startup */
HPExport void plugin_init(void) {
	if (SERVER_TYPE == SERVER_TYPE_CHAR) {
		addPacket(PACKET_MC_REQ_DATA_BG, 10, char_load_bgdata_fromsql, hpParse_FromMap);
		addPacket(PACKET_MC_SAVE_DATA_BG, 10 + sizeof(struct s_bgstats), char_save_bgdata_tosql, hpParse_FromMap);
		addPacket(PACKET_CM_FAME_DATA_BG, -1, char_p_fame_data_bg, hpChrif_Parse);
	} else {
		// Deserter Counter
		addHookPost(npc, script_event, bg_npc_script_event);
		// On Ready
		addHookPost(chrif, on_ready, chrif_on_ready_post);
		// Get Data from Char
		addHookPre(clif, pLoadEndAck, clif_parse_LoadEndAck_pre);
		// Skill Record
		addHookPre(clif, useskill, bg_record_skill);
		addHookPre(unit, attack, bg_record_bare_attack);
		// Ammo Consumed
		addHookPre(battle, consume_ammo, bg_record_ammo_spent);
		// Damage Record
		addHookPre(clif, skill_damage, bg_record_max_damage);
		addHookPre(clif, damage, bg_record_max_damage2);
		addHookPre(mob, damage, bg_record_damage2);
		addHookPre(mob, dead, mob_dead_pre);
		// Ranking
		addHookPre(pc, dead, pc_dead_pre);
		// Healing Record
		addHookPost(skill, calc_heal, bg_record_healing);
		// Record Requirements (SP/Zeny/Spirit Ball)
		addHookPre(skill, consume_requirement, bg_record_requirement);
		// Record Requirements (Gemstones)
		addHookPre(pc, delitem, bg_record_requirement_item);
		// Record Potion Usage
		addHookPre(pc, itemheal, bg_record_potion_used);
		// Support Skill
		addHookPost(skill, castend_nodamage_id, bg_record_support_skill);
		// Save
		addHookPost(chrif, save, bg_chrif_save_post);
		addHookPre(chrif, recvfamelist, update_bg_fame);
		// Get Packet
		addPacket(PACKET_CM_DATA_BG, 10 + sizeof(struct s_bgstats), map_bgdata_from_char, hpChrif_Parse);

		addScriptCommand("bg_addpoint", "si?", bg_addpoint);
		addScriptCommand("bg_addpoint_area", "isiiiisi", bg_addpoint_area);
		addScriptCommand("bg_reward", "iiiiiii", bg_reward);

		addAtcommand("bgrank", bgrank);

		script->constdb_comment("battleground mode");
		script->set_constant("BG_CQ", BG_CQ, false, false);
		script->set_constant("BG_RUSH", BG_RUSH, false, false);
		script->set_constant("BG_TDM", BG_TDM, false, false);
		script->set_constant("BG_CTF", BG_CTF, false, false);
		script->set_constant("BG_SC", BG_SC, false, false);
		script->set_constant("BG_BOSS", BG_BOSS, false, false);
		script->set_constant("BG_DOM", BG_DOM, false, false);
		script->set_constant("BG_EOS", BG_EOS, false, false);
		script->set_constant("BG_TI", BG_TI, false, false);

		script->constdb_comment("battleground results");
		script->set_constant("BG_R_WIN", BG_R_WIN, false, false);
		script->set_constant("BG_R_TIE", BG_R_TIE, false, false);
		script->set_constant("BG_R_LOSE", BG_R_LOSE, false, false);
	}
}

HPExport void server_online (void) {
	ShowInfo ("'%s' Plugin by fTakano. Version '%s'\n", pinfo.name, pinfo.version);
}
