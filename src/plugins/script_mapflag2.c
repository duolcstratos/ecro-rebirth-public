
#include "common/hercules.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common/HPMi.h"
#include "common/memmgr.h"
#include "common/mmo.h"
#include "common/socket.h"
#include "common/strlib.h"
#include "common/timer.h"
#include "common/mapindex.h"
#include "common/utils.h"
#include "common/db.h"
#include "common/nullpo.h"
#include "map/battle.h"
#include "map/clif.h"
#include "map/script.h"
#include "map/skill.h"
#include "map/pc.h"
#include "map/npc.h"
#include "map/map.h"
#include "map/status.h"
#include "map/channel.h"
#include "map/itemdb.h"
#include "map/guild.h"
#include "plugins/HPMHooking.h"
#include "common/HPMDataCheck.h"

HPExport struct hplugin_info pinfo = {
	"script_mapflags2",			// Plugin name
	SERVER_TYPE_MAP,			// Which server types this plugin works with?
	"1.3a",						// Plugin version
	HPM_VERSION,				// HPM Version (don't change, macro is automatically updated)
};

enum p_mapflags {
	MF_GVG_NOALLAINCE = 100,
	MF_NOCONSUME,
	MF_NOEQUIP,
	MF_MACPROTECT,
	MF_NOVICEONLY,
	MF_LMS,
	MF_NOPARTY,
	MF_NOGUILD,
	MF_NOHIDING,
};

struct plugin_mapflag {
	unsigned gvg_noalliance : 1;
	unsigned noconsume : 1;
	unsigned noequip : 1;
	unsigned macprotect : 1;
	unsigned noviceonly : 1;
	unsigned lms : 1;
	unsigned noparty : 1;
	unsigned noguild : 1;
	unsigned nohiding : 1;
};

#define CREATE_MAPD(mapflag) \
		if (!(mf_data = getFromMAPD(&map->list[m], 0))) \
		{ \
			CREATE(mf_data,struct plugin_mapflag,1); \
			addToMAPD(&map->list[m], mf_data, 0, true); \
		} \
		init = true; \
		mf_data->mapflag = 1

uint16 GetWord(uint32 val, int idx) {
	switch(idx) {
	case 0: return (uint16)((val & 0x0000FFFF));
	case 1: return (uint16)((val & 0xFFFF0000) >> 0x10);
	default:
		return 0;
	}
}

void parse_mapflags(const char **name, const char **w3, const char **w4, const char **start, const char **buffer, const char **filepath, int **retval) {
	int16 m = map->mapname2mapid(*name);
	struct plugin_mapflag *mf_data;
	bool init = false;
	if (!strcmpi(*w3,"gvg_noalliance")) {
		CREATE_MAPD(gvg_noalliance);
	} else if (!strcmpi(*w3,"noconsume")) {
		CREATE_MAPD(noconsume);
	} else if (!strcmpi(*w3,"noequip")) {
		CREATE_MAPD(noequip);
	} else if (!strcmpi(*w3,"macprotect")) {
		CREATE_MAPD(macprotect);
	} else if (!strcmpi(*w3,"noviceonly")) {
		CREATE_MAPD(noviceonly);
	} else if (!strcmpi(*w3,"lms")) {
		CREATE_MAPD(lms);
	} else if (!strcmpi(*w3,"noparty")) {
		CREATE_MAPD(noparty);
	} else if (!strcmpi(*w3,"noguild")) {
		CREATE_MAPD(noguild);
	} else if (!strcmpi(*w3,"nohiding")) {
		CREATE_MAPD(nohiding);
	}
	if (init == true)
		hookStop();
	return;
}

static void map_zone_change2_pre(int *m_, struct map_zone_data **zone_) {
	const char *empty = "";
	struct map_zone_data *zone = *zone_;
	int m = *m_;
	struct plugin_mapflag *mf_data;

	if (zone == NULL)
		return;
	Assert_retv(m >= 0 && m < map->count);
	if (map->list[m].zone == zone)
		return;

	mf_data = getFromMAPD(&map->list[m], 0);

	if (mf_data) {
		if (mf_data->gvg_noalliance)
			mf_data->gvg_noalliance = 0;
		else if (mf_data->noconsume)
			mf_data->noconsume = 0;
		else if (mf_data->noequip)
			mf_data->noequip = 0;
		else if (mf_data->macprotect)
			mf_data->macprotect = 0;
		else if (mf_data->noviceonly)
			mf_data->noviceonly = 0;
		else if (mf_data->lms)
			mf_data->lms = 0;
		else if (mf_data->noparty)
			mf_data->noparty = 0;
		else if (mf_data->noguild)
			mf_data->noguild = 0;
		else if (mf_data->nohiding)
			mf_data->nohiding = 0;
	}

	if (!map->list[m].zone->info.merged) /* we don't update it for merged zones! */
		map->list[m].prev_zone = map->list[m].zone;

	if (map->list[m].zone_mf_count)
		map->zone_remove(m);

	if (zone->merge_type == MZMT_MERGEABLE && map->list[m].prev_zone->merge_type != MZMT_NEVERMERGE) {
		zone = map->merge_zone(zone,map->list[m].prev_zone);
	}

	map->zone_apply(m,zone,empty,empty,empty);
}

int check_alliance(int retVal, struct block_list *src, struct block_list *target, int flag) {
	struct block_list *s_bl = src, *t_bl = target;
	struct plugin_mapflag *mf_data;
	if (retVal != -1 || src==NULL || target == NULL)
		return retVal;
	if ((t_bl = battle->get_master(target)) == NULL)
		t_bl = target;
	if ((s_bl = battle->get_master(src)) == NULL)
		s_bl = src;
	if (s_bl->type == BL_PC && t_bl->type == BL_PC) {
		struct map_session_data *sd = BL_CAST(BL_PC, s_bl);
		int s_guild = status->get_guild_id(s_bl);
		int t_guild = status->get_guild_id(t_bl);
		mf_data = getFromMAPD(&map->list[sd->bl.m], 0);
		if (mf_data && s_guild && t_guild && guild->isallied(s_guild, t_guild) && mf_data->gvg_noalliance) 
			return 1;
	}
	return -1;
}

int noconsume_item(struct map_session_data **sd_, int *n) {
	struct map_session_data *sd = *sd_;
	struct plugin_mapflag *mf_data;

	if (sd == NULL)
		return 0;

	mf_data = getFromMAPD(&map->list[sd->bl.m], 0);
	
	if (mf_data && mf_data->noconsume) {
		clif->message(sd->fd, "You can't consume any usable items in this map.");
		hookStop();
		return 0;
	}
	return 1;
}

int pc_checkequip(int retVal, struct map_session_data* sd_, unsigned short map_index, int x, int y, enum clr_type clrtype) {
	int16 m;
	int i;
	struct map_session_data* sd = sd_;
	struct plugin_mapflag* mf_data;

	if (sd == NULL)
		return 1;

	if (!map_index || !mapindex_id2name(map_index) || (m = map->mapindex2mapid(map_index)) == -1) {
		ShowDebug("pc_setpos: Passed mapindex(%d) is invalid!\n", map_index);
		return 1;
	}

	mf_data = getFromMAPD(&map->list[m], 0);

	if (mf_data && mf_data->noequip && pc_get_group_level(sd) < 10) {
		for (i = 0; i < EQI_MAX; i++) {
			if (sd->equip_index[i] >= 0)
				pc->unequipitem(sd, sd->equip_index[i], PCUNEQUIPITEM_RECALC|PCUNEQUIPITEM_FORCE);
		}
		clif->message(sd->fd, "All of your equipment has been stripped.");
	}
	return 0;
}

int noequip_allowed(struct map_session_data **sd_,int *n,int *req_pos) {
	struct plugin_mapflag *mf_data;
	struct map_session_data *sd = *sd_;
	struct item_data *item;

	if (sd == NULL)
		return 0;

	mf_data = getFromMAPD(&map->list[sd->bl.m], 0);

	if (*n < 0 || *n >= MAX_INVENTORY) {
		clif->equipitemack(sd,0,0,EIA_FAIL);
		return 0;
	}

	if (DIFF_TICK(sd->canequip_tick,timer->gettick()) > 0) {
		clif->equipitemack(sd,*n,0,EIA_FAIL);
		return 0;
	}

	item = sd->inventory_data[*n];

	if (mf_data && mf_data->noequip) {
		clif->message(sd->fd, "You can't wear any items in this map.");
		hookStop();
		return 0;
	}
	return 1;
}

int pc_count_unique(struct block_list *bl, va_list ap) {
	struct map_session_data* dstsd = (struct map_session_data*)bl;
	struct block_list* src = va_arg(ap, struct block_list*);
	struct map_session_data *sd = NULL;
	//unsigned int unique_id_src,unique_id_target;

	nullpo_ret(dstsd);
	nullpo_ret(src);

	sd = (struct map_session_data *)src;
	//unique_id_src = sockt->session[sd->fd]->gepard_info.unique_id;
	//unique_id_target = sockt->session[dstsd->fd]->gepard_info.unique_id;

	if (sd->bl.id == dstsd->bl.id)	//Same Person
		return 0;

	//if (unique_id_src != unique_id_target)	//UID Doesn't Match
	//	return 0;

	return 1;
}

int unique_id_check(struct map_session_data *sd, int16 map_id) {
	int c = 0;
	if (sd==NULL)
		return 0;

	c = map->foreachinmap(pc_count_unique, map_id , BL_PC, &sd->bl);
	return c;
}

int unique_id_check2(struct map_session_data* sd, const char *zone_name) {
	int d = 0;
	struct map_session_data* pl_sd;
	struct s_mapiterator* iter;
	nullpo_ret(sd);

	iter = mapit_getallusers();
	for (pl_sd = (TBL_PC*)mapit->first(iter); mapit->exists(iter); pl_sd = (TBL_PC*)mapit->next(iter)) {
		if (!(strcmp(map->list[pl_sd->bl.m].zone->name, zone_name) == 0))
			continue;

		//if (sockt->session[sd->fd]->gepard_info.unique_id == sockt->session[pl_sd->fd]->gepard_info.unique_id)
		//	d++;
	}
	mapit->free(iter);
	return d;
}

int pc_check_uid(struct map_session_data** sd_, unsigned short* map_index_, int* x, int* y, enum clr_type* clrtype_) {
	int16 m;
	unsigned short map_index = *map_index_;
	struct plugin_mapflag *mf_data;
	struct map_session_data *sd = *sd_;

	if (sd == NULL)
		return 1;

	if (!map_index || !mapindex_id2name(map_index) || (m = map->mapindex2mapid(map_index)) == -1) {
		ShowDebug("pc_setpos: Passed mapindex(%d) is invalid!\n", map_index);
		return 1;
	}
	
	mf_data = getFromMAPD(&map->list[m], 0);

	if (mf_data && mf_data->macprotect && unique_id_check(sd, m)) {
		clif->message(sd->fd, "Only 1 player with the same MAC address is allowed on this map.");
		hookStop();
		return 0;
	}

	return 0;
}

int pc_check_class(struct map_session_data** sd_, unsigned short* map_index_, int* x, int* y, enum clr_type* clrtype_) {
	int16 m;
	unsigned short map_index = *map_index_;
	struct plugin_mapflag* mf_data;
	struct map_session_data* sd = *sd_;

	nullpo_ret(sd);

	if (!map_index || !mapindex_id2name(map_index) || (m = map->mapindex2mapid(map_index)) == -1) {
		ShowDebug("pc_setpos: Passed mapindex(%d) is invalid!\n", map_index);
		return 1;
	}

	mf_data = getFromMAPD(&map->list[m], 0);

	if (mf_data && mf_data->noviceonly && ((sd->job & MAPID_BASEMASK) != MAPID_NOVICE || sd->status.base_level > 1) && !pc_has_permission(sd, PC_PERM_WARP_ANYWHERE)) {
		clif->message(sd->fd, "Only Level 1 Novices are allowed on this map.");
		hookStop();
		return 1;
	}

	if (mf_data && mf_data->lms && (sd->status.class == JOB_PALADIN || sd->status.class == JOB_CREATOR) && !pc_has_permission(sd, PC_PERM_WARP_ANYWHERE)) {
		clif->message(sd->fd, "Paladins and Creators are not allowed inside the Last Man Standing area.");
		hookStop();
		return 1;
	}

	if (mf_data && mf_data->noparty && sd->status.party_id > 0 && !pc_has_permission(sd, PC_PERM_WARP_ANYWHERE)) {
		clif->message(sd->fd, "Please leave to your party to access the map.");
		hookStop();
		return 1;
	}

	if (mf_data && mf_data->noguild && sd->status.guild_id > 0 && !pc_has_permission(sd, PC_PERM_WARP_ANYWHERE)) {
		clif->message(sd->fd, "Please leave to your guild to access the map."); 
		hookStop();
		return 1;
	}

	return 0;
}

void clif_check_uid(int fd, struct map_session_data* sd) {
	int16 m = -1;
	struct plugin_mapflag* mf_data;

	m = sd->bl.m;
	mf_data = getFromMAPD(&map->list[m], 0);

	if (mf_data && mf_data->macprotect && unique_id_check(sd, m)) {
		clif->message(sd->fd, "Only 1 player with the same MAC address is allowed on this map.");
		pc->setpos(sd, sd->status.save_point.map, sd->status.save_point.x, sd->status.save_point.y, CLR_TELEPORT);
		hookStop();
		return;
	}
	return;
}

void clif_check_class(int fd, struct map_session_data* sd) {
	int16 m = -1;
	struct plugin_mapflag* mf_data;

	m = sd->bl.m;
	mf_data = getFromMAPD(&map->list[m], 0);

	if (mf_data && mf_data->noviceonly && ((sd->job & MAPID_BASEMASK) != MAPID_NOVICE || sd->status.base_level > 1) && !pc_has_permission(sd, PC_PERM_WARP_ANYWHERE)) {
		clif->message(sd->fd, "Only Level 1 Novices are allowed on this map.");
		pc->setpos(sd, sd->status.save_point.map, sd->status.save_point.x, sd->status.save_point.y, CLR_TELEPORT);
		hookStop();
		return;
	}

	if (mf_data && mf_data->lms && (sd->status.class == JOB_PALADIN || sd->status.class == JOB_CREATOR) && !pc_has_permission(sd, PC_PERM_WARP_ANYWHERE)) {
		clif->message(sd->fd, "Paladins and Creators are not allowed inside the Last Man Standing area.");
		pc->setpos(sd, sd->status.save_point.map, sd->status.save_point.x, sd->status.save_point.y, CLR_TELEPORT);
		hookStop();
		return;
	}

	if (mf_data && mf_data->noparty && sd->status.party_id > 0 && !pc_has_permission(sd, PC_PERM_WARP_ANYWHERE)) {
		clif->message(sd->fd, "Please leave to your party to access the map.");
		pc->setpos(sd, sd->status.save_point.map, sd->status.save_point.x, sd->status.save_point.y, CLR_TELEPORT);
		hookStop();
		return;
	}

	if (mf_data && mf_data->noparty && sd->status.guild_id > 0 && !pc_has_permission(sd, PC_PERM_WARP_ANYWHERE)) {
		clif->message(sd->fd, "Please leave to your guild to access the map.");
		pc->setpos(sd, sd->status.save_point.map, sd->status.save_point.x, sd->status.save_point.y, CLR_TELEPORT);
		hookStop();
		return;
	}

	return;
}

static int skill_castend_nodamage_id_pre(struct block_list **src_, struct block_list **bl_, uint16 *skill_id_, uint16 *skill_lv_, int64 *tick, int *flag) {
	struct map_session_data *sd, *dstsd;
	struct block_list *src = *src_;
	struct block_list *bl = *bl_;
	struct status_change *tsc;
	struct status_change_entry *tsce;
	uint16 skill_id = *skill_id_;
	uint16 skill_lv = *skill_lv_;
	enum sc_type type;
	struct plugin_mapflag* mf_data;

	if (skill_id > 0 && !skill_lv) {
		hookStop();
		return 0;
	}

	nullpo_retr(1, src);
	nullpo_retr(1, bl);

	if (src->m != bl->m) {
		hookStop();
		return 1;
	}

	sd = BL_CAST(BL_PC, src);
	dstsd = BL_CAST(BL_PC, bl);

	if (bl->prev == NULL) {
		hookStop();
		return 1;
	}
	if (status->isdead(src)) {
		hookStop();
		return 1;
	}

	type = skill->get_sc_type(skill_id);
	tsc = status->get_sc(bl);
	tsce = (tsc != NULL && type != SC_NONE) ? tsc->data[type] : NULL;

	mf_data = getFromMAPD(&map->list[bl->m], 0);

	switch (skill_id) {
		case TF_HIDING:
		case ST_CHASEWALK:
		case AS_CLOAKING:
			if (mf_data && mf_data->nohiding && sd) {
				char output[CHAT_SIZE_MAX];
				sprintf(output, "Cannot use [%s] due to its restriction on this area.", skill->get_desc(skill_id));
				clif->messagecolor_self(sd->fd, COLOR_RED, output);
				hookStop();
			}
			break;
	}
	return 0;
}

ACMD(mapflag2) {
#define CHECKFLAG(cmd) do { if (mf_data && mf_data->cmd) clif->message(sd->fd,#cmd); } while(0)
#define SETFLAG(cmd) do { \
	if (strcmp(flag_name , #cmd) == 0) { \
		if (!(mf_data = getFromMAPD(&map->list[sd->bl.m], 0))) \
		{ \
			CREATE(mf_data,struct plugin_mapflag,1); \
			addToMAPD(&map->list[sd->bl.m], mf_data, 0, true); \
		} \
		mf_data->cmd = flag; \
		sprintf(atcmd_output,"[ @mapflag ] %s flag has been set to %s value = %hd",#cmd,flag?"On":"Off",flag); \
		clif->message(sd->fd,atcmd_output); \
		return true; \
	} \
} while(0)
	char atcmd_output[200];
	char flag_name[100];
	short flag=0,i;
	struct plugin_mapflag *mf_data = getFromMAPD(&map->list[sd->bl.m], 0);

	memset(flag_name, '\0', sizeof(flag_name));

	if (!message || !*message || (sscanf(message, "%99s %hd", flag_name, &flag) < 1)) {
		clif->message(sd->fd,"Enabled Mapflags:");
		clif->message(sd->fd,"----------------------------------");

		CHECKFLAG(noequip);     CHECKFLAG(gvg_noalliance);  CHECKFLAG(noconsume);
		CHECKFLAG(macprotect);  CHECKFLAG(noviceonly);      CHECKFLAG(lms);        CHECKFLAG(noparty);
		CHECKFLAG(noguild);     CHECKFLAG(nohiding);

		clif->message(sd->fd," ");
		clif->message(sd->fd,"Usage: '@mapflag2 noequip 1' (0=Off | 1=On)");
		clif->message(sd->fd,"Type '@mapflag2 available' to list the available mapflags.");
		return true;
	}
	for (i = 0; flag_name[i]; i++) flag_name[i] = TOLOWER(flag_name[i]); //lowercase

	SETFLAG(noequip);     SETFLAG(gvg_noalliance);  SETFLAG(noconsume);
	SETFLAG(macprotect);  SETFLAG(noviceonly);      SETFLAG(lms);        SETFLAG(noparty);
	SETFLAG(noguild);     SETFLAG(nohiding);
	
	clif->message(sd->fd,"Invalid Flag");
	clif->message(sd->fd,"Usage: '@mapflag2 noequip 1' (0=Off | 1=On)");
	clif->message(sd->fd,"Available Flags:"); // Available Flags:
	clif->message(sd->fd,"----------------------------------");
	clif->message(sd->fd,"noequip, gvg_noalliance, noconsume, macprotect, noviceonly");
	clif->message(sd->fd,"lms, noparty, noguild, nohiding");
#undef CHECKFLAG
#undef SETFLAG

	return true;
}

/*
Syntax:
setmapflag2 "mapname",mf_noequip,1;
*/

BUILDIN(setmapflag2) {
#define MAPFLAG2_BUILDIN(mapflag,map_value) \
		if (!(mf_data = getFromMAPD(&map->list[m], 0))) \
		{ \
			CREATE(mf_data,struct plugin_mapflag,1); \
			addToMAPD(&map->list[m], mf_data, 0, true); \
		} \
		mf_data->mapflag = 1

	int16 m,i;
	const char *str;
	int val=script_getnum(st, 4);
	struct plugin_mapflag *mf_data;

	str=script_getstr(st,2);
	i = script_getnum(st, 3);
	m = map->mapname2mapid(str);

	if (m >= 0 && val >= 0) {
		switch(i) {
			case MF_GVG_NOALLAINCE:				MAPFLAG2_BUILDIN(gvg_noalliance,val); break;
			case MF_NOCONSUME:					MAPFLAG2_BUILDIN(noconsume,val); break;
			case MF_NOEQUIP:					MAPFLAG2_BUILDIN(noequip,val); break;
			case MF_MACPROTECT:					MAPFLAG2_BUILDIN(macprotect,val); break;
			case MF_NOVICEONLY:					MAPFLAG2_BUILDIN(noviceonly,val); break;
			case MF_LMS:						MAPFLAG2_BUILDIN(lms,val); break;
			case MF_NOPARTY:					MAPFLAG2_BUILDIN(noparty,val); break;
			case MF_NOGUILD:					MAPFLAG2_BUILDIN(noguild,val); break;
			case MF_NOHIDING:					MAPFLAG2_BUILDIN(nohiding,val); break;
		}
	}
	return true;
#undef MAPFLAG2_BUILDIN
}

/* Server Startup */
HPExport void plugin_init (void) {
	
	addAtcommand("mapflag2",mapflag2);
	addScriptCommand("setmapflag2","iii",setmapflag2);

	addHookPre(map, zone_change2, map_zone_change2_pre);
	addHookPre(npc, parse_unknown_mapflag, parse_mapflags);
	addHookPost(battle, check_target, check_alliance);
	addHookPost(clif, pLoadEndAck, clif_check_class);
	addHookPost(clif, pLoadEndAck, clif_check_uid);
	addHookPre(pc, useitem, noconsume_item);
	addHookPre(pc, equipitem, noequip_allowed);
	addHookPost(pc, setpos, pc_checkequip);
	addHookPre(pc, setpos, pc_check_uid);
	addHookPre(pc, setpos, pc_check_class);
	addHookPre(skill, castend_nodamage_id, skill_castend_nodamage_id_pre);

	script->set_constant("mf_gvg_noalliance",MF_GVG_NOALLAINCE,false,false);
	script->set_constant("mf_noconsume",MF_NOCONSUME,false,false);
	script->set_constant("mf_noequip",MF_NOEQUIP,false,false);
	script->set_constant("mf_macprotect",MF_MACPROTECT,false,false);
	script->set_constant("mf_noviceonly",MF_NOVICEONLY,false,false);
	script->set_constant("mf_lms",MF_LMS,false,false);
	script->set_constant("mf_noparty",MF_NOPARTY,false,false);
	script->set_constant("mf_noguild",MF_NOGUILD,false,false);
	script->set_constant("mf_nohiding",MF_NOHIDING,false,false);
}

HPExport void server_online (void) {
	ShowInfo ("'%s' Plugin by Dastgir/Hercules. Version '%s'\n",pinfo.name,pinfo.version);
}
