/**
 * This file is part of Hercules.
 * http://herc.ws - http://github.com/HerculesWS/Hercules
 *
 * Copyright (C) 2014-2018  Hercules Dev Team
 *
 * Hercules is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/// Base author: Haru <haru@dotalux.com>
/// mapquit() script command

#include "common/hercules.h"
#include "map/script.h"
#include "map/pc.h"

#include "common/HPMDataCheck.h"

HPExport struct hplugin_info pinfo = {
	"script_getequip",	// Plugin name
	SERVER_TYPE_MAP,	// Which server types this plugin works with?
	"0.2",				// Plugin version
	HPM_VERSION,		// HPM Version (don't change, macro is automatically updated)
};

/*==========================================
 * GetEquipBoundType(Pos);     Pos: 1-SCRIPT_EQUIP_TABLE_SIZE
 * 0 = Not bounded
 * 1 = Account-bound
 * 2 = Guild-bounded
 * 3 = Party-bounded
 * 4 = Char-bounded
 *------------------------------------------*/
BUILDIN(getequipboundtype) {
	int i = -1, num;
	struct map_session_data *sd = script->rid2sd(st);

	num = script_getnum(st, 2);
	sd = script->rid2sd(st);
	if (sd == NULL)
		return true;

	if (num > 0 && num < ARRAYLENGTH(script->equip))
		i = pc->checkequip(sd, script->equip[num - 1]);
	if (i >= 0 && sd->status.inventory[i].bound != 0)
		script_pushint(st, sd->status.inventory[i].bound);
	else
		script_pushint(st, 0);

	return true;
}

/*==========================================
 * GetEquipIsRental(Pos);     Pos: 1-SCRIPT_EQUIP_TABLE_SIZE
 * 0 = false | 1 = true
 *------------------------------------------*/
BUILDIN(getequipisrental) {
	int i = -1, num;
	struct map_session_data *sd = script->rid2sd(st);

	num = script_getnum(st, 2);
	sd = script->rid2sd(st);
	if (sd == NULL)
		return true;

	if (num > 0 && num < ARRAYLENGTH(script->equip))
		i = pc->checkequip(sd, script->equip[num - 1]);
	if (i >= 0 && sd->status.inventory[i].expire_time > 0)
		script_pushint(st, 1);
	else
		script_pushint(st, 0);

	return true;
}

HPExport void server_preinit(void) {
}

HPExport void plugin_init(void) {
	addScriptCommand("getequipboundtype", "i", getequipboundtype);
	addScriptCommand("getequipisrental", "i", getequipisrental);
}

HPExport void server_online(void) {
	ShowInfo("'%s' Plugin by fTakano. Version '%s'\n", pinfo.name, pinfo.version);
}
