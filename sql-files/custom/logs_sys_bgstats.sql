--   _____             _             _        _   _ 
--  |  _  \           |_|           / |      | | | |
--  | |_| |_ ________  _  ____  ____| |__    | |_| | ____  _ ________
--  |  ___/ '__/  _  \/ \/ __ \/ ___|  __|   |  _  |/ __ \| '___/ ___|
--  | |   | |  | |_|  | |  ___| |___| |____  | | | |  ___/| |  | |___
--  \_/   \_/  \_____/| |\_____\____\_____/  \_/ | |\_____\_/   \____\
--                ____, |                        | |
--               /______/                        \_|
--

CREATE TABLE IF NOT EXISTS `char_bg_kills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `killer` varchar(25) NOT NULL,
  `killer_id` int(11) NOT NULL,
  `killed` varchar(25) NOT NULL,
  `killed_id` int(11) NOT NULL,
  `map` varchar(16) NOT NULL DEFAULT '',
  `skill` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `killer_id` (`killer_id`),
  KEY `killed_id` (`killed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;