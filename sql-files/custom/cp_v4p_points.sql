-- Dumping structure for table empireclassic.cp_v4p_points
CREATE TABLE IF NOT EXISTS `cp_v4p_points` (
  `account_id` int(10) unsigned NOT NULL,
  `points` int(10) unsigned NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table empireclassic.cp_v4p_points: 6 rows
/*!40000 ALTER TABLE `cp_v4p_points` DISABLE KEYS */;
REPLACE INTO `cp_v4p_points` (`account_id`, `points`) VALUES
	(2000000, 10),
	(2000012, 1),
	(2000017, 1),
	(2000036, 1),
	(2000004, 10),
	(2000064, 10);
/*!40000 ALTER TABLE `cp_v4p_points` ENABLE KEYS */;