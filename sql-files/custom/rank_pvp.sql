-- Dumping structure for table empireclassic.rank_pvp
CREATE TABLE IF NOT EXISTS `rank_pvp` (
  `char_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(23) NOT NULL DEFAULT '',
  `kills` int(11) NOT NULL DEFAULT 0,
  `deaths` int(11) NOT NULL DEFAULT 0,
  `streaks` int(11) NOT NULL DEFAULT 0,
  `streak_time` datetime DEFAULT NULL,
  PRIMARY KEY (`char_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table empireclassic.rank_pvp: ~0 rows (approximately)
/*!40000 ALTER TABLE `rank_pvp` DISABLE KEYS */;
/*!40000 ALTER TABLE `rank_pvp` ENABLE KEYS */;