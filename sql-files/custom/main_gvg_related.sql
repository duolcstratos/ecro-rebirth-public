
CREATE TABLE IF NOT EXISTS `gotm_empbreak` (
  `char_id` int(11) NOT NULL DEFAULT 0,
  `guild_id` int(11) NOT NULL DEFAULT 0,
  `point` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`char_id`),
  KEY `guild_id` (`guild_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `gotm_factor` (
  `f_id` int(11) NOT NULL AUTO_INCREMENT,
  `guild_id` int(11) NOT NULL DEFAULT 0,
  `castle_name` varchar(50) NOT NULL,
  `atk_factor` int(11) NOT NULL DEFAULT 0,
  `def_factor` int(11) NOT NULL DEFAULT 0,
  `add_date` int(11) NOT NULL DEFAULT 0,
  `session` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`f_id`),
  KEY `guild_id` (`guild_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `gotm_kill` (
  `char_id` int(11) NOT NULL DEFAULT 0,
  `account_id` int(11) DEFAULT 0,
  `guild_id` int(11) DEFAULT 0,
  `kill` int(11) DEFAULT 0,
  PRIMARY KEY (`char_id`),
  KEY `account_id` (`account_id`),
  KEY `guild_id` (`guild_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `gotm_longdef` (
  `ld_id` int(11) NOT NULL AUTO_INCREMENT,
  `guild_id` int(11) NOT NULL DEFAULT 0,
  `castle_name` varchar(50) NOT NULL,
  `def_time` int(11) NOT NULL DEFAULT 0,
  `add_date` int(11) NOT NULL DEFAULT 19700101,
  `session` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ld_id`),
  KEY `guild_id` (`guild_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `gotm_scoreboard` (
  `guild_id` int(11) NOT NULL,
  `long_def` int(11) NOT NULL DEFAULT 0,
  `agit_hold` int(11) NOT NULL DEFAULT 0,
  `atk_factor` int(11) NOT NULL DEFAULT 0,
  `def_factor` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`guild_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `guild_longdef` (
  `guild_id` int(11) DEFAULT 0,
  `castle_name` varchar(50) DEFAULT NULL,
  `def_time` int(11) DEFAULT 0,
  `add_date` int(11) DEFAULT 19700101,
  KEY `guild_id` (`guild_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `rank_empbreak_koe` (
  `rek_id` int(11) NOT NULL AUTO_INCREMENT,
  `char_id` int(11) NOT NULL DEFAULT 0,
  `guild_id` int(11) NOT NULL DEFAULT 0,
  `point` int(11) NOT NULL DEFAULT 0,
  `session` int(11) NOT NULL DEFAULT 0,
  `add_date` int(11) NOT NULL DEFAULT 19700101,
  PRIMARY KEY (`rek_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `rank_empbreak_woe` (
  `rew_id` int(11) NOT NULL AUTO_INCREMENT,
  `char_id` int(11) NOT NULL DEFAULT 0,
  `guild_id` int(11) NOT NULL DEFAULT 0,
  `point` int(11) NOT NULL DEFAULT 0,
  `add_date` int(11) NOT NULL DEFAULT 19700101,
  PRIMARY KEY (`rew_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `rank_kill_koe` (
  `rkk_id` int(11) NOT NULL AUTO_INCREMENT,
  `char_id` int(11) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `guild_id` int(11) DEFAULT NULL,
  `kill` int(11) DEFAULT 0,
  `session` int(11) DEFAULT 0,
  `add_date` int(11) NOT NULL DEFAULT 19700101,
  PRIMARY KEY (`rkk_id`),
  KEY `char_id` (`char_id`),
  KEY `account_id` (`account_id`),
  KEY `guild_id` (`guild_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `rank_kill_woe` (
  `rkw_id` int(11) NOT NULL AUTO_INCREMENT,
  `char_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `guild_id` int(11) DEFAULT NULL,
  `kill` int(11) DEFAULT 0,
  `add_date` int(11) DEFAULT 19700101,
  PRIMARY KEY (`rkw_id`),
  KEY `char_id` (`char_id`),
  KEY `account_id` (`account_id`),
  KEY `guild_id` (`guild_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `sys_woe_event` (
  `swe_id` int(11) NOT NULL AUTO_INCREMENT,
  `guild_id` int(11) NOT NULL DEFAULT 0,
  `castle_name` varchar(50) NOT NULL,
  PRIMARY KEY (`swe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
