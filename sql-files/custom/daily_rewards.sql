CREATE TABLE IF NOT EXISTS `daily_rewards` (
  `account_id` int(11) unsigned NOT NULL,
  `last_unique_id` varchar(100) NOT NULL DEFAULT '',
  `day` int(11) unsigned NOT NULL DEFAULT 0,
  `last_date` int(11) unsigned NOT NULL DEFAULT 0,
  `next_date` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;