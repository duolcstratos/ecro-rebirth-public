//==== DarkRO Scripts ========================================
//= Battlegrounds - Stone Control Mode (Mod by Zephyrus)
//===== By: ==================================================
//= Project Herc [vBrenth/fTakano]
//===== Current Version: =====================================
//= 1.0
//===== Description: =========================================
// Stone Control
//===== Changelog: ===========================================
//= Not yet tested for bugs
//= 1.0 - Implementation
//============================================================

function	script	SC_CheckStone	{
	for (.@i=1; .@i < 7; .@i++) {
		if (getvariableofnpc(.Stone[.@i], "Flavius_SC") == getarg(0))
			return .@i;
	}
	return 0;
}

-	script	Flavius_SC	FAKE_NPC,{
	end;

OnInit:
	.BG_Name$ = "Stone Control (Flavius)";
	.BG_Max_Score = 10;

	// .bg_reward[0] = Winner | .bg_reward[1] = Loser
	setarray .bg_reward[0], 30, 15;
	end;

OnGuillaumeQuit:
OnCroixQuit:
	BG_Deserter();
	if (!BG_Status(0) && BG_TypeCheck("Flavius_SC"))
		donpcevent("BG_Mechanics::OnJoinEvent");
OnGuillaumeDie:
OnCroixDie:
	if (BG_Status(1) && BG_TypeCheck("Flavius_SC") && (.@Stone = SC_CheckStone(getcharid(CHAR_ID_CHAR))) > 0) {
		.@Stone$ = sprintf("Neutral Stone#%d", .@Stone);
		.Stone[.@Stone] = 0;
		getmapxy(.@m$, .@x, .@y, UNITTYPE_PC);
		movenpc(.@Stone$, .@x, .@y);
		mapannounce("bat_b04", sprintf("The Neutral Stone has been dropped by [%s].", strcharinfo(PC_NAME)), bc_npc, 0xFFFFFF);
		bg_addpoint("sc_dropped", 1);
		initnpctimer(.@Stone$);
		deltimer(strnpcinfo(NPC_NAME)+"::OnFlash");
		setpcblock(PCBLOCK_SKILL|PCBLOCK_USEITEM, false);
		@STONE_HOLDER = true;
		addtimer(2000, strnpcinfo(NPC_NAME)+"::OnPrevStoneHolder");
		enablenpc(.@Stone$);
	}
	end;

OnPrevStoneHolder:
	@STONE_HOLDER = false;
	end;

OnBuildTeams:
	$@BG_Team1 = bg_team_create2("bat_b04", 390, 10, 0, strnpcinfo(NPC_NAME)+"::OnGuillaumeQuit", strnpcinfo(NPC_NAME)+"::OnGuillaumeDie");
	$@BG_Team2 = bg_team_create2("bat_b04", 10, 290, 1, strnpcinfo(NPC_NAME)+"::OnCroixQuit", strnpcinfo(NPC_NAME)+"::OnCroixDie");
	end;

OnReady:
	if (!BG_TypeCheck("Flavius_SC"))
		end;

	initnpctimer();
	.Guillaume_Score = .Croix_Score = 0;
	setarray .Stone[1], 0, 0, 0, 0, 0, 0;
	setarray .x[1], 177, 222, 222, 177, 200, 199;
	setarray .y[1], 182, 182, 117, 117, 105, 194;
	for (.@i=1; .@i<7; .@i++)
		donpcevent("Neutral Stone#"+.@i+"::OnBGStart");
	bg_updatescore("bat_b04", .Guillaume_Score, .Croix_Score);
	sleep(2000);
	bg_warp($@BG_Team1, "bat_b04", 328, 150);
	bg_warp($@BG_Team2, "bat_b04", 62, 150);
	sleep(2000);
	donpcevent("#Respawn_Guill_SCF::OnBGStart");
	donpcevent("#Respawn_Croix_SCF::OnBGStart");
	end;

OnGuillaumeScore:
	.Guillaume_Score++;
	donpcevent(strnpcinfo(NPC_NAME)+"::OnValidateScore");
	end;

OnCroixScore:
	.Croix_Score++;
	donpcevent(strnpcinfo(NPC_NAME)+"::OnValidateScore");
	end;

OnValidateScore:
	if (!BG_Status(1) || !BG_TypeCheck("Flavius_SC"))
		end;
	if (.Guillaume_Score > .BG_Max_Score) .Guillaume_Score = .BG_Max_Score;
	if (.Croix_Score > .BG_Max_Score) .Croix_Score = .BG_Max_Score;
	bg_updatescore("bat_b04", .Guillaume_Score, .Croix_Score);

	if (.Croix_Score >= .BG_Max_Score || .Guillaume_Score >= .BG_Max_Score)
		donpcevent(strnpcinfo(NPC_NAME)+"::OnMatchEnd");
	end;

OnTimer600000:
	mapannounce("bat_b04", "-- The Battle of Stone Control will end in 5 minutes! --", bc_npc, 0xDDCD48);
	end;

OnTimer840000:
	mapannounce("bat_b04", "-- The Battle of Stone Control will end in 1 minute! --", bc_npc, 0xDDCD48);
	end;

OnTimer900000:
OnMatchEnd:
	stopnpctimer();
	donpcevent("#Respawn_Guill_SCF::OnBGStop");
	donpcevent("#Respawn_Croix_SCF::OnBGStop");
	$@BG_Status = 2;

	.@Badge_W = .bg_reward[0];
	.@Badge_L = .bg_reward[1];

	if (.Guillaume_Score > .Croix_Score) {
		bg_reward($@BG_Team1, Battleground_Badge, .@Badge_W, 0, 1, BG_SC, BG_R_WIN);
		bg_reward($@BG_Team2, Battleground_Badge, .@Badge_L, 0, 1, BG_SC, BG_R_LOSE);
		mapannounce("bat_b04", "The Guillaume army has won the battle of "+.BG_Name$+"!", bc_npc, 0x3080FF);
	} else if (.Guillaume_Score < .Croix_Score) {
		bg_reward($@BG_Team1, Battleground_Badge, .@Badge_L, 0, 1, BG_SC, BG_R_LOSE);
		bg_reward($@BG_Team2, Battleground_Badge, .@Badge_W, 0, 1, BG_SC, BG_R_WIN);
		mapannounce("bat_b04", "The Croix army has won the battle of "+.BG_Name$+"!", bc_npc, 0xFF3030);
	} else {
		bg_reward($@BG_Team1, Battleground_Badge, .@Badge_L, 0, 1, BG_SC, BG_R_TIE);
		bg_reward($@BG_Team2, Battleground_Badge, .@Badge_L, 0, 1, BG_SC, BG_R_TIE);
		mapannounce("bat_b04", "The Battle of "+.BG_Name$+" resulted to a draw!", bc_npc, 0xDDCD48);
	}
	.Guillaume_Score = .Croix_Score = 0;
	for (.@i=1; .@i<7; .@i++) {
		stopnpctimer("Neutral Stone#"+.@i);
		stopnpctimer("Stone Point#G"+.@i+"_SCF");
		stopnpctimer("Stone Point#C"+.@i+"_SCF");
	}

	for (.@i=1; .@i<7; .@i++) {
		if (attachrid(charid2rid(.Stone[.@i])))
			setpcblock(PCBLOCK_SKILL|PCBLOCK_USEITEM, false);
		detachrid();
	}

	sleep(5000);
	setarray .Stone[1], 0, 0, 0, 0, 0, 0;
	donpcevent("BG_Mechanics::OnRotate");
	bg_updatescore("bat_b04", 0, 0);
	for (.@i=1; .@i<7; .@i++) {
		donpcevent("Neutral Stone#"+.@i+"::OnBGStop");
		donpcevent("Stone Point#G"+.@i+"_SCF::OnBGStop");
		donpcevent("Stone Point#C"+.@i+"_SCF::OnBGStop");
	}
	end;

OnFlash:
	if (BG_Status(1) && BG_TypeCheck("Flavius_SC") && (.@Stone = SC_CheckStone(getcharid(CHAR_ID_CHAR))) > 0) {
		getmapxy(.@m$, .@x, .@y, UNITTYPE_PC);
		if (getcharid(CHAR_ID_BG) == $@BG_Team1)
			viewpointmap(.@m$, 1, .@x, .@y, .@Stone, 0x0000FF);
		else if (getcharid(CHAR_ID_BG) == $@BG_Team2)
			viewpointmap(.@m$, 1, .@x, .@y, .@Stone, 0xFF0000);

		specialeffect(EF_BOWLINGBASH, AREA, getcharid(CHAR_ID_ACCOUNT));
		emotion(e_hlp, AREA);
		addtimer(2000, strnpcinfo(NPC_NAME)+"::OnFlash");
		percentheal(-5, -5);
	}
	end;
}

//===========================================================================
// -- Battleground Flavius Stones
//===========================================================================
-	script	Neutral_Stone_SC	BARRICADE,1,1,{
	end;

OnTouch:
	if (!BG_Status(1) || !BG_TypeCheck("Flavius_SC") || !getcharid(CHAR_ID_BG) || @STONE_HOLDER)
		end;

	.@Stone = atoi(strnpcinfo(NPC_NAME_HIDDEN));
	if (getvariableofnpc(.Stone[.@Stone], "Flavius_SC") || SC_CheckStone(getcharid(CHAR_ID_CHAR))) end;

	BG_RemoveHide();
	setpcblock(PCBLOCK_SKILL|PCBLOCK_USEITEM, true);

	set(getvariableofnpc(.Stone[.@Stone], "Flavius_SC"), getcharid(CHAR_ID_CHAR));
	addtimer(2000, "Flavius_SC::OnFlash");
	disablenpc(strnpcinfo(NPC_NAME));
	stopnpctimer();
	end;

OnBGStart:
	initnpctimer();
	end;

OnTimer2000:
	initnpctimer();
	getmapxy(.@m$, .@x, .@y, UNITTYPE_NPC);
	viewpointmap("bat_b04", 1, .@x, .@y, atoi(strnpcinfo(NPC_NAME_HIDDEN)), 0xFFFFFF);
	end;

OnBGStop:
	.@Stone = atoi(strnpcinfo(NPC_NAME_HIDDEN));
	movenpc(strnpcinfo(NPC_NAME), getvariableofnpc(.x[.@Stone], "Flavius_SC"), getvariableofnpc(.y[.@Stone], "Flavius_SC"));
	enablenpc(strnpcinfo(NPC_NAME));
	stopnpctimer();
	end;
}

bat_b04,177,182,0	duplicate(Neutral_Stone_SC)	Neutral Stone#1	BARRICADE,1,1
bat_b04,222,182,0	duplicate(Neutral_Stone_SC)	Neutral Stone#2	BARRICADE,1,1
bat_b04,222,117,0	duplicate(Neutral_Stone_SC)	Neutral Stone#3	BARRICADE,1,1
bat_b04,177,117,0	duplicate(Neutral_Stone_SC)	Neutral Stone#4	BARRICADE,1,1
bat_b04,200,105,0	duplicate(Neutral_Stone_SC)	Neutral Stone#5	BARRICADE,1,1
bat_b04,199,194,0	duplicate(Neutral_Stone_SC)	Neutral Stone#6	BARRICADE,1,1

//===========================================================================
// -- Battleground Base Stones
//===========================================================================
-	script	Base_Stone_SC	GAJOMART,1,1,{
	end;

OnTouch:
	.@Team$ = charat(strnpcinfo(NPC_NAME_HIDDEN), 0);	
	.@Slot = atoi(charat(strnpcinfo(NPC_NAME_HIDDEN), 1));
	.@Team = .@Team$ == "G" ? $@BG_Team1 : $@BG_Team2;

	if (!BG_Status(1) || !BG_TypeCheck("Flavius_SC")) end;
	if (getd(".Point_"+.@Team$+.@Slot) != 0 && getcharid(CHAR_ID_BG) != .@Team && !SC_CheckStone(getcharid(CHAR_ID_CHAR))) {
		set(getvariableofnpc(.Stone[getd(".Point_"+.@Team$+.@Slot)], "Flavius_SC"), getcharid(CHAR_ID_CHAR));
		setnpcdisplay(sprintf("Stone Point#%s%d_SCF", .@Team$, .@Slot), sprintf("Stone Point#%s%d_SCF", .@Team$, .@Slot), GAJOMART);

		BG_RemoveHide();
		setpcblock(PCBLOCK_SKILL|PCBLOCK_USEITEM, true);

		mapannounce("bat_b04", sprintf("%s Stone has been stolen by [%s].", (.@Team$ == "G" ? "Guillaume" : "Croix"), strcharinfo(PC_NAME)), bc_npc, (.@Team$ == "G" ? 0x3080FF : 0xFF3030));
		bg_addpoint("sc_stole", 1);

		setd(".Point_"+.@Team$+.@Slot, 0);
		addtimer(2000, "Flavius_SC::OnFlash");
		stopnpctimer();
	} else if (getd(".Point_"+.@Team$+.@Slot) == 0 && getcharid(CHAR_ID_BG) == .@Team && (.@Stone = SC_CheckStone(getcharid(CHAR_ID_CHAR))) > 0) {
		initnpctimer();
		setd(".Point_"+.@Team$+.@Slot, .@Stone);
		setd(".Count_"+.@Team$+.@Slot, 0);
		deltimer("Flavius_SC::OnFlash");

		mapannounce("bat_b04", sprintf("%s Stone has been captured by [%s].", (.@Team$ == "G" ? "Guillaume" : "Croix"), strcharinfo(PC_NAME)), bc_npc, (.@Team$ == "G" ? 0x3080FF : 0xFF3030));
		bg_addpoint("sc_captured", 1);

		setpcblock(PCBLOCK_SKILL|PCBLOCK_USEITEM, false);
		setnpcdisplay(strnpcinfo(NPC_NAME), (.@Team$ == "G" ? "Guillaume Stone#G"+.@Slot+"_SCF":"Croix Stone#C"+.@Slot+"_SCF"), BARRICADE);
		set(getvariableofnpc(.Stone[getd(".Point_"+.@Team$+.@Slot)], "Flavius_SC"), 0);
	}
	end;

OnTimer2000:
	.@Team$ = charat(strnpcinfo(NPC_NAME_HIDDEN), 0);	
	.@Slot = atoi(charat(strnpcinfo(NPC_NAME_HIDDEN), 1));

	initnpctimer();
	getmapxy(.@m$, .@x, .@y, UNITTYPE_NPC);
	viewpointmap("bat_b04", 1, .@x, .@y, getd(".Point_"+.@Team$+.@Slot), .@Team$ == "C" ? 0xFF0000 : 0x0000FF);
	specialeffect(EF_GANBANTEIN, AREA);

	set(getd(".Count_"+.@Team$+.@Slot), getd(".Count_"+.@Team$+.@Slot) + 1);

	if (getd(".Count_"+.@Team$+.@Slot) >= 5) {
		setd(".Count_"+.@Team$+.@Slot, 0);
		donpcevent("Flavius_SC::On"+(.@Team$ == "G" ? "Guillaume" : "Croix")+"Score");
	}
	end;

OnBGStop:
	.@Team$ = charat(strnpcinfo(NPC_NAME_HIDDEN), 0);	
	.@Slot = atoi(charat(strnpcinfo(NPC_NAME_HIDDEN), 1));
	stopnpctimer();
	setnpcdisplay(sprintf("Stone Point#%s%d_SCF", .@Team$, .@Slot), sprintf("Stone Point#%s%d_SCF", .@Team$, .@Slot), GAJOMART);
	setd(".Point_"+.@Team$+.@Slot, 0);
	setd(".Count_"+.@Team$+.@Slot, 0);
	end;
}

// -- Croix Base Stones
bat_b04,85,159,0	duplicate(Base_Stone_SC)	Stone Point#C1_SCF	GAJOMART,1,1
bat_b04,78,159,0	duplicate(Base_Stone_SC)	Stone Point#C2_SCF	GAJOMART,1,1
bat_b04,71,159,0	duplicate(Base_Stone_SC)	Stone Point#C3_SCF	GAJOMART,1,1
bat_b04,85,140,0	duplicate(Base_Stone_SC)	Stone Point#C4_SCF	GAJOMART,1,1
bat_b04,78,140,0	duplicate(Base_Stone_SC)	Stone Point#C5_SCF	GAJOMART,1,1
bat_b04,71,140,0	duplicate(Base_Stone_SC)	Stone Point#C6_SCF	GAJOMART,1,1

// -- Guillaume Base Stones
bat_b04,312,159,0	duplicate(Base_Stone_SC)	Stone Point#G1_SCF	GAJOMART,1,1
bat_b04,319,159,0	duplicate(Base_Stone_SC)	Stone Point#G2_SCF	GAJOMART,1,1
bat_b04,326,159,0	duplicate(Base_Stone_SC)	Stone Point#G3_SCF	GAJOMART,1,1
bat_b04,312,140,0	duplicate(Base_Stone_SC)	Stone Point#G4_SCF	GAJOMART,1,1
bat_b04,319,140,0	duplicate(Base_Stone_SC)	Stone Point#G5_SCF	GAJOMART,1,1
bat_b04,326,140,0	duplicate(Base_Stone_SC)	Stone Point#G6_SCF	GAJOMART,1,1

//===========================================================================
// -- Battleground Therapists
//===========================================================================
bat_b04,390,13,5	duplicate(Battle Therapist#bat)	Battle Therapist#sc-f1	4_F_SISTER
bat_b04,10,293,5	duplicate(Battle Therapist#bat)	Battle Therapist#sc-f2	4_F_SISTER

//===========================================================================
// -- Battleground Respawn Location
//===========================================================================
-	script	Flavius_SC_Respawn	HIDDEN_WARP_NPC,{
	end;

OnBGStart:
	initnpctimer();
	end;

OnBGStop:
	stopnpctimer();
	end;

OnTimer24000:
	specialeffect(EF_SANCTUARY, AREA);
	areapercentheal("bat_b04", 382, 2, 397, 17, 100, 100);
	areapercentheal("bat_b04", 2, 282, 17, 297, 100, 100);
	end;

OnTimer25000:
	stopnpctimer();
	areawarp("bat_b04", 382, 2, 397, 17, "bat_b04", 311, 224);
	areawarp("bat_b04", 2, 282, 17, 297, "bat_b04", 87, 75);
	initnpctimer();
	end;
}

bat_b04,390,10,0	duplicate(Flavius_SC_Respawn)	#Respawn_Guill_SCF	HIDDEN_WARP_NPC
bat_b04,10,290,0	duplicate(Flavius_SC_Respawn)	#Respawn_Croix_SCF	HIDDEN_WARP_NPC

//===========================================================================
// -- Battleground Team Flags
//===========================================================================
bat_b04,304,231,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_1	1_FLAG_LION
bat_b04,319,231,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_2	1_FLAG_LION
bat_b04,304,218,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_3	1_FLAG_LION
bat_b04,319,218,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_4	1_FLAG_LION
bat_b04,304,231,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_5	1_FLAG_LION
bat_b04,304,231,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_6	1_FLAG_LION
bat_b04,335,142,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_7	1_FLAG_LION
bat_b04,335,157,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_8	1_FLAG_LION
bat_b04,390,16,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_9	1_FLAG_LION
bat_b04,292,163,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_10	1_FLAG_LION
bat_b04,292,136,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_11	1_FLAG_LION
bat_b04,241,185,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_12	1_FLAG_LION
bat_b04,247,179,1	duplicate(Guillaume Camp#bat)	Guillaume Camp#sc_13	1_FLAG_LION

bat_b04,96,81,1	duplicate(Croix Camp#bat)	Croix Camp#sc_1	1_FLAG_EAGLE
bat_b04,96,68,1	duplicate(Croix Camp#bat)	Croix Camp#sc_2	1_FLAG_EAGLE
bat_b04,79,81,1	duplicate(Croix Camp#bat)	Croix Camp#sc_3	1_FLAG_EAGLE
bat_b04,79,68,1	duplicate(Croix Camp#bat)	Croix Camp#sc_4	1_FLAG_EAGLE
bat_b04,96,81,1	duplicate(Croix Camp#bat)	Croix Camp#sc_5	1_FLAG_EAGLE
bat_b04,96,81,1	duplicate(Croix Camp#bat)	Croix Camp#sc_6	1_FLAG_EAGLE
bat_b04,59,164,1	duplicate(Croix Camp#bat)	Croix Camp#sc_7	1_FLAG_EAGLE
bat_b04,59,137,1	duplicate(Croix Camp#bat)	Croix Camp#sc_8	1_FLAG_EAGLE
bat_b04,10,296,1	duplicate(Croix Camp#bat)	Croix Camp#sc_9	1_FLAG_EAGLE
bat_b04,110,162,1	duplicate(Croix Camp#bat)	Croix Camp#sc_10	1_FLAG_EAGLE
bat_b04,110,137,1	duplicate(Croix Camp#bat)	Croix Camp#sc_11	1_FLAG_EAGLE
bat_b04,152,120,1	duplicate(Croix Camp#bat)	Croix Camp#sc_12	1_FLAG_EAGLE
bat_b04,158,114,1	duplicate(Croix Camp#bat)	Croix Camp#sc_13	1_FLAG_EAGLE

//===========================================================================
// -- Battleground Map Flags
//===========================================================================
bat_b04	mapflag	pvp	off
bat_b04	mapflag	battleground	2
bat_b04	mapflag	nomemo
bat_b04	mapflag	nosave	SavePoint
bat_b04	mapflag	noteleport
bat_b04	mapflag	nowarp
bat_b04	mapflag	nowarpto
bat_b04	mapflag	noreturn
bat_b04	mapflag	nobranch
bat_b04	mapflag	nopenalty
bat_b04	mapflag	novending
bat_b04	mapflag	noemergencycall
