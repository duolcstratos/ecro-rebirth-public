//==== DarkRO Scripts ========================================
//= Pvp Warper - Revamped
//===== By: ==================================================
//= [GM] Mirage
//===== Current Version: =====================================
//= 1.3
//===== Description: =========================================
//= Pvp Warper with Multiple Rooms
//===== Changelog: ===========================================
//= Not yet tested for bugs
//= 1.0 Implementation
//= 1.1 Added Class and No Gods PVP Rooms
//= 1.2 Added total players on all Pvp Rooms (NPC waitingroom)
//= 1.3 Added total players to all maps at Class specific PVP Room
//============================================================

function	script	F_PKMostKill	{
	.@col$ = "name, kills, deaths";
	.@rs = query_sql("SELECT "+escape_sql(.@col$)+" FROM "+escape_sql(getarg(0))+" WHERE `kills` > 0 ORDER BY `kills` DESC, `streaks` DESC, `deaths` ASC LIMIT "+getarg(1), .@name$, .@kills, .@deaths);
	if (!.@rs) {
		mes "[^FF8000 PK Warper & Ladder ^000000]";
		mes "The ladder is currently empty.";
		next;
	}
	for (.@j=0; .@j<.@rs; .@j+=getarg(2)) {
		mes "[^FF8000 PK Warper & Ladder ^000000]";
		for (.@i=.@j; .@i<getarg(2) && .@i<.@rs; .@i++) {
			mes "^996600"+(.@i+1)+": ^006699"+.@name$[.@i]+" ^00AA00["+.@kills[.@i]+"] ^FF0000<"+.@deaths[.@i]+">^000000";
		}
		next;
	}
	return;
}

function	script	F_PKHighestStreak	{
	.@col$ = "name, streaks, DATE_FORMAT(streak_time,'%a %e/%c/%y %r')";
	.@order$ = "streaks";
	.@rs = query_sql("SELECT "+.@col$+" FROM "+escape_sql(getarg(0))+" WHERE "+escape_sql(.@order$)+" > 3 ORDER BY "+escape_sql(.@order$)+" DESC LIMIT "+getarg(1), .@name$, .@streaks, .@time$);
	if (!.@rs) {
		mes "[^FF8000 PK Warper & Ladder ^000000]";
		mes "The ladder is currently empty.";
		next;
	}
	for (.@j=0; .@j<.@rs; .@j+=getarg(2)) {
		mes "[^FF8000 PK Warper & Ladder ^000000]";
		for (.@i=.@j; .@i<getarg(2) && .@i<.@rs; .@i++) {
			mes "^996600"+(.@i+1)+": ^006699"+.@name$[.@i]+" ^70AC11{"+.@streaks[.@i]+"} ^000000on :";
			mes "    ^EE8800"+.@time$[.@i]+"^000000";
		}
		next;
	}
	return;
}

prontera,161,184,3	script	[ PvP Warper ]	4_M_JOB_KNIGHT1,{

	set .@npcname$,"^FF8000 PK Warper & Ladder ^000000";
	while (1) {
		mes "["+ .@npcname$ +"]";
		mes "Hello, " + (Sex ? "Sir" : "Madam") + " ^0000FF"+ strcharinfo(0) +"^000000!";
		mes "I can warp you to any PvP Rooms or If you want to, I can show you your PVP stats.";
		mes "[ ^FF0000NOTE^000000 ] You must be ^FF0000Lv200+^000000 to  enter all PVP Rooms.";
		next;
		//switch(select("PvP Room - ^0000FFMain^000000 [^FF0000" + getmapusers("pvproom") + "^000000 Players]:Pvp Room - ^0000FFClass^000000 [^FF0000" + (getmapusers("pvproom_1") + getmapusers("pvproom_2") + getmapusers("pvproom_3") + getmapusers("pvproom_4") + getmapusers("pvproom_5") + getmapusers("pvproom_6") + getmapusers("pvproom_7") + getmapusers("pvproom_8") + getmapusers("pvproom_9") + getmapusers("pvproom_10") + getmapusers("pvproom_11") + getmapusers("pvproom_12") + getmapusers("pvproom_13") + getmapusers("pvproom_14") + getmapusers("pvproom_15") + getmapusers("pvproom_16")) + "^000000 Players]:PvP Room - ^0000FFNo Gods^000000 [^FF0000" + getmapusers("pvproom_16") + "^000000 Players]:PvP Shop:Check Rankings")) {
		switch(select("PvP Room - ^0000FFMain^000000 (^FF0000" + getmapusers("pvproom") + "^000000 Players):PvP Room - ^0000FFClass^000000 (^FF0000" + (getmapusers("pvproom_1") + getmapusers("pvproom_2") + getmapusers("pvproom_3") + getmapusers("pvproom_4") + getmapusers("pvproom_5") + getmapusers("pvproom_6") + getmapusers("pvproom_7") + getmapusers("pvproom_8") + getmapusers("pvproom_9") + getmapusers("pvproom_10") + getmapusers("pvproom_11") + getmapusers("pvproom_12") + getmapusers("pvproom_13") + getmapusers("pvproom_14") + getmapusers("pvproom_15") + getmapusers("pvproom_16")) + "^000000 Players):GvG Room - ^0000FFMain^000000 (^FF0000" + getmapusers("guild_vs4") + "^000000 Players):PvP Shop:Check Rankings")) {
		//switch(select("PvP Room - ^0000FFMain^000000 [^FF0000" + getmapusers("pvproom") + "^000000 Players]:PvP Shop:Check Rankings")) {
		case 1:
			if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter this PvP Room!"; close; }
			announce strcharinfo(0) +" has entered the PvP Room (Main)",0,0x00FFFF;
			warp ("pvproom", 0, 0);
			close;
			break;
		case 2:
			switch(select("Lord Knight (^FF0000" + getmapusers("pvproom_1") + "^000000 Players):High Priest (^FF0000" + getmapusers("pvproom_2") + "^000000 Players):High Wizard (^FF0000" + getmapusers("pvproom_3") + "^000000 Players):Whitesmith (^FF0000" + getmapusers("pvproom_4") + "^000000 Players):Sniper (^FF0000" + getmapusers("pvproom_5") + "^000000 Players):Assassin Cross (^FF0000" + getmapusers("pvproom_6") + "^000000 Players):Paladin (^FF0000" + getmapusers("pvproom_7") + "^000000 Players):Champion (^FF0000" + getmapusers("pvproom_8") + "^000000 Players):Professor (^FF0000" + getmapusers("pvproom_9") + "^000000 Players):Stalker (^FF0000" + getmapusers("pvproom_10") + "^000000 Players):Creator (^FF0000" + getmapusers("pvproom_11") + "^000000 Players):Clown (^FF0000" + getmapusers("pvproom_12") + "^000000 Players):Gypsy (^FF0000" + getmapusers("pvproom_13") + "^000000 Players):Starwars Jobs (^FF0000" + getmapusers("pvproom_14") + "^000000 Players):Baby Jobs (^FF0000" + getmapusers("pvproom_15") + "^000000 Players)")) {
		case 1: // Lord Knight PVP Room
				if (getmapusers("pvproom_1"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Lord_Knight) { mes "["+ .@npcname$ +"]"; mes "You must be a Lord Knight to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Lord Knight)",0,0x00FFFF;
				warp ("pvproom_1", 0, 0);
			close;
			end;
		case 2: // High Priest PVP Room
				if (getmapusers("pvproom_2"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_High_Priest) { mes "["+ .@npcname$ +"]"; mes "You must be a High Priest to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (High Priest)",0,0x00FFFF;
				warp "pvproom_2",0,0;
			close;
			end;
		case 3:	// High Wizard PVP Room
				if (getmapusers("pvproom_3"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_High_Wizard) { mes "["+ .@npcname$ +"]"; mes "You must be a High Wizard to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (High Wizard)",0,0x00FFFF;
				warp "pvproom_3",0,0;
			close;
			end;
		case 4:	// Whitesmith PVP Room
				if (getmapusers("pvproom_4"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Whitesmith) { mes "["+ .@npcname$ +"]"; mes "You must be a Whitesmith to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Whitesmith)",0,0x00FFFF;
				warp "pvproom_4",0,0;
			close;
			end;
		case 5: // Sniper PVP Room
				if (getmapusers("pvproom_5"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Sniper) { mes "["+ .@npcname$ +"]"; mes "You must be a Sniper to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Sniper)",0,0x00FFFF;
				warp "pvproom_5",0,0;
			close;
			end;
		case 6: // Assassin Cross PVP Room
				if (getmapusers("pvproom_6"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Assassin_Cross) { mes "["+ .@npcname$ +"]"; mes "You must be a Assassin Cross to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Assassin Cross)",0,0x00FFFF;
				warp "pvproom_6",0,0;
			close;
			end;
		case 7: // Paladin PVP Room
				if (getmapusers("pvproom_7"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Paladin) { mes "["+ .@npcname$ +"]"; mes "You must be a Paladin to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Paladin)",0,0x00FFFF;
				warp "pvproom_7",0,0;
			close;
			end;
		case 8: // Champion PVP Room
				if (getmapusers("pvproom_8"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Champion) { mes "["+ .@npcname$ +"]"; mes "You must be a Champion to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Champion)",0,0x00FFFF;
				warp "pvproom_8",0,0;
			close;
			end;
		case 9: // Professor PVP Room
				if (getmapusers("pvproom_9"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Professor) { mes "["+ .@npcname$ +"]"; mes "You must be a Professor to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Professor)",0,0x00FFFF;
				warp "pvproom_9",0,0;
			close;
			end;
		case 10: // Stalker PVP Room
				if (getmapusers("pvproom_10"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Stalker) { mes "["+ .@npcname$ +"]"; mes "You must be a Stalker to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Stalker)",0,0x00FFFF;
				warp "pvproom_10",0,0;
			close;
			end;
		case 11: // Creator PVP Room
				if (getmapusers("pvproom_11"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Creator) { mes "["+ .@npcname$ +"]"; mes "You must be a Creator to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Creator)",0,0x00FFFF;
				warp "pvproom_11",0,0;
			close;
			end;
		case 12: // Clown PVP Room
				if (getmapusers("pvproom_12"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Clown) { mes "["+ .@npcname$ +"]"; mes "You must be a Clown to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Clown)",0,0x00FFFF;
				warp "pvproom_12",0,0;
			close;
			end;
		case 13: // Gypsy PVP Room
				if (getmapusers("pvproom_13"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Gypsy) { mes "["+ .@npcname$ +"]"; mes "You must be a Gypsy to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Gypsy)",0,0x00FFFF;
				warp "pvproom_13",0,0;
			close;
			end;
		case 14: // Starwars Jobs PVP Room
				if (getmapusers("pvproom_14"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class != Job_Padawan && Class != Job_Jedi && Class != Job_Sith) { mes "["+ .@npcname$ +"]"; mes "You must be a Starwars Job to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Starwars)",0,0x00FFFF;
				warp "pvproom_14",0,0;
			close;
			end;
		case 15: // Baby Jobs PVP Room
				if (getmapusers("pvproom_15"))
				if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				if (Class < 4023 || Class > 4045) { mes "["+ .@npcname$ +"]"; mes "You must be a Baby to enter this PVP Room!"; close; }
				announce strcharinfo(0) +" has entered the PvP Room (Baby)",0,0x00FFFF;
				warp "pvproom_15",0,0;
			close;
			end;
			}
		/*case 3: // Non-Donors Pvp
				if (getmapusers("pvproom_16")) callsub S_full;
				if(BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter PVP Room."; close; }
				for( set .@i,0; .@i<getarraysize(.NoGod_ids); set .@i,.@i+1 ) {
				if(countitem(.NoGod_ids[.@i])) {
					mes "["+ .@npcname$ +"]"; mes "^ff0000God Items^000000 are not allowed in this area.";
					mes "Put it on your storage and try again.";
					close;
						}
					}
				warp "pvproom_16",0,0;
				end;*/
		case 3:
			if (BaseLevel < 200) { mes "["+ .@npcname$ +"]"; mes "You must be Level ^ff0000200^000000+ to enter this GvG Room!"; close; }
			announce strcharinfo(0) +" has entered the GvG Room (Main)",0,0x00FFFF;
			warp ("guild_vs4", 0, 0);
			close;
			break;
		case 4:
			openshop("PK_Shop");
			end;
		case 5:
			switch(select("PvP Rankings:GvG Rankings:View Personal Record:Information:Cancel")) {
				default:
					close();
					break;
				case 1:
					.@tbl_name$ = "rank_pvp";
					mes "["+ .@npcname$ +"]";
					mes "What rank list you want to see?";
					next;
					switch(select("Most Kills:Highest Streaks:Go Back")) {
						default: break;
						case 1:
							F_PKMostKill(.@tbl_name$, 20, 10);
							break;
						case 2:
							F_PKHighestStreak(.@tbl_name$, 20, 10);
							break;
					}
					break;
				case 2:
					.@tbl_name$ = "rank_gvg";
					mes "["+ .@npcname$ +"]";
					mes "What rank list you want to see?";
					next;
					switch(select("Most Kills:Highest Streaks:Most Ownage:Go Back")) {
						default: break;
						case 1:
							F_PKMostKill(.@tbl_name$, 20, 10);
							break;
						case 2:
							F_PKHighestStreak(.@tbl_name$, 20, 10);
							break;
						case 3:
							.@col$ = "name, high_own_cnt, DATE_FORMAT(own_time, '%a %e/%c/%y %r')";
							.@order$ = "high_own_cnt";
							.@rs = query_sql("SELECT "+.@col$+" FROM rank_gvg_ownage ORDER BY "+escape_sql(.@order$)+" DESC LIMIT 20", .@guild$, .@own_cnt, .@time$);
							if (!.@rs) {
								mes "["+ .@npcname$ +"]";
								mes "The ladder is currently empty.";
								next;
							}
							for (.@j=0; .@j<.@rs; .@j+=10) {
								mes "["+ .@npcname$ +"]";
								for (.@i=.@j; .@i<10 && .@i<.@rs; .@i++) {
									mes "^996600"+(.@i+1)+": ^006699"+.@guild$[.@i]+" ^00AAAA("+.@own_cnt[.@i]+") ^000000on :";
									mes "    ^EE8800"+.@time$[.@i]+"^000000";
								}
								next;
							}
							break;
					}
					break;
				case 3:
					.@pvp_rs = query_sql("SELECT kills, deaths, streaks, DATE_FORMAT(streak_time, '%a %e/%c/%y %r') FROM rank_pvp WHERE char_id="+getcharid(CHAR_ID_CHAR), .@kills_p, .@deaths_p, .@streaks_p, .@time_p$);
					.@gvg_rs = query_sql("SELECT kills, deaths, streaks, DATE_FORMAT(streak_time, '%a %e/%c/%y %r') FROM rank_gvg WHERE char_id="+getcharid(CHAR_ID_CHAR), .@kills_g, .@deaths_g, .@streaks_g, .@time_g$);
					if (!.@pvp_rs && !.@gvg_rs) {
						mes "["+ .@npcname$ +"]";
						mes "No records found.";
						next;
					} else {
						if (.@pvp_rs) {
							mes "["+ .@npcname$ +"]";
							mes "Current PvP Streak : ^70AC11{"+@PVP_STREAK_HOLDER+"}^000000";
							mes "Total PvP Kills : ^00AA00["+.@kills_p+"]^000000";
							mes "Total PvP Deaths : ^FF0000<"+.@deaths_p+">^000000";
							if (.@streaks_p) {
								mes "Highest PvP Streak was ^70AC11{"+.@streaks_p+"}^000000 on :";
								mes "    ^EE8800"+.@time_p$+"^000000";
							}
						}
						if (.@gvg_rs) {
							if (.@pvp_rs) next;
							mes "["+ .@npcname$ +"]";
							mes "Current GvG Streak : ^70AC11{"+@GVG_STREAK_HOLDER+"}^000000";
							mes "Total GvG Kills : ^00AA00["+.@kills_g+"]^000000";
							mes "Total GvG Deaths : ^FF0000<"+.@deaths_g+">^000000";
							if (.@streaks_g) {
								mes "Highest GvG Streak was ^70AC11{"+.@streaks_g+"}^000000 on :";
								mes "    ^EE8800"+.@time_g$+"^000000";
							}
						}
						next;
					}
					break;
				case 4:
					mes "["+ .@npcname$ +"]";
					mes "Explanation for Most Kills:";
					mes "^996600Rank: ^006699NAME ^00AA00[Total Kills] ^FF0000<Deaths>^000000";
					mes " ";
					mes "The ^00AA00Kills^000000 added when a player kills another player.";
					mes " ";
					mes "The ^FF0000Deaths^000000 count increase when a player is killed by another player, or suicide (eg: Grand Cross).";
					mes " ";
					mes "A player killed by monsters, homunculi or pets will not affect the kill/death count.";
					next;
					mes "["+ .@npcname$ +"]";
					mes "Explanation for Highest Streak:";
					mes "^996600Rank: ^006699NAME ^70AC11{Highest Streak} ^000000on :";
					mes "    ^EE8800TIME^000000";
					mes " ";
					mes "The ^70AC11Streak^000000 are added when a player kills another player. It will reset upon log out, killed by another player, or suicide (eg: Sacrifice).";
					mes " ";
					mes "Then it record in the server the ^EE8800TIME^000000 when that player got that highest streak.";
					mes " ";
					mes "A player killed by monsters, homunculi or pets will not reset the streak count.";
					mes " ";
					mes "The numbers of straight kills to get these announcements are :";
					for (.@i=0; .@i<getarraysize(getvariableofnpc(.streakList$, "PK_Mechanics")); .@i+=3)
						mes "^70AC11"+getelementofarray(getvariableofnpc(.streakList$, "PK_Mechanics"), .@i+1)+"^000000 : "+getelementofarray(getvariableofnpc(.streakList$, "PK_Mechanics"), .@i);
					next;
					mes "["+ .@npcname$ +"]";
					mes "Explanation for Longest Ownage:";
					mes "^996600Rank: ^006699NAME ^00AAAA(Ownage) ^000000on :";
					mes "    ^EE8800TIME^000000";
					mes " ";
					mes "The ^00AAAAOwnage^000000 added every time any guild member killed another player that doesn't belong to their guild. IT will reset when any of the guild member was killed by ANY player.";
					mes " ";
					mes "Then it record in the server the ^EE8800TIME^000000 when the guild got the longest ownage.";
					mes " ";
					mes "Any guild member killed by monsters, homunculi or pets will not reset the ownage count.";
					next;
					if (getvariableofnpc(.MinLevel, "PK_Mechanics")) {
						mes "["+ .@npcname$ +"]";
						mes "Both players (^FF0000THE TARGET^000000 and ^00AA00THE KILLER^000000) must have at least a minimum base level of "+getvariableofnpc(.MinLevel, "PK_Mechanics")+" in order to get an announcement or get in the rankings.";
						next;
					}
					if (getvariableofnpc(.OverkillPunish, "PK_Mechanics")) {
						mes "["+ .@npcname$ +"]";
						mes "^FF3030--- Applicable on PvP Rankings ---^000000";
						mes "The player is only allowed to kill the same player five (5) times.";
						mes " ";
						mes "If the player continues to kill the same player, their kill count and streak count will be deducted until it reaches to zero (0).";
						mes " ";
						mes "This might remove you from the PvP Rankings.";
						next;
					}
					break;
			}
		}
	}

OnInit:

	// Unit Title
	setunittitle(getnpcid(), "Gero");
	setunitdata(getnpcid(), UDT_GROUP, 32);

	// items not allowed in Non Donator PVP
    	setarray .NoGod_ids[0], 5013, 2629, 2410, 2630, 1530;

	// PVP Room Counter - Transparent Style
		/*while (1) {
			.@count = getmapusers("pvproom") + getmapusers("guild_vs4") + getmapusers("pvproom_1") + getmapusers("pvproom_2") + getmapusers("pvproom_3") + getmapusers("pvproom_4") + getmapusers("pvproom_5") + getmapusers("pvproom_6") + getmapusers("pvproom_7") + getmapusers("pvproom_8") + getmapusers("pvproom_9") + getmapusers("pvproom_10") + getmapusers("pvproom_11") + getmapusers("pvproom_12") + getmapusers("pvproom_13") + getmapusers("pvproom_14") + getmapusers("pvproom_15") + getmapusers("pvproom_16");
        		showscript "PVP Room ["+.@count+"]", getnpcid(1), AREA;
        		sleep 500;
			}
		end;
	}*/

	// PVP Room Counter - Chat Room Style
		while( 1 ){
			.@count = getmapusers("pvproom") + getmapusers("guild_vs4") + getmapusers("pvproom_1") + getmapusers("pvproom_2") + getmapusers("pvproom_3") + getmapusers("pvproom_4") + getmapusers("pvproom_5") + getmapusers("pvproom_6") + getmapusers("pvproom_7") + getmapusers("pvproom_8") + getmapusers("pvproom_9") + getmapusers("pvproom_10") + getmapusers("pvproom_11") + getmapusers("pvproom_12") + getmapusers("pvproom_13") + getmapusers("pvproom_14") + getmapusers("pvproom_15") + getmapusers("pvproom_16");
			waitingroom "PVP Room ["+.@count+"]",0;
			sleep 2000;
			delwaitingroom;
			}
		freeloop(0);
		end;
	}

// PvP Points System: Main Script
-	script	PvP_Points_Main	FAKE_NPC,{

OnInit:
	// Map List for PvP Points System
	setarray .Map$,
		"pvproom","pvproom_1","pvproom_2","pvproom_3","pvproom_4","pvproom_5","pvproom_6","pvproom_7","pvproom_8","pvproom_9","pvproom_10","pvproom_11","pvproom_12","pvproom_13","pvproom_14","pvproom_15","pvproom_16","t_stadium","guild_vs4";

	// Points per Kill
	.added = 1;

	// Maximum Repeat Kill
	.killcount = 3;

	// Points Reduction for Repeating Kills
	.deducted = 2;
	end;

OnPCKillEvent:
	for (.@i = 0; .@i < getarraysize(.Map$); ++.@i) {
		if (strcharinfo(3) == .Map$[.@i]) {
			.@PK_CurID = killedrid;
			if (getcharid(3) == killedrid) end;
			if (@PK_KilledId == .@PK_CurID || !@PK_KilledId)
				@PK_RepeatKills++;
			else
				@PK_RepeatKills = 0;
			if (@PK_RepeatKills >= .killcount) {
				if (PVP_Points > 0)
				PVP_Points -= .deducted;
				dispbottom "You've killed the same character more than "+.killcount +" times. You lose "+ .deducted +" PVP Point!";
			} else {
				PVP_Points += .added;
				dispbottom "You've killed "+ rid2name(.@PK_CurID) +"! You gained "+ (.added * 1) +" PVP Point!";
			}
				@PK_KilledId = .@PK_CurID;
		}
	}
	end;
}

// PvP Points Shop
-	trader	PK_Shop	FAKE_NPC,{
	OnInit:
	tradertype(NST_CUSTOM);
	sellitem Level1_Buff_Scroll,2;
	sellitem Yggseed_Box,5;
	sellitem Yggberry_Box,10;
	sellitem BoT_Box,10;
	sellitem Poison_Box,10;
	sellitem Sunlight_Box,15;
	sellitem saiyan3,150;
	sellitem saiyanhair_black,150;
	sellitem Saiyan_Tail,200;
	sellitem Hat_Saitama_Angry,200;
	sellitem QueenAnzRevenge,300;
	end;

OnCountFunds:
	setcurrency(PVP_Points);
	end;

OnPayFunds:
	if (PVP_Points < @price) end;
	PVP_Points = PVP_Points - @price;
	purchaseok(); end;
}

// Exit
pvproom,99,61,3	script	Unknown Portal#PvP	4_PURPLE_WARP,{
	//dispbottom "You are leaving...";
	progressbar "red",5;
	warp "SavePoint",0,0;
	end;
}

// Duplicate NPCs
//prontera,164,195,4	duplicate(Unknown Portal)	Unknown Portal#PvP1	4_PURPLE_WARP

// Mapflags
pvproom	mapflag	pvp
pvproom	mapflag	nomemo
pvproom	mapflag	noteleport
pvproom	mapflag	nowarp
pvproom	mapflag	nowarpto
pvproom	mapflag	pvp_noparty
pvproom	mapflag	pk_area
pvproom	mapflag	pvp_noguild
pvproom	mapflag	nosave
pvproom	mapflag	nobranch

pvproom_1	mapflag	pvp
pvproom_1	mapflag	nomemo
pvproom_1	mapflag	noteleport
pvproom_1	mapflag	nowarp
pvproom_1	mapflag	nowarpto
pvproom_1	mapflag	pvp_noparty
pvproom_1	mapflag	pk_area
pvproom_1	mapflag	pvp_noguild
pvproom_1	mapflag	nosave
pvproom_1	mapflag	nobranch

pvproom_2	mapflag	pvp
pvproom_2	mapflag	nomemo
pvproom_2	mapflag	noteleport
pvproom_2	mapflag	nowarp
pvproom_2	mapflag	nowarpto
pvproom_2	mapflag	pvp_noparty
pvproom_2	mapflag	pk_area
pvproom_2	mapflag	pvp_noguild
pvproom_2	mapflag	nosave
pvproom_2	mapflag	nobranch

pvproom_3	mapflag	pvp
pvproom_3	mapflag	nomemo
pvproom_3	mapflag	noteleport
pvproom_3	mapflag	nowarp
pvproom_3	mapflag	nowarpto
pvproom_3	mapflag	pvp_noparty
pvproom_3	mapflag	pk_area
pvproom_3	mapflag	pvp_noguild
pvproom_3	mapflag	nosave
pvproom_3	mapflag	nobranch

pvproom_4	mapflag	pvp
pvproom_4	mapflag	nomemo
pvproom_4	mapflag	noteleport
pvproom_4	mapflag	nowarp
pvproom_4	mapflag	nowarpto
pvproom_4	mapflag	pvp_noparty
pvproom_4	mapflag	pk_area
pvproom_4	mapflag	pvp_noguild
pvproom_4	mapflag	nosave
pvproom_4	mapflag	nobranch

pvproom_5	mapflag	pvp
pvproom_5	mapflag	nomemo
pvproom_5	mapflag	noteleport
pvproom_5	mapflag	nowarp
pvproom_5	mapflag	nowarpto
pvproom_5	mapflag	pvp_noparty
pvproom_5	mapflag	pk_area
pvproom_5	mapflag	pvp_noguild
pvproom_5	mapflag	nosave
pvproom_5	mapflag	nobranch

pvproom_6	mapflag	pvp
pvproom_6	mapflag	nomemo
pvproom_6	mapflag	noteleport
pvproom_6	mapflag	nowarp
pvproom_6	mapflag	nowarpto
pvproom_6	mapflag	pvp_noparty
pvproom_6	mapflag	pk_area
pvproom_6	mapflag	pvp_noguild
pvproom_6	mapflag	nosave
pvproom_6	mapflag	nobranch

pvproom_7	mapflag	pvp
pvproom_7	mapflag	nomemo
pvproom_7	mapflag	noteleport
pvproom_7	mapflag	nowarp
pvproom_7	mapflag	nowarpto
pvproom_7	mapflag	pvp_noparty
pvproom_7	mapflag	pk_area
pvproom_7	mapflag	pvp_noguild
pvproom_7	mapflag	nosave
pvproom_7	mapflag	nobranch

pvproom_8	mapflag	pvp
pvproom_8	mapflag	nomemo
pvproom_8	mapflag	noteleport
pvproom_8	mapflag	nowarp
pvproom_8	mapflag	nowarpto
pvproom_8	mapflag	pvp_noparty
pvproom_8	mapflag	pk_area
pvproom_8	mapflag	pvp_noguild
pvproom_8	mapflag	nosave
pvproom_8	mapflag	nobranch

pvproom_9	mapflag	pvp
pvproom_9	mapflag	nomemo
pvproom_9	mapflag	noteleport
pvproom_9	mapflag	nowarp
pvproom_9	mapflag	nowarpto
pvproom_9	mapflag	pvp_noparty
pvproom_9	mapflag	pk_area
pvproom_9	mapflag	pvp_noguild
pvproom_9	mapflag	nosave
pvproom_9	mapflag	nobranch

pvproom_10	mapflag	pvp
pvproom_10	mapflag	nomemo
pvproom_10	mapflag	noteleport
pvproom_10	mapflag	nowarp
pvproom_10	mapflag	nowarpto
pvproom_10	mapflag	pvp_noparty
pvproom_10	mapflag	pk_area
pvproom_10	mapflag	pvp_noguild
pvproom_10	mapflag	nosave
pvproom_10	mapflag	nobranch

pvproom_11	mapflag	pvp
pvproom_11	mapflag	nomemo
pvproom_11	mapflag	noteleport
pvproom_11	mapflag	nowarp
pvproom_11	mapflag	nowarpto
pvproom_11	mapflag	pvp_noparty
pvproom_11	mapflag	pk_area
pvproom_11	mapflag	pvp_noguild
pvproom_11	mapflag	nosave
pvproom_11	mapflag	nobranch

pvproom_12	mapflag	pvp
pvproom_12	mapflag	nomemo
pvproom_12	mapflag	noteleport
pvproom_12	mapflag	nowarp
pvproom_12	mapflag	nowarpto
pvproom_12	mapflag	pvp_noparty
pvproom_12	mapflag	pk_area
pvproom_12	mapflag	pvp_noguild
pvproom_12	mapflag	nosave
pvproom_12	mapflag	nobranch

pvproom_13	mapflag	pvp
pvproom_13	mapflag	nomemo
pvproom_13	mapflag	noteleport
pvproom_13	mapflag	nowarp
pvproom_13	mapflag	nowarpto
pvproom_13	mapflag	pvp_noparty
pvproom_13	mapflag	pk_area
pvproom_13	mapflag	pvp_noguild
pvproom_13	mapflag	nosave
pvproom_13	mapflag	nobranch

pvproom_14	mapflag	pvp
pvproom_14	mapflag	nomemo
pvproom_14	mapflag	noteleport
pvproom_14	mapflag	nowarp
pvproom_14	mapflag	nowarpto
pvproom_14	mapflag	pvp_noparty
pvproom_14	mapflag	pk_area
pvproom_14	mapflag	pvp_noguild
pvproom_14	mapflag	nosave
pvproom_14	mapflag	nobranch

pvproom_15	mapflag	pvp
pvproom_15	mapflag	nomemo
pvproom_15	mapflag	noteleport
pvproom_15	mapflag	nowarp
pvproom_15	mapflag	nowarpto
pvproom_15	mapflag	pvp_noparty
pvproom_15	mapflag	pk_area
pvproom_15	mapflag	pvp_noguild
pvproom_15	mapflag	nosave
pvproom_15	mapflag	nobranch

pvproom_16	mapflag	pvp
pvproom_16	mapflag	nomemo
pvproom_16	mapflag	noteleport
pvproom_16	mapflag	nowarp
pvproom_16	mapflag	nowarpto
pvproom_16	mapflag	pvp_noparty
pvproom_16	mapflag	pk_area
pvproom_16	mapflag	pvp_noguild
pvproom_16	mapflag	nosave
pvproom_16	mapflag	nobranch

t_stadium	mapflag	pvp
t_stadium	mapflag	nomemo
t_stadium	mapflag	noteleport
t_stadium	mapflag	nowarp
t_stadium	mapflag	nowarpto
t_stadium	mapflag	pvp_noparty
t_stadium	mapflag	pk_area
t_stadium	mapflag	pvp_noguild
t_stadium	mapflag	nosave
t_stadium	mapflag	nobranch
































