prontera,165,232,3	script	Aura Master#npc	4_M_JOB_WIZARD,{
	getinventorylist;
	disable_items;
	set @header$,"[^FF8000 Aura Master ^000000]";
	mes @header$;
	mes "I am Zephyr, the Aura Master.";
	mes "I can change the equip location";
	mes "and the color of the aura";
	mes "you desired to change.";
	next;
	mes @header$;
	mes "and... I can only change non-costume Aura.";
	mes " ";
	mes "I must warn you that there's a chance of failing the convertion.";
	next;
	mes @header$;
	mes "Select what aura do you want to change.";
	next;
	.@menu$ = "";
	for (.@i=0; .@i<getarraysize(.auraList$); .@i++)
		.@menu$ += .auraList$[.@i] + ":";
	.@menu$ += "Cancel";
	.@i = select (.@menu$) - 1;
	if (.@i == getarraysize(.auraList$)) close();
	mes @header$;
	mes "Searching your inventory for...";
	mes "^777777"+.auraList$[.@i]+"^000000";
	next;
	switch (.@i) {
		case 0:
			copyarray(.@aura[0], .lhzAura[0], getarraysize(.lhzAura));
			copyarray(.@aura[getarraysize(.@aura)], .lhzAuraM[0], getarraysize(.lhzAuraM));
			break;
		case 1:
			copyarray(.@aura[0], .godAura[0], getarraysize(.godAura));
			copyarray(.@aura[getarraysize(.@aura)], .godAuraM[0], getarraysize(.godAuraM));
			break;
		case 2:
			copyarray(.@aura[0], .amaterasuAura[0], getarraysize(.amaterasuAura));
			copyarray(.@aura[getarraysize(.@aura)], .amaterasuAuraM[0], getarraysize(.amaterasuAuraM));
			break;
		case 3:
			copyarray(.@aura[0], .elfAura[0], getarraysize(.elfAura));
			copyarray(.@aura[getarraysize(.@aura)], .elfAuraM[0], getarraysize(.elfAuraM));
			break;
		case 4:
			copyarray(.@aura[0], .runeOfPower[0], getarraysize(.runeOfPower));
			copyarray(.@aura[getarraysize(.@aura)], .runeOfPowerM[0], getarraysize(.runeOfPowerM));
			break;
		case 5:
			copyarray(.@aura[0], .thunderAura[0], getarraysize(.thunderAura));
			copyarray(.@aura[getarraysize(.@aura)], .thunderAuraM[0], getarraysize(.thunderAuraM));
			break;
		case 6:
			copyarray(.@aura[0], .aresAura[0], getarraysize(.aresAura));
			copyarray(.@aura[getarraysize(.@aura)], .aresAuraM[0], getarraysize(.aresAuraM));
			break;
		case 7:
			copyarray(.@aura[0], .AthenaAura[0], getarraysize(.AthenaAura));
			copyarray(.@aura[getarraysize(.@aura)], .AthenaAuraM[0], getarraysize(.AthenaAuraM));
			break;
		case 8:
			copyarray(.@aura[0], .BatmanAura[0], getarraysize(.BatmanAura));
			copyarray(.@aura[getarraysize(.@aura)], .BatmanAuraM[0], getarraysize(.BatmanAuraM));
			break;
		case 9:
			copyarray(.@aura[0], .CronusAura[0], getarraysize(.CronusAura));
			copyarray(.@aura[getarraysize(.@aura)], .CronusAuraM[0], getarraysize(.CronusAuraM));
			break;
		case 10:
			copyarray(.@aura[0], .FirexAura[0], getarraysize(.FirexAura));
			copyarray(.@aura[getarraysize(.@aura)], .FirexAuraM[0], getarraysize(.FirexAuraM));
			break;
		case 11:
			copyarray(.@aura[0], .FluzAura[0], getarraysize(.FluzAura));
			copyarray(.@aura[getarraysize(.@aura)], .FluzAuraM[0], getarraysize(.FluzAuraM));
			break;
		case 12:
			copyarray(.@aura[0], .IncendAura[0], getarraysize(.IncendAura));
			copyarray(.@aura[getarraysize(.@aura)], .IncendAuraM[0], getarraysize(.IncendAuraM));
			break;
		case 13:
			copyarray(.@aura[0], .ThorAura[0], getarraysize(.ThorAura));
			copyarray(.@aura[getarraysize(.@aura)], .ThorAuraM[0], getarraysize(.ThorAuraM));
			break;
		case 14:
			copyarray(.@aura[0], .ZartAura[0], getarraysize(.ZartAura));
			copyarray(.@aura[getarraysize(.@aura)], .ZartAuraM[0], getarraysize(.ZartAuraM));
			break;
		case 15:
			copyarray(.@aura[0], .ZenAura[0], getarraysize(.ZenAura));
			copyarray(.@aura[getarraysize(.@aura)], .ZenAuraM[0], getarraysize(.ZenAuraM));
			break;
		case 16:
			copyarray(.@aura[0], .ZeusAura[0], getarraysize(.ZeusAura));
			copyarray(.@aura[getarraysize(.@aura)], .ZeusAuraM[0], getarraysize(.ZeusAuraM));
			break;
		case 17:
			copyarray(.@aura[0], .BrightAura[0], getarraysize(.BrightAura));
			copyarray(.@aura[getarraysize(.@aura)], .BrightAuraM[0], getarraysize(.BrightAuraM));
			break;
		case 18:
			copyarray(.@aura[0], .ElectricAura[0], getarraysize(.ElectricAura));
			copyarray(.@aura[getarraysize(.@aura)], .ElectricAuraM[0], getarraysize(.ElectricAuraM));
			break;
		case 19:
			copyarray(.@aura[0], .HerculesAura[0], getarraysize(.HerculesAura));
			copyarray(.@aura[getarraysize(.@aura)], .HerculesAuraM[0], getarraysize(.HerculesAuraM));
			break;
		case 20:
			copyarray(.@aura[0], .HestiaAura[0], getarraysize(.HestiaAura));
			copyarray(.@aura[getarraysize(.@aura)], .HerculesAuraM[0], getarraysize(.HerculesAuraM));
			break;
		case 21:
			copyarray(.@aura[0], .NartAura[0], getarraysize(.NartAura));
			copyarray(.@aura[getarraysize(.@aura)], .NartAuraM[0], getarraysize(.NartAuraM));
			break;
	}
	.@menu$ = "";
	freeloop(1);
	for (.@k=0; .@k<getarraysize(.@aura); .@k++) {
		for (.@j=0; .@j<@inventorylist_count; .@j++) {
			if (@inventorylist_id[.@j] == .@aura[.@k] && (@inventorylist_card1[.@j] != 255 && @inventorylist_card1[.@j] != 254) && @inventorylist_equip[.@j] == 0) {
				setarray(.@auraIdx$[getarraysize(.@auraIdx$)], .@j);
				.@menu$ += getitemname(@inventorylist_id[.@j]) + ":";
				.@found = 1;
			}
		}
	}
	.@menu$ += "Cancel";
	if (!.@found) {
		mes @header$;
		mes "No ^777777"+.auraList$[.@i]+"s^000000 found in your inventory.";
		close();
	}
	.@j = select (.@menu$) - 1;
	.@itm = @inventorylist_id[atoi(.@auraIdx$[.@j])];
	if (.@j == getarraysize(.@auraIdx$)) close();
	switch(getiteminfo(.@itm, 5)) {
		case 1:
			switch (.@i) {
				case 0: setd(".@auraList$", ".lhzAura"); setd(".@auraListEqp$", ".lhzAuraM"); break;
				case 1: setd(".@auraList$", ".godAura"); setd(".@auraListEqp$", ".godAuraM"); break;
				case 2: setd(".@auraList$", ".amaterasuAura"); setd(".@auraListEqp$", ".amaterasuAuraM"); break;
				case 3: setd(".@auraList$", ".elfAura"); setd(".@auraListEqp$", ".elfAuraM"); break;
				case 4: setd(".@auraList$", ".runeOfPower"); setd(".@auraListEqp$", ".runeOfPowerM"); break;
				case 5: setd(".@auraList$", ".thunderAura"); setd(".@auraListEqp$", ".thunderAuraM"); break;
				case 6: setd(".@auraList$", ".aresAura"); setd(".@auraListEqp$", ".aresAuraM"); break;
				case 7: setd(".@auraList$", ".AthenaAura"); setd(".@auraListEqp$", ".AthenaAuraM"); break;
				case 8: setd(".@auraList$", ".BatmanAura"); setd(".@auraListEqp$", ".BatmanAuraM"); break;
				case 9: setd(".@auraList$", ".CronusAura"); setd(".@auraListEqp$", ".CronusAuraM"); break;
				case 10: setd(".@auraList$", ".FirexAura"); setd(".@auraListEqp$", ".FirexAuraM"); break;
				case 11: setd(".@auraList$", ".FluzAura"); setd(".@auraListEqp$", ".FluzAuraM"); break;
				case 12: setd(".@auraList$", ".IncendAura"); setd(".@auraListEqp$", ".IncendAuraM"); break;
				case 13: setd(".@auraList$", ".ThorAura"); setd(".@auraListEqp$", ".ThorAuraM"); break;
				case 14: setd(".@auraList$", ".ZartAura"); setd(".@auraListEqp$", ".ZartAuraM"); break;
				case 15: setd(".@auraList$", ".ZenAura"); setd(".@auraListEqp$", ".ZenAuraM"); break;
				case 16: setd(".@auraList$", ".ZeusAura"); setd(".@auraListEqp$", ".ZeusAuraM"); break;
				case 17: setd(".@auraList$", ".BrightAura"); setd(".@auraListEqp$", ".BrightAuraM"); break;
				case 18: setd(".@auraList$", ".ElectricAura"); setd(".@auraListEqp$", ".ElectricAuraM"); break;
				case 19: setd(".@auraList$", ".HerculesAura"); setd(".@auraListEqp$", ".HerculesAuraM"); break;
				case 20: setd(".@auraList$", ".HestiaAura"); setd(".@auraListEqp$", ".HestiaAuraM"); break;
				case 21: setd(".@auraList$", ".NartAura"); setd(".@auraListEqp$", ".NartAuraM"); break;
			}
			break;
		default:
			switch (.@i) {
				case 0: setd(".@auraList$", ".lhzAuraM"); setd(".@auraListEqp$", ".lhzAura"); break;
				case 1: setd(".@auraList$", ".godAuraM"); setd(".@auraListEqp$", ".godAura"); break;
				case 2: setd(".@auraList$", ".amaterasuAuraM"); setd(".@auraListEqp$", ".amaterasuAura"); break;
				case 3: setd(".@auraList$", ".elfAuraM"); setd(".@auraListEqp$", ".elfAura"); break;
				case 4: setd(".@auraList$", ".runeOfPowerM"); setd(".@auraListEqp$", ".runeOfPower"); break;
				case 5: setd(".@auraList$", ".thunderAuraM"); setd(".@auraListEqp$", ".thunderAura"); break;
				case 6: setd(".@auraList$", ".aresAuraM"); setd(".@auraListEqp$", ".aresAura"); break;
				case 7: setd(".@auraList$", ".AthenaAuraM"); setd(".@auraListEqp$", ".AthenaAura"); break;
				case 8: setd(".@auraList$", ".BatmanAuraM"); setd(".@auraListEqp$", ".BatmanAura"); break;
				case 9: setd(".@auraList$", ".CronusAuraM"); setd(".@auraListEqp$", ".CronusAura"); break;
				case 10: setd(".@auraList$", ".FirexAuraM"); setd(".@auraListEqp$", ".FirexAura"); break;
				case 11: setd(".@auraList$", ".FluzAuraM"); setd(".@auraListEqp$", ".FluzAura"); break;
				case 12: setd(".@auraList$", ".IncendAuraM"); setd(".@auraListEqp$", ".IncendAura"); break;
				case 13: setd(".@auraList$", ".ThorAuraM"); setd(".@auraListEqp$", ".ThorAura"); break;
				case 14: setd(".@auraList$", ".ZartAuraM"); setd(".@auraListEqp$", ".ZartAura"); break;
				case 15: setd(".@auraList$", ".ZenAuraM"); setd(".@auraListEqp$", ".ZenAura"); break;
				case 16: setd(".@auraList$", ".ZeusAuraM"); setd(".@auraListEqp$", ".ZeusAura"); break;
				case 17: setd(".@auraList$", ".BrightAuraM"); setd(".@auraListEqp$", ".BrightAura"); break;
				case 18: setd(".@auraList$", ".ElectricAuraM"); setd(".@auraListEqp$", ".ElectricAura"); break;
				case 19: setd(".@auraList$", ".HerculesAuraM"); setd(".@auraListEqp$", ".HerculesAura"); break;
				case 20: setd(".@auraList$", ".HestiaAuraM"); setd(".@auraListEqp$", ".HestiaAura"); break;
				case 21: setd(".@auraList$", ".NartAuraM"); setd(".@auraListEqp$", ".NartAura"); break;
			}
			break;
	}
	copyarray(.@newAura[0], getd(.@auraList$+"[0]"), getarraysize(getd(.@auraList$+"[0]")));
	copyarray(.@newAuraEqp[0], getd(.@auraListEqp$+"[0]"), getarraysize(getd(.@auraListEqp$+"[0]")));
	for (.@l=0; .@l<getarraysize(.@newAura); .@l++) {
		if (.@itm == .@newAura[.@l])
			.@idx = .@l;
	}
	.@itmEqp = getd(.@auraListEqp$+"["+.@idx+"]");
	mes @header$;
	mes "What do you want to do with your";
	mes "^777777"+getitemname(.@itm)+"^000000?";
	next;
	switch (select("Change Aura Color:Change Equip Location:Cancel")) {
		default:
			close();
			break;

		case 1:
			mes @header$;
			mes "Choose what color you want.";
			next;
			.@menu$ = "";
			for (.@i=0; .@i<getarraysize(.@newAura); .@i++) {
				if (.@itm != .@newAura[.@i]) {
					setarray(.@exAura[getarraysize(.@exAura)], .@newAura[.@i]); 
					.@menu$ += getitemname(.@newAura[.@i]) + ":";
				}
			}
			.@menu$ += "Cancel";
			.@i = select (.@menu$) - 1;
			if (.@i == getarraysize(.@exAura)) close();
			mes @header$;
			mes "Changing the color of your";
			mes "^777777"+getitemname(.@itm)+"^000000 to";
			mes "^3366ff"+getitemname(.@exAura[.@i])+"^000000";
			mes "will cost "+F_InsertPlural(.itemReq[1], getitemname(.itemReq[0]), 0, "^009900%d %s^000000")+".";
			next;
			mes @header$;
			mes "Do you want to proceed?";
			next;
			if (select("Yes:No") == 2) close();
			if (countitem(.itemReq[0]) < .itemReq[1]) {
				mes @header$;
				mes "It seems that you don't have an Aura Coupon.";
				close();
			}
			delitem(.itemReq[0], .itemReq[1]);
			.@x = rand(100);
			if (.@x < .successRateRecolor) {
				specialeffect(EF_REFINEFAIL, AREA, getcharid(3));
				mes @header$;
				mes "Sorry, "+strcharinfo(0)+".";
				mes "The re-coloring of the aura has failed.";
				close();
			}
			specialeffect(EF_REFINEOK, AREA, getcharid(3));
			delitem(.@itm, 1);
			getitem(.@exAura[.@i], 1);
			mes @header$;
			mes "Here's your ^3366ff"+getitemname(.@exAura[.@i])+"^000000.";
			close();
			break;

		case 2:
			mes @header$;
			mes "Your ^777777"+getitemname(.@itm)+"^000000 is equippable on your "+(getiteminfo(.@itm, 5) == 1 ? "lower":"middle")+" headgear.";
			mes " ";
			mes "Changing its equip location";
			mes "to "+(getiteminfo(.@itm, 5) != 1 ? "lower":"middle") + " headgear";
			mes "will cost "+F_InsertPlural(.itemReq[3], getitemname(.itemReq[2]), 0, "^009900%d %s^000000")+".";
			next;
			mes @header$;
			mes "Do you want to proceed?";
			next;
			if (select("Yes:No") == 2) close();
			if (countitem(.itemReq[2]) < .itemReq[3]) {
				mes @header$;
				mes "Sorry, but you don't have";
				mes "enough Black Credits to proceed.";
				close();
			}
			delitem(.itemReq[2], .itemReq[3]);
			specialeffect(EF_REFINEOK, AREA, getcharid(3));
			delitem(.@itm, 1);
			getitem(.@itmEqp, 1);
			mes @header$;
			mes "Your ^777777"+getitemname(.@itm)+"^000000 is now equippable to your "+(getiteminfo(.@itm, 5) != 1 ? "lower":"middle")+" headgear.";
			close();
			break;
	}
	end;

OnInit:
	.successRateRecolor = 0;		// Set this to 0 to disable the success rate (Recolor).
	.successRateRelocate = 0;		// Set this to 0 to disable the success rate (Relocate).
	
	setarray .itemReq[0], Aura_Coupon, 1, Black_Credit, 5;
	
	setarray(.auraList$[0], "Lighthalzen Aura", "God Aura", "Amaterasu Aura", "Elf Aura", "Rune of Power", "Thunder Aura", "Aura of Ares", "Aura of Athena", "Aura of Nifelheim", "Aura of Cornus", "Aura of Hidden Volcano", "Aura of Galactical Fluctuation", "Aura of Incendiary Flames", "Aura of Thor", "Aura of Zart", "Aura of Zen", "Aura of Zeus", "Aura of Glittering Mist", "Aura of Lesser Thunder", "Aura of Hercules", "Aura of Hestia", "Aura of Interstellar Force");

	setarray(.lhzAura[0], L_Lighthalzen_Aura_Black, L_Lighthalzen_Aura_Blue, L_Lighthalzen_Aura_Gold, L_Lighthalzen_Aura_Green, L_Lighthalzen_Aura_Pink, L_Lighthalzen_Aura_Red, L_Lighthalzen_Aura_White);
	setarray(.lhzAuraM[0], M_Lighthalzen_Aura_Black, M_Lighthalzen_Aura_Blue, M_Lighthalzen_Aura_Gold, M_Lighthalzen_Aura_Green, M_Lighthalzen_Aura_Pink, M_Lighthalzen_Aura_Red, M_Lighthalzen_Aura_White);

	setarray(.godAura[0], L_God_Aura_Black, L_God_Aura_Blue, L_God_Aura_Gold, L_God_Aura_Green, L_God_Aura_Pink, L_God_Aura_Red, L_God_Aura_White);
	setarray(.godAuraM[0], M_God_Aura_Black, M_God_Aura_Blue, M_God_Aura_Gold, M_God_Aura_Green, M_God_Aura_Pink, M_God_Aura_Red, M_God_Aura_White);

	setarray(.amaterasuAura[0], Black_Amaterasu_Aura_L, Blue_Amaterasu_Aura_L, Gold_Amaterasu_Aura_L, Green_Amaterasu_Aura_L, Navy_Blue_Amaterasu_Aura_L, Pink_Amaterasu_Aura_L, Red_Amaterasu_Aura_L, White_Amaterasu_Aura_L);
	setarray(.amaterasuAuraM[0], Black_Amaterasu_Aura_M, Blue_Amaterasu_Aura_M, Gold_Amaterasu_Aura_M, Green_Amaterasu_Aura_M, Navy_Blue_Amaterasu_Aura_M, Pink_Amaterasu_Aura_M, Red_Amaterasu_Aura_M, White_Amaterasu_Aura_M);

	setarray(.elfAura[0], L_Elf_Aura_Black, L_Elf_Aura_Blue, L_Elf_Aura_Gold, L_Elf_Aura_Green, L_Elf_Aura_Pink, L_Elf_Aura_Red, L_Elf_Aura_White);
	setarray(.elfAuraM[0], M_Elf_Aura_Black, M_Elf_Aura_Blue, M_Elf_Aura_Gold, M_Elf_Aura_Green, M_Elf_Aura_Pink, M_Elf_Aura_Red, M_Elf_Aura_White);

	setarray(.runeOfPower[0], L_Rune_Of_Power_Black, L_Rune_Of_Power_Blue, L_Rune_Of_Power_Gold, L_Rune_Of_Power_Green, L_Rune_Of_Power_Pink, L_Rune_Of_Power_Red, L_Rune_Of_Power_White);
	setarray(.runeOfPowerM[0], M_Rune_Of_Power_Black, M_Rune_Of_Power_Blue, M_Rune_Of_Power_Gold, M_Rune_Of_Power_Green, M_Rune_Of_Power_Pink, M_Rune_Of_Power_Red, M_Rune_Of_Power_White);

	setarray(.thunderAura[0], L_Thunder_Aura_BLK, L_Thunder_Aura_BLU, L_Thunder_Aura_GRN, L_Thunder_Aura_ORG, L_Thunder_Aura_PNK, L_Thunder_Aura_PRP, L_Thunder_Aura_RED, L_Thunder_Aura_YLW);
	setarray(.thunderAuraM[0], M_Thunder_Aura_BLK, M_Thunder_Aura_BLU, M_Thunder_Aura_GRN, M_Thunder_Aura_ORG, M_Thunder_Aura_PNK, M_Thunder_Aura_PRP, M_Thunder_Aura_RED, M_Thunder_Aura_YLW);

	setarray(.aresAura[0], L_ARES_BLUE, L_ARES_GREEN, L_ARES_ORANGE, L_ARES_PINK, L_ARES_PURPLE, L_ARES_RED, L_ARES_YELLOW);
	setarray(.aresAuraM[0], M_ARES_BLUE, M_ARES_GREEN, M_ARES_ORANGE, M_ARES_PINK, M_ARES_PURPLE, M_ARES_RED, M_ARES_YELLOW);
	
	setarray(.AthenaAura[0], L_ATHENA_BLUE, L_ATHENA_GREEN, L_ATHENA_ORANGE, L_ATHENA_PINK, L_ATHENA_PURPLE, L_ATHENA_RED, L_ATHENA_YELLOW);
	setarray(.AthenaAuraM[0], M_ATHENA_BLUE, M_ATHENA_GREEN, M_ATHENA_ORANGE, M_ATHENA_PINK, M_ATHENA_PURPLE, M_ATHENA_RED, M_ATHENA_YELLOW);
	
	setarray(.BatmanAura[0], L_BATMAN_BLUE, L_BATMAN_GREEN, L_BATMAN_ORANGE, L_BATMAN_PINK, L_BATMAN_PURPLE, L_BATMAN_RED, L_BATMAN_YELLOW);
	setarray(.BatmanAuraM[0], M_BATMAN_BLUE, M_BATMAN_GREEN, M_BATMAN_ORANGE, M_BATMAN_PINK, M_BATMAN_PURPLE, M_BATMAN_RED, M_BATMAN_YELLOW);
	
	setarray(.CronusAura[0], L_CRONUS_BLACK,L_CRONUS_BLUE, L_CRONUS_GREEN, L_CRONUS_ORANGE, L_CRONUS_PINK, L_CRONUS_PURPLE, L_CRONUS_RED, L_CRONUS_YELLOW);
	setarray(.CronusAuraM[0], M_CRONUS_BLACK,M_CRONUS_BLUE, M_CRONUS_GREEN, M_CRONUS_ORANGE, M_CRONUS_PINK, M_CRONUS_PURPLE, M_CRONUS_RED, M_CRONUS_YELLOW);
	
	setarray(.FirexAura[0], L_FIREX_BLUE, L_FIREX_GREEN, L_FIREX_ORANGE, L_FIREX_PINK, L_FIREX_PURPLE, L_FIREX_RED, L_FIREX_YELLOW);
	setarray(.FirexAuraM[0], M_FIREX_BLUE, M_FIREX_GREEN, M_FIREX_ORANGE, M_FIREX_PINK, M_FIREX_PURPLE, M_FIREX_RED, M_FIREX_YELLOW);
	
	setarray(.FluzAura[0], L_FLUZ_BLUE, L_FLUZ_BROWN, L_FLUZ_GOLD, L_FLUZ_GREEN, L_FLUZ_ORANGE, L_FLUZ_PINK, L_FLUZ_PURPLE, L_FLUZ_RED, L_FLUZ_SILVER, L_FLUZ_WHITE, L_FLUZ_YELLOW);
	setarray(.FluzAuraM[0], M_FLUZ_BLUE, M_FLUZ_BROWN, M_FLUZ_GOLD, M_FLUZ_GREEN, M_FLUZ_ORANGE, M_FLUZ_PINK, M_FLUZ_PURPLE, M_FLUZ_RED, M_FLUZ_SILVER, M_FLUZ_WHITE, M_FLUZ_YELLOW);
	
	setarray(.IncendAura[0], L_INCEND_BLUE, L_INCEND_GREEN, L_INCEND_ORANGE, L_INCEND_PINK, L_INCEND_PURPLE, L_INCEND_RED, L_INCEND_YELLOW);
	setarray(.IncendAuraM[0], M_INCEND_BLUE, M_INCEND_GREEN, M_INCEND_ORANGE, M_INCEND_PINK, M_INCEND_PURPLE, M_INCEND_RED, M_INCEND_YELLOW);
	
	setarray(.ThorAura[0], L_THOR_BLACK, L_THOR_BLUE, L_THOR_GREEN, L_THOR_ORANGE, L_THOR_PINK, L_THOR_PURPLE, L_THOR_RED, L_THOR_YELLOW);
	setarray(.ThorAuraM[0], M_THOR_BLACK, M_THOR_BLUE, M_THOR_GREEN, M_THOR_ORANGE, M_THOR_PINK, M_THOR_PURPLE, M_THOR_RED, M_THOR_YELLOW);
	
	setarray(.ZartAura[0], L_ZART_BLACK, L_ZART_BLUE, L_ZART_GREEN, L_ZART_ORANGE, L_ZART_PINK, L_ZART_PURPLE, L_ZART_RED, L_ZART_YELLOW);
	setarray(.ZartAuraM[0], M_ZART_BLACK, M_ZART_BLUE, M_ZART_GREEN, M_ZART_ORANGE, M_ZART_PINK, M_ZART_PURPLE, M_ZART_RED, M_ZART_YELLOW);
	
	setarray(.ZenAura[0], L_ZEN_BLUE, L_ZEN_GREEN, L_ZEN_ORANGE, L_ZEN_PINK, L_ZEN_PURPLE, L_ZEN_RED, L_ZEN_YELLOW);
	setarray(.ZenAuraM[0], M_ZEN_BLUE, M_ZEN_GREEN, M_ZEN_ORANGE, M_ZEN_PINK, M_ZEN_PURPLE, M_ZEN_RED, M_ZEN_YELLOW);
	
	setarray(.ZeusAura[0], L_ZEUS_BLACK, L_ZEUS_BLUE, L_ZEUS_GREEN, L_ZEUS_ORANGE, L_ZEUS_PINK, L_ZEUS_PURPLE, L_ZEUS_RED, L_ZEUS_WHITE, L_ZEUS_YELLOW);
	setarray(.ZeusAuraM[0], M_ZEUS_BLACK, M_ZEUS_BLUE, M_ZEUS_GREEN, M_ZEUS_ORANGE, M_ZEUS_PINK, M_ZEUS_PURPLE, M_ZEUS_RED, M_ZEUS_WHITE, M_ZEUS_YELLOW);
	
	setarray(.BrightAura[0], L_BRIGHT_BLUE, L_BRIGHT_GREEN, L_BRIGHT_ORANGE, L_BRIGHT_PINK, L_BRIGHT_PURPLE, L_BRIGHT_RED, L_BRIGHT_YELLOW);
	setarray(.BrightAuraM[0], M_BRIGHT_BLUE, M_BRIGHT_GREEN, M_BRIGHT_ORANGE, M_BRIGHT_PINK, M_BRIGHT_PURPLE, M_BRIGHT_RED, M_BRIGHT_YELLOW);
	
	setarray(.ElectricAura[0], L_ELECTRIC_BLACK, L_ELECTRIC_BLUE, L_ELECTRIC_GREEN, L_ELECTRIC_ORANGE, L_ELECTRIC_PINK, L_ELECTRIC_PURPLE, L_ELECTRIC_RED, L_ELECTRIC_TREVAS1, L_ELECTRIC_TREVAS2, L_ELECTRIC_WHITE, L_ELECTRIC_YELLOW);
	setarray(.ElectricAuraM[0], M_ELECTRIC_BLACK, M_ELECTRIC_BLUE, M_ELECTRIC_GREEN, M_ELECTRIC_ORANGE, M_ELECTRIC_PINK, M_ELECTRIC_PURPLE, M_ELECTRIC_RED, M_ELECTRIC_TREVAS1, M_ELECTRIC_TREVAS2, M_ELECTRIC_WHITE, M_ELECTRIC_YELLOW);
	
	setarray(.HerculesAura[0], L_HERCULES_BLACK, L_HERCULES_BLUE, L_HERCULES_GREEN, L_HERCULES_ORANGE, L_HERCULES_PINK, L_HERCULES_PURPLE, L_HERCULES_RED, L_HERCULES_YELLOW);
	setarray(.HerculesAuraM[0], M_HERCULES_BLACK, M_HERCULES_BLUE, M_HERCULES_GREEN, M_HERCULES_ORANGE, M_HERCULES_PINK, M_HERCULES_PURPLE, M_HERCULES_RED, M_HERCULES_YELLOW);
	
	setarray(.HestiaAura[0], L_HESTIA_BLACK, L_HESTIA_BLUE, L_HESTIA_GREEN, L_HESTIA_ORANGE, L_HESTIA_PINK, L_HESTIA_PURPLE, L_HESTIA_RED, L_HESTIA_WHITE, L_HESTIA_YELLOW);
	setarray(.HestiaAuraM[0], M_HESTIA_BLACK, M_HESTIA_BLUE, M_HESTIA_GREEN, M_HESTIA_ORANGE, M_HESTIA_PINK, M_HESTIA_PURPLE, M_HESTIA_RED, M_HESTIA_WHITE, M_HESTIA_YELLOW);
	
	setarray(.NartAura[0], L_NART_BLACK, L_NART_BLUE, L_NART_GREEN, L_NART_ORANGE, L_NART_PINK, L_NART_PURPLE, L_NART_RED, L_NART_YELLOW);
	setarray(.NartAuraM[0], M_NART_BLACK, M_NART_BLUE, M_NART_GREEN, M_NART_ORANGE, M_NART_PINK, M_NART_PURPLE, M_NART_RED, M_NART_YELLOW);
	end;
}
