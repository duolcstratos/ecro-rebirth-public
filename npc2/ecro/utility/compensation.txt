
prontera,146,175,5	script	Compensation Staff	4_F_KAFRA8,{

	if (#COMP100822) {
		mes "[^FF8000 Compensation Staff ^000000]";
		mes "You have already redeemed your compensation.";
		mes " ";
		mes "Sorry for the inconvenience.";
		close;
	}
	#COMP100822++;

	query_sql("SELECT `last_unique_id` FROM login WHERE `account_id`="+getcharid(3), @uid$);
	for (.@i=0; .@i<getarraysize(.cmp_list$); .@i++) {
		if (.cmp_list$[.@i] != @uid$) continue;
		else {
			.@found = 1;
			break;
		}
	}
	getitembound Bubble_Gum,5,1;
	getitembound Str_Dish10_,10,1;
	getitembound Agi_Dish10_,10,1;
	getitembound Int_Dish10_,10,1;
	getitembound Dex_Dish10_,10,1;
	getitembound Luk_Dish10_,10,1;
	getitembound Vit_Dish10_,10,1;
	getitembound Level200_Buff_Scroll,25,1;

	/*if (!.@found) {
		setarray .cmp_list$[getarraysize(.cmp_list$)], @uid$;
		getitembound Job_Ticket, 1, 1;
		getitembound Name_Passport, 1, 1;
		getitembound Max_Weight_Up_Scroll, 10, 1;
		rentitem Valkyrie_Armor, 7776000;
		rentitem Boarding_Halter, 2592000;
		#VOTEPOINTS += 150;
		message strcharinfo(0), "You have received 150 Vote Points. Cherish it well.";
	}*/

	if (getstrlen(@uid$) > 0)
		@uid$ = "";

	mes "[^FF8000 Compensation Staff ^000000]";
	mes "Here's your compensation.";
	mes "Thanks for supporting the server.";
	mes " ";
	mes "Sorry for the inconvenience.";
	close;

OnInit:
	waitingroom("Server Compensation",0);
	//while (1) {
		//showscript "Server Compensation", getnpcid(0);
		//sleep 500;
		//}
	end;
}
