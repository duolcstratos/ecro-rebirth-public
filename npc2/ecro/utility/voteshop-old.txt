//==== DarkRO Scripts ====================================
//= Vote Shop
//===== By: ================================================== 
//= [GM] Brenth
//===== Current Version: ===================================== 
//= 1.2
//===== Description: ========================================= 
//= Simple Voting Shop.
//===== Changelog: =========================================== 
//= Not yet tested for bugs
//= 1.0 Implementation
//============================================================

prt_in,135,34,3	script	Voting Staff	4_F_NOVICE,{

	set @header$,"[^FF8000 Voting Staff ^000000]";
	mes @header$; 
	mes "Good day, " + (Sex ? "Sir!" : "Madam!");
	mes "Make sure you have voted the server today to earn more points!";
	mes " ";
	mes "How may I help you?";
	mes "You have ^FF0000"+#VOTEPOINTS+"^000000 Vote Points.";
	next;
	switch(select("Redeem Vote Points:Visit Vote Shop:Cancel")) {
		case 1:
			query_sql("SELECT `value` FROM `cp_loginprefs` WHERE `account_id`='"+escape_sql(getcharid(3))+"' AND `name`='FluxVotePoints'", .@pts);
			if (.@pts < 1) {
				mes @header$; 
				mes "Sorry, you don't have any accumulated vote points stored from your account.";
				close;
			}
			#VOTEPOINTS += .@pts;
			query_sql("UPDATE `cp_loginprefs` SET `value`=0 WHERE `account_id`='"+escape_sql(getcharid(3))+"' AND `name`='FluxVotePoints'");
			query_logsql("INSERT INTO cp_votes_redeem(`account_id`,`charname`,`points_claimed`) VALUES('"+getcharid(3)+"','"+escape_sql(strcharinfo(0))+"','"+.@pts+"')");
			mes @header$; 
			mes "Here is your ^3366ff"+(.@pts)+"^000000 Vote Points.";
			mes "Use it well.";
			close;
			break;
		case 2:
			mes @header$; 
			mes "Please select an item category.";
			mes " ";
			mes "You have ^FF0000"+#VOTEPOINTS+"^000000 Vote Points.";
			next;
			switch(select("Miscellaneous:Headgears:Accessories:Cancel")) {
			case 1:
				openshop("Vote_Misc_Shop");
				end;
			case 2:
				openshop("Vote_Headgear_Shop");
				end;
			case 3:
				openshop("Vote_Acce_Shop");
				end;
			case 4:
				L_Cancel:
					mes @header$;
					mes "Okay, see you around.";
					close;
					end;
			}
			break;
		default: goto L_Cancel;	break;
	}
}


-	trader	Vote_Misc_Shop	-1,{
	OnInit:
	tradertype(NST_CUSTOM);
	sellitem Red_Potion, 10;
	/*sellitem Battle_Manual_X3,5;
	sellitem Bubble_Gum,10;
	sellitem Max_Weight_Up_Scroll,40;
	sellitem Costume_Ticket,50;
	sellitem Name_Passport,150;*/
	end;

OnCountFunds:
	setcurrency(#VOTEPOINTS); end;

OnPayFunds:
	if( #VOTEPOINTS < @price ) end;
	#VOTEPOINTS = #VOTEPOINTS - @price;
	purchaseok(); end;
}

-	trader	Vote_Headgear_Shop	-1,{
	OnInit:
	tradertype(NST_CUSTOM);
	sellitem Red_Potion, 10;
	// Upper
	/*sellitem Beer_Cap,75;
	sellitem Drooping_White_Kitty,75;
	sellitem Notice_Board,100;
	sellitem Aniv_Star_Hat,100;
	sellitem Ignis_Cap,100;
	sellitem Samambaia,100;
	sellitem Flying_Angel_,125;
	sellitem Jaguar_Hat,125;
	sellitem Happy_Wig,125;
	sellitem Bell_Ribbon,125;
	sellitem Bird_Nest,150;
	sellitem Peacock_Feather,150;
	sellitem Fish_Head_Hat,175;
	sellitem Bizofnil_Wing_Deco,200;
	sellitem J_Captain_Hat,250;
	sellitem Sakura_Mist_Hat,250;
	sellitem Hyegun_Hat,250;
	sellitem Coppola,250;
	sellitem Gold_Tiara_,300;

	// Mid
	sellitem Ifrit's_Ear,75;
	sellitem Wings_Of_Victory,250;

	// Lower
	sellitem Romantic_White_Flower,75;
	sellitem Poring_Letter,100;*/
	end;

OnCountFunds:
	setcurrency(#VOTEPOINTS); end;

OnPayFunds:
	if( #VOTEPOINTS < @price ) end;
	#VOTEPOINTS = #VOTEPOINTS - @price;
	purchaseok(); end;
}

-	trader	Vote_Acce_Shop	-1,{
	OnInit:
	tradertype(NST_CUSTOM);
	sellitem Red_Potion, 10;
	/*sellitem Ring_of_Kanan,100;
	sellitem C1_10P_Accelerator,100;
	sellitem Ring_of_Garazeb,100;
	sellitem Ring_of_Hera,100;
	sellitem Ring_of_Sabine,100;
	sellitem Ring_of_Ezra,100;*/
	end;

OnCountFunds:
	setcurrency(#VOTEPOINTS); end;

OnPayFunds:
	if( #VOTEPOINTS < @price ) end;
	#VOTEPOINTS = #VOTEPOINTS - @price;
	purchaseok(); end;
}