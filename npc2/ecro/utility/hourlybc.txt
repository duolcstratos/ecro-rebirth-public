//==== DarkRO Scripts ====================================
//= Hourly Broadcaster
//===== By: ================================================== 
//= [GM] Brenth
//===== Current Version: ===================================== 
//= 1.0
//===== Description: ========================================= 
//= Announcer every hour
//===== Changelog: =========================================== 
//= Not yet tested for bugs
//= 1.0 Implementation
//============================================================

-	script	HourlyBC	FAKE_NPC,{

OnInit:
	bindatcmd("hourlybc",strnpcinfo(3)+"::OnHrBCsettings",99,99);
OnReload:
	set .a,1;
	set .ac$,"0xFFFF00"; //Announce color
	set .a_tl1$,"Welcome to Empire Classic! Join our Discord and Facebook for Events & Updates.";
	set .a_tl2$,"Discord: www.ecro.gg/discord & FB: www.ecro.gg/facebook";
	set .a_yl1$,"Staffs will never ask for your password. Please report any suspicious activities.";
	set .a_yl2$,"Vote for every 24 hours and write us a Review to support ecRO. https://ecro.gg/rms";
	set .a_gl1$,"Event is now on-going on our forums! Visit: www.forum.ecro.gg for more details";
	set .a_gl1$,"Please visit: www.forum.ecro.gg for more details";
	set .a_gl1$,"Concerns and Inquiries? Type @request <message>.";
	set .a_gl2$,"Please use English when using Broadcaster and DO NOT use it on Trashtalks or Nonsense topic.";
	end;

OnHrBCsettings:
OnWhisperGlobal:
	if(getgroupid() < 1) end;
	mes "[ Announcer ]";
	mes "Hello! This is GM hourly announcer config.";
	next;
	mes "[ Announcer ]";
	mes "Announce Message 1:";
	mes "Line 1: "+.a_tl1$+"";
	mes " ";
	mes "Line 2: "+.a_tl2$+"";
	next;
	mes "[ Announcer ]";
	mes "Announce Message 2:";
	mes "Line 1: "+.a_yl1$+"";
	mes " ";
	mes "Line 2: "+.a_yl2$+"";
	next;
	mes "[ Announcer ]";
	mes "Announce Message 3:";
	mes "Line 1: "+.a_gl1$+"";
	mes " ";
	mes "Line 2: "+.a_gl2$+"";
	next;
	mes "[ Announcer ]";
	mes "Would you like to edit anything sir?";
	menu "Edit message 1",Q_E1,"Edit message 2",Q_E2,"Edit message 3",Q_E3,"Do you want to broadcast it now?",Q_BC,"Reload messages to default.",Q_R;

	Q_E1:
		next;
		mes "[ Announcer ]";
		mes "Okay please enter your first line of your message sir."; 
		input .f_l1$;
		mes " ";
		mes "Next is for the second line of your message sir.";
		input .f_l2$;
		next;
		mes "[ Announcer ]";
		mes "Ok so the message will be.";
		mes "Line 1: "+.f_l1$+"";
		mes " ";
		mes "Line 2: "+.f_l2$+"";
		mes "You ok with this sir?";
		if(select("Yes:Cancel")==2)close;
		next;
		mes "[ Announcer ]";
		mes "Done.";
		set .a_tl1$,.f_l1$;
		set .a_tl2$,.f_l2$;
		mes "Do you want to broadcast it now?";
		if(select("Yes:Cancel")==2)close;
		close2;
		announce ""+.a_tl1$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_tl2$+"",0,""+.ac$+"";
		end;

	Q_E2:
		next;
		mes "[ Announcer ]";
		mes "Okay please enter your first line of your message sir."; 
		input .s_l1$;
		mes " ";
		mes "Next is for the second line of your message sir.";
		input .s_l2$;
		next;
		mes "[ Announcer ]";
		mes "Ok so the message will be.";
		mes "Line 1: "+.s_l1$+"";
		mes " ";
		mes "Line 2: "+.s_l2$+"";
		mes "You ok with this sir?";
		if(select("Yes:Cancel")==2)close;
		next;
		mes "[ Announcer ]";
		mes "Done.";
		set .a_yl1$,.s_l1$;
		set .a_yl2$,.s_l2$;
		mes "Do you want to broadcast it now?";
		if(select("Yes:Cancel")==2)close;
		close2;
		announce ""+.a_yl1$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_yl2$+"",0,""+.ac$+"";
		end;

	Q_E3:
		next;
		mes "[ Announcer ]";
		mes "Okay please enter your first line of your message sir."; 
		input .d_l1$;
		mes " ";
		mes "Next is for the second line of your message sir.";
		input .d_l2$;
		next;
		mes "[ Announcer ]";
		mes "Ok so the message will be.";
		mes "Line 1: "+.d_l1$+"";
		mes " ";
		mes "Line 2: "+.d_l2$+"";
		mes "You ok with this sir?";
		if(select("Yes:Cancel")==2)close;
		next;
		mes "[ Announcer ]";
		mes "Done.";
		set .a_gl1$,.d_l1$;
		set .a_gl2$,.d_l2$;
		mes "Do you want to broadcast it now?";
		if(select("Yes:Cancel")==2)close;
		close2;
		announce ""+.a_gl1$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_gl2$+"",0,""+.ac$+"";
		end;
		
	Q_BC:
		next;
		mes "[ Announcer ]";
		mes "Do you want to broadcast it now?";
		if(select("Yes:Cancel")==2)close;
		close2;
		announce ""+.a_tl1$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_tl2$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_yl1$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_yl2$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_gl1$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_gl2$+"",0,""+.ac$+"";
		end;

	Q_R:
		mes "Messages reloaded.";
		close2;
		donpcevent "HourlyBC::OnReload";
		end;	

OnMinute01:
OnAnnounce:

	sleep2 3000;	
	if(.a == 4) set .a,1;
	if(.a == 1) {
		announce ""+.a_tl1$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_tl2$+"",0,""+.ac$+"";
		set .a,.a+1;
		end;
	}

	if(.a == 2) {
		announce ""+.a_yl1$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_yl2$+"",0,""+.ac$+"";
		set .a,.a+1;
		end;
	}

	if(.a == 3) {
		announce ""+.a_gl1$+"",0,""+.ac$+"";
		sleep2 5000;
		announce ""+.a_gl2$+"",0,""+.ac$+"";
		set .a,.a+1;
		end;
	}
}
	