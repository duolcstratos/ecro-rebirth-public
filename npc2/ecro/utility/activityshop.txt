//==== DarkRO Scripts ====================================
//= Activity Shop
//===== By: ==================================================
//= [GM] Brenth
//===== Current Version: =====================================
//= 1.2
//===== Description: =========================================
//= Simple Activity Shop.
//===== Changelog: ===========================================
//= Not yet tested for bugs
//= 1.0 Implementation
//============================================================

prt_in,136,28,3	script	Activity Shop	4_M_NFLOSTMAN,{

	set @header$,"[^FF8000 Activity Shop ^000000]";
	mes @header$;
	mes "Hi " + (Sex ? "sir!" : "madam!");
	mes " ";
	mes "How have you been, You should do some activities!";
	next;
	switch(select("What do you have?:I need more time!")){
	case 1:
		mes @header$;
		mes "You can get activity points while playing, AFK will stop you from getting points!";
		mes " ";
		mes "^FF0000NOTE^000000: Please choose wisely.";
		next;
		switch(select("Upper Headgears:Middle Headgears:Lower Headgears:Cancel")) {
		//openshop("Activity_Shop");
			case 1:
				openshop("Activity_Shop1");
				end;
			case 2:
				openshop("Activity_Shop2");
				end;
			case 3:
				openshop("Activity_Shop3");
				end;
			case 4:
			L_Cancel:
					mes @header$;
					mes "Okay, see you around.";
					close;
					end;
			}
	case 2:
		mes @header$;
		mes "Just relax, All you have to do is... Uhm....";
		close;
	}
}


-	trader	Activity_Shop1	-1,{
	OnInit:
	tradertype(NST_CUSTOM);
	/*sellitem Red_Potion, 10;*/
	// Upper
	sellitem Hunting_Cap,150;
	sellitem Academy_Graduating_Cap,150;
	sellitem L_Orc_Hero_Helm,176;
	sellitem Asara_Fairy_Hat,176;
	sellitem Baseball_Cap,200;
	sellitem Soulless_Wing_,250;
	sellitem Whikebain_Ears,300;
	sellitem Ancient_Elven_Ear,300;
	sellitem Helm_Of_Abyss,350;
	sellitem Sakura_Milk_Tea_Hat,350;
	sellitem Emperor_Wreath_J,350;
	sellitem Blue_Pajamas_Hat,400;
	sellitem Jaguar_Hat,400;
	sellitem Cat_Ears_Beret,400;
	sellitem Darkness_Helm,500;
	sellitem Classic_Hat,500;
	sellitem Valkyrie_Feather_Band,600;
	sellitem Dice_Hat,750;
	sellitem Snake_Head,750;
	end;

OnCountFunds:
	setcurrency(#ACTIVITYPOINTS); end;

OnPayFunds:
	if( #ACTIVITYPOINTS < @price ) end;
	#ACTIVITYPOINTS = #ACTIVITYPOINTS - @price;
	purchaseok(); end;
}

-	trader	Activity_Shop2	-1,{
	OnInit:
	tradertype(NST_CUSTOM);
	// Mid
	sellitem Eyes_Of_Darkness,126;
	sellitem Round_Eyes,150;
	sellitem Angel_Spirit,200;
	sellitem Black_Glasses,300;
	sellitem Mischievous_Fairy,300;
	sellitem Type_10_Glasses,300;
	sellitem Hairband_Of_Reginleif,400;
	end;

OnCountFunds:
	setcurrency(#ACTIVITYPOINTS); end;

OnPayFunds:
	if( #ACTIVITYPOINTS < @price ) end;
	#ACTIVITYPOINTS = #ACTIVITYPOINTS - @price;
	purchaseok(); end;
}

-	trader	Activity_Shop3	-1,{
	OnInit:
	tradertype(NST_CUSTOM);
	// Lower
	sellitem Donut_In_Mouth,150;
	sellitem Pirate_Dagger,200;
	sellitem Pencil_in_Mouth,250;
	sellitem Red_Glasses,250;
	sellitem Gang_Scarf,300;
	sellitem Antique_Pipe,350;
	end;

OnCountFunds:
	setcurrency(#ACTIVITYPOINTS); end;

OnPayFunds:
	if( #ACTIVITYPOINTS < @price ) end;
	#ACTIVITYPOINTS = #ACTIVITYPOINTS - @price;
	purchaseok(); end;
}
