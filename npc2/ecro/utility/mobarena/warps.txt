/**********************************************
 * Warp Portals
 *********************************************/

arena_m01,19,143,4	script	mobArena01_exit	WARPNPC,2,2,{
	end;

OnTouch:
	//warp("prt_in",rand(283,285),rand(128,130));
	warp "prt_in",284,125;
	end;
}

arena_m01,159,98,4	script	mobArena01_mobArena02	WARPNPC,2,2,{
	end;

OnTouch:
	.@mobArenaMinParty = getvariableofnpc(.mobArenaMinParty, "Alliance Captain#ma");
	.@aid = getcharid(3);
	.@pid = getcharid(1);
	.@i = query_sql("SELECT `entry` FROM ma_access_party WHERE `party_id`="+.@pid, .@entry);
	if (.@i == 0) {
		.@cnt = 0;
		getpartymember(.@pid, 2);
		for (.@i=0; .@i<$@partymembercount; .@i++) {
			if (attachrid($@partymemberaid[.@i])) {
				if (strcharinfo(3) == "arena_m01"
					&& isloggedin(getcharid(3), getcharid(0)))
					.@cnt++;
				else
					continue;
			}
		}
		attachrid(.@aid);
		if (.@cnt < .@mobArenaMinParty)
			dispbottom("[Monster Arena] Sorry, you don't have an access to Monster Arena F2.");
		else {
			mes "^6699FFYou have gathered enough members to enter the Monster Arena F2.";
			mes " ";
			mes "Do you want to enter now?";
			next;
			if (select("Yes:No") == 2) warp("arena_m01",155,98);
			else {
				if (query_sql("SELECT `entry` FROM ma_access_party WHERE `party_id`="+.@pid, .@entry) == 0)
					query_sql("INSERT INTO ma_access_party(`party_id`, `entry`) VALUES("+.@pid+", 1)");
				warp("arena_m02",20,170);
			}
		}
	} else {
		if (.@entry)
			warp("arena_m02",20,170);
		else
			dispbottom("[Monster Arena] Sorry, you don't have an access to Monster Arena F2.");
	}
	end;
}

arena_m02,17,172,0	warp	mobArena02_mobArena01	1,1,arena_m01,155,98

arena_m02,145,58,4	script	mobArena02_mobArena03	WARPNPC,2,2,{
	end;

OnTouch:
	.@mobArenaMinGuild = getvariableofnpc(.mobArenaMinGuild, "Alliance Captain#ma");
	.@aid = getcharid(3);
	.@gid = getcharid(2);
	.@i = query_sql("SELECT `entry` FROM ma_access_guild WHERE `guild_id`="+.@gid, .@entry);
	if (.@i == 0) {
		if (getmapguildusers("arena_m02",.@gid) < .@mobArenaMinGuild)
			dispbottom("[Monster Arena] Sorry, you don't have an access to Monster Arena F3.");
		else {
			mes "^6699FFYou have gathered enough members to enter the Monster Arena F3.";
			mes " ";
			mes "Do you want to enter now?";
			next;
			if (select("Yes:No") == 2) warp("arena_m02",145,55);
			else {
				if (query_sql("SELECT `entry` FROM ma_access_guild WHERE `guild_id`="+.@gid, .@entry) == 0)
					query_sql("INSERT INTO ma_access_guild(`guild_id`, `entry`) VALUES("+.@gid+", 1)");
				warp("arena_m03",189,18);
			}
		}
	} else {
		if (.@entry)
			warp("arena_m03",189,18);
		else
			dispbottom("[Monster Arena] Sorry, you don't have an access to Monster Arena F3.");
	}
	end;
}

arena_m03,189,14,0	warp	mobArena03_mobArena02	1,1,arena_m02,145,55
