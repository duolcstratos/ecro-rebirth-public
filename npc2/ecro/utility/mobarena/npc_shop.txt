prt_in,283,138,4	script	Arena Badge Shop#ma	4_M_REIDIN_KURS,{
	mes "[ ^FF8000Lieutenant Havoc^000000 ]";
	mes "Hello, I'm ^3355FFLieutenant Havoc^000000.";
	mes "Would you like to trade your ^3366FF"+(getitemname(getvariableofnpc(.mobArenaBadgeID, "Alliance Captain#ma")))+"s^000000 for my stock at hand?";
	next;
	if (select("Yes:No") == 2) {
		mes "[ ^FF8000Lieutenant Havoc^000000 ]";
		mes "Just talk to me when you're ready to trade your ^3366FF"+(getitemname(getvariableofnpc(.mobArenaBadgeID, "Alliance Captain#ma")))+"s^000000.";
		close;
	}
	mes "[ ^FF8000Lieutenant Havoc^000000 ]";
	mes "So you want to exchange ^FFA500Monster Arena Badges^000000 for Items.";
	mes " ";
	mes "Here are the purchasable items.";
	openshop("MA_Shop");
	end;
}

-	trader	MA_Shop	FAKE_NPC,{
	OnInit:
	tradertype(NST_CUSTOM);
	sellitem Lightsaber_Cap, 250;
	sellitem Hat_Domino, 625;
	sellitem Hat_Danzo, 875;
	sellitem Ainz_Mask, 875;
	sellitem Tobi_Goggles, 925;
	sellitem Tobi_Mask, 1125;
	sellitem Vampire_Wings, 1125;
	sellitem Full_Hollow_Mask, 1250;
	sellitem Invoker_Cape, 1375;
	sellitem Hokage_Cape, 1375;
	end;

OnCountFunds:
	setcurrency(countitem(3116)); end;

OnPayFunds:
	if (countitem(3116) < @price) end;
	delitem 3116, @price;
	purchaseok(); end;
}
