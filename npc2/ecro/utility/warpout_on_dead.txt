
-	script	Sys_WarpOut_On_Dead	FAKE_NPC,{
	end;

OnInit:
	.deadDuration = 300; // Time in seconds.
	end;

OnPCDieEvent:
	if (strcharinfo(3) == "thana_boss" || strcharinfo(3) == "lhz_dun03"  || strcharinfo(3) == "lhz_dun04")
		addtimer(1000, strnpcinfo(0)+"::OnDeadCheck");
	end;

OnDeadCheck:
	@DEADWARP_TICK++;
	if (Hp > 0) @DEADWARP_TICK = 0;
	else {
		addtimer(1000, strnpcinfo(0)+"::OnDeadCheck");
		if (isnight() && @DEADWARP_TICK >= .deadDuration) {
			message strcharinfo(0), "You will be warped-out from this area.";
			sleep2(500);
			warp "SavePoint", 0, 0;
			@DEADWARP_TICK = 0;
		}
	}
	end;
}
