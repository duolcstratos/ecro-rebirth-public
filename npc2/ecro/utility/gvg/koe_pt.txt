
-	script	Sys_KoEReward	FAKE_NPC,{

OnInit:
	// Mins to stay to get points
	// Default: 10 to disable set the value to 0
	.timetocheck = 0;

	// If player stays more than ".timetocheck" mins, 
	// they get point every x mins.
	// Default: 2, if .timetocheck has been set to 0, this will be disabled.
	.timeexcess = 2;

	// Excess time points if .timetocheck has been set to 0, this will be disabled.
	.excesspoints = 1;

	// If .timetocheck has been disabled, this will be executed.
	.minute_cnt = 2;
	.koe_reward = 1;

	// Seconds idled to stop getting points
	.idle_time = 45;
	end;

OnPCLoadMapEvent:
	if (getmapflag(strcharinfo(PC_MAP), mf_gvg) && strcharinfo(PC_MAP) == "koe") {
		if (@KOE_IDLED) {
			@KOE_IDLED = false;
			doevent(strnpcinfo(0)+"::OnAddTick");
			dispbottom("You have re-activated your KoE Reward system.");
			end;
		}
		if (!@KOE_ENTER)
			doevent(strnpcinfo(0)+"::OnAddTick");
	}
	end;

OnPCDieEvent:
	@KOE_ENTER = false;

OnPCLogoutEvent:
	if (getmapflag(strcharinfo(PC_MAP), mf_gvg) && strcharinfo(PC_MAP) == "koe") {
		sleep2(200);
		deltimer(strnpcinfo(0)+"::OnTick");
	}
	end;

OnTick:
	if (getmapflag(strcharinfo(PC_MAP), mf_gvg) && strcharinfo(PC_MAP) == "koe") {
		if (checkidle() >= .idle_time && .idle_time > 0) {
			@KOE_IDLED = true;
			dispbottom("You've idled for "+callfunc("F_InsertPlural", .idle_time, "second")+" in KoE. You must relog to reset the system.", 0xff0000);
			deltimer(strnpcinfo(0)+"::OnTick");
			end;
		}
		if (KOE_SCHED != $KOE_SCHED) {
			KOE_SCHED = $KOE_SCHED;
			#KOE_POINTS_HANDLER = KoE_Time_Participate = 0;
			@KOE_TICK = 0;
			@KOE_POINT_START = false;
			@KOE_ENTER = false;
		}
		if (@KOE_ENTER)
			@KOE_ENTER = true;
		if (!@KOE_POINT_START) {
			if (!KoE_Time_Participate) {
				if (.timetocheck == 0)
					dispbottom "You'll earn "+F_InsertPlural((.koe_reward * ($BONUSPT_KOE ? 2:1)), "KoE Point")+" for every "+F_InsertPlural(.minute_cnt, "minute")+" without idling.";
				else
					dispbottom("You'll earn "+(.timetocheck/2 * ($BONUSPT_KOE ? 2:1))+" KoE Points if you'll participate for at least "+.timetocheck+" minutes without idling.");
			}
			@KOE_POINT_START = true;
		}
		if (@KOE_TICK < 59) {
			@KOE_TICK++;
			doevent(strnpcinfo(0)+"::OnAddTick");
		} else {
			@KOE_TICK = 0;
			doevent(strnpcinfo(0)+"::OnTallyKOETime");
		}
	} else
		doevent(strnpcinfo(0)+"::OnNotKOEMap");
	end;

OnTallyKOETime:
	if (getmapflag(strcharinfo(PC_MAP), mf_gvg) && strcharinfo(PC_MAP) == "koe") {
		.@excessPt = .excesspoints * ($BONUSPT_KOE ? 2:1);
		KoE_Time_Participate++;
		if (.timetocheck == 0) {
			dispbottom "You've participated "+F_InsertPlural(KoE_Time_Participate, "minute")+" in King of Emperium.";
			if (KoE_Time_Participate % .minute_cnt == 0 && KoE_Time_Participate != 0) {
				#KOE_POINTS_HANDLER += .koe_reward * ($BONUSPT_KOE ? 2 : 1);
				dispbottom "You've earned "+F_InsertPlural(.koe_reward, "KoE Point")+". Current KoE Points to claim: "+#KOE_POINTS_HANDLER;
			}
		} else {
			if (KoE_Time_Participate < .timetocheck)
				dispbottom("You've participated for "+callfunc("F_InsertPlural", KoE_Time_Participate, "minute")+" in KoE.");
			else if (KoE_Time_Participate == .timetocheck) {
				#KOE_POINTS_HANDLER += (.timetocheck/2) * ($BONUSPT_KOE ? 2:1);
				dispbottom("You've participated for "+callfunc("F_InsertPlural", KoE_Time_Participate, "minute")+" in KoE and earned "+#KOE_POINTS_HANDLER+" KoE Points to be collected.");
				dispbottom("In addition, you will earn "+.@excessPt+" KoE Points for every "+.timeexcess+" minutes.");
			} else if (KoE_Time_Participate > .timetocheck) {
				@ExcessTime++;
				dispbottom("You've participated for "+callfunc("F_InsertPlural", KoE_Time_Participate, "minute")+" in KoE.");
				if (@ExcessTime == .timeexcess) {
					@ExcessTime = 0;
					#KOE_POINTS_HANDLER += .@excessPt;
					dispbottom("You've earned "+callfunc("F_InsertPlural", .@excessPt, "KoE Point")+". Current KoE Points to claim: "+#KOE_POINTS_HANDLER);
				}
			}
		}
		doevent(strnpcinfo(0)+"::OnAddTick");
	}
	end;

OnNotKOEMap:
	@KOE_ENTER = false;
	@KOE_POINT_START = false;
	deltimer(strnpcinfo(0)+"::OnTick");
	end;

OnAddTick:
	addtimer(1000, strnpcinfo(0)+"::OnTick");
	end;
}
