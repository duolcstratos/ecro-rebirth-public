//==== DarkRO Scripts ====================================
//= Player Commands
//===== By: ================================================== 
//= [GM] Mirage
//===== Current Version: ===================================== 
//= 1.0
//===== Description: ========================================= 
//= @item with logs
//===== Changelog: =========================================== 
//= 1.0 Initial Implementation
//============================================================
-	script	addcash	-1,{

OnInit:
bindatcmd("addcash",strnpcinfo(3)+"::OnAtcmd_addcash",90,99);
end;
 
OnAtcmd_addcash:

if(getgmlevel() < 90){ end; }


set @npc$,"^00B2EE[Cash Generator]^000000";

mes @npc$;
mes "Hi "+strcharinfo(0)+" What do you want to do?";

switch(select("^3CB371-^000000 Cash System:^3CB371-^000000 Server Statistics")){

case 1:
next;
mes @npc$;
mes "What do you want to manage in the cash system?";
switch(select("^3CB371-^000000 Add Cash:^ff0000-^000000 Remove Cash:^FFA500-^000000 View Cash")){

case 1:
	next;
	mes @npc$;
	mes "Enter the login for the account you want to add CashPoints to.";
	input .@cash$;

		query_sql "SELECT `account_id` FROM `login` WHERE `userid`='"+.@cash$+"'",.@cont;
		if(.@cont == 0)
		{
		next;
		mes @npc$;
		mes "^ff0000Sorry, you entered your account login incorrectly.^000000";
		close;
	}
	
	next;
	mes @npc$;
	mes "Enter the amount of Cash's you wish to add to the account "+.@cash$+".";
	input .@valor;
			if(.@valor == 0)
			{
			next;
			mes @npc$;
			mes "^ff0000Sorry, you entered an invalid amount.^000000";
			close;
			}
	next;
	mes @npc$;
	mes ""+strcharinfo(0)+", are you sure you want to add ^00BFFF"+.@valor+"^000000 Cash in the account ^00BFFF"+.@cash$+"^000000?";
	if (select("^3CB371[»]^000000 Yes:^ff0000[»]^000000 No") == 2) close;
	
	next;
		mes @npc$;
		mes "Have been added ^3CB371+"+.@valor+"^000000 in the user's account ^3CB371"+.@cash$+"^000000 successfully.";
		query_sql "UPDATE `login` SET `cash` = `cash` +"+.@valor+" WHERE `userid`='"+.@cash$+"'";
		query_sql "INSERT INTO `addcash_log` (`account_id`, `char_id`,  `char_name`, `ação`, `cash`, `login`, `data`) VALUES ('"+getcharid(3)+"','"+getcharid(0)+"','"+strcharinfo(0)+"', '+', '"+.@valor+"', '"+.@cash$+"', '"+gettimestr("%d/%m/%Y - %H:%M:%S", 25)+"')";
		close;

case 2:
	next;
	mes @npc$;
	mes "Enter the login for the account you wish to remove Cash's from.";
	input .@cash$;

		query_sql "SELECT `account_id` FROM `login` WHERE `userid`='"+.@cash$+"'",.@cont;
		if(.@cont == 0)
		{
		next;
		mes @npc$;
		mes "^ff0000Sorry, you entered your account login incorrectly.^000000";
		close;
	}
	
	next;
	mes @npc$;
	mes "Enter the amount of Cash's you wish to remove from the account "+.@cash$+".";
	input .@valor;
			if(.@valor == 0)
			{
			next;
			mes @npc$;
			mes "^ff0000Sorry, you entered an invalid amount.^000000";
			close;
			}
	next;
	mes @npc$;
	mes "Are you sure you want to remove ^00BFFF"+.@valor+"^000000 Account cash ^00BFFF"+.@cash$+"^000000?";
	if (select("^3CB371[»]^000000 Yes:^ff0000[»]^000000 No") == 2) close;
	
		next;
		mes @npc$;
		mes "Have been removed ^3CB371-"+.@valor+"^000000 user account ^3CB371"+.@cash$+"^000000 successfully.";
		query_sql "SELECT `account_id` FROM `login` WHERE `userid`='"+.@cash$+"'",.@id;
		query_sql "UPDATE `acc_reg_num` SET `value` = `value`-"+.@valor+" WHERE `account_id`='"+.@id+"'";
		query_sql "INSERT INTO `addcash_log` (`account_id`, `char_id`,  `char_name`, `ação`, `cash`, `login`, `data`) VALUES ('"+getcharid(3)+"','"+getcharid(0)+"','"+strcharinfo(0)+"', '-', '"+.@valor+"', '"+.@cash$+"', '"+gettimestr("%d/%m/%Y - %H:%M:%S", 25)+"')";
		close;

case 3:
	next;
	mes @npc$;
	mes "Enter the login of the account you want to see the amount of Cash's.";
	input .@cash$;

		query_sql "SELECT `account_id` FROM `login` WHERE `userid`='"+.@cash$+"'",.@cont;
		if(.@cont == 0)
		{
		next;
		mes @npc$;
		mes "^ff0000Sorry, you entered your account login incorrectly.^000000";
		close;
	}
	
		next;
		query_sql "SELECT `account_id` FROM `login` WHERE `userid`='"+.@cash$+"'",.@id;
		query_sql "SELECT `value` FROM `acc_reg_num` WHERE `account_id`='"+.@id+"'",.@saldo;
		mes @npc$;
		mes "The bill "+@cash$+" It has ^00BFFF"+.@saldo+"^000000 Cash's. ";
		close;
	}

	case 2:
next;
		query_sql "SELECT count(*) FROM `login` WHERE `account_id` > 1",.@count_acc;
		query_sql "SELECT count(*) FROM `char`",.@count_char;
		query_sql "SELECT `zeny` FROM `char` WHERE `zeny` > 0",.@count_zeny;
	for( set .@z,0; .@z < getarraysize(.@count_zeny); set .@z,.@z + 1) 
		set .@total_zeny,.@total_zeny + .@count_zeny[.@z];
		mes @npc$;
		mes "Total Accounts: " + .@count_acc;
			mes "Total Characters: " + .@count_char;
		mes "Total Zeny: " + .@total_zeny;
		close;
	}
}

-	script	Sistema Cash#02	-1,{

OnPCLoginEvent:
query_sql "SELECT `cash` FROM `login` WHERE `account_id` = '"+getcharid(3)+"'", .@cash;
if(.@cash == 0){ end; }
dispbottom "[Cash Generator] Donation completed successfully! Credits Received: "+.@cash+"";
set #CASHPOINTS,#CASHPOINTS+.@cash;
query_sql "UPDATE `login` SET `cash` = `cash` =0 WHERE `account_id`='"+getcharid(3)+"'";
end;
}


/*
ALTER TABLE `login` ADD `cash` INT( 11 ) NOT NULL DEFAULT '0';
*/

/*
CREATE TABLE `addcash_log` (
 `account_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
 `char_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
 `char_name`varchar(32) DEFAULT '0',  
 `ação` varchar(32) DEFAULT '0',
 `cash` int(11) UNSIGNED NOT NULL DEFAULT '0',
 `login` varchar(32) DEFAULT '0',
 `data` varchar(32) DEFAULT '0'
) ENGINE=MyISAM;
*/