
-	script	AtHold	FAKE_NPC,{

OnInit:
	bindatcmd "hold",strnpcinfo(3)+"::OnHold";
	end;

OnHold:
	if (!@Hold) {
		set @Hold, 1;
		message strcharinfo(0), "Hold is now on, You cannot move from your position.";
		setpcblock(PCBLOCK_MOVE, true);
	} else {
		set @Hold, 0;
		message strcharinfo(0), "Hold is now off.";
		setpcblock(PCBLOCK_MOVE, false);
	}
}
