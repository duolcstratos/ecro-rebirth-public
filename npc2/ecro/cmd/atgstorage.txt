// ALTER TABLE  `guild` ADD  `storage_pass` VARCHAR( 24 ) NULL AFTER  `skill_point`

-	script	AtGStorage	-1,{

OnInit:
	bindatcmd "gstorage",strnpcinfo(3)+"::OnGStorage";
	end;

OnGStorage:
	if (strcharinfo(3) == "sec_pri") end;

	if (!getcharid(2)) {
		message strcharinfo(0),"You are not in a guild.";
		close;
	}
	.@guild_id = getcharid(2);
	if (.@guild_id) {
		if (getcharid(0) == getguildmasterid(.@guild_id)) {
			if (select("Open Guild Storage","Set Guild Password") == 2) {
				mes "Enter your new Guild Storage Password.";
				mes "^777777(4~20 characters)^000000";
				mes " ";
				mes "^0055FFNONE^000000 = Disable";
				while(input(.@new_pass$,4,20));
				if (.@new_pass$ == "NONE") .@new_pass$ = "";
				mes "Guild Storage Password updated.";
				query_sql("UPDATE `guild` SET `storage_pass` = '"+escape_sql(.@new_pass$)+"' WHERE `guild_id` = "+.@guild_id);
				next;
			}
		}
		query_sql("SELECT `storage_pass` FROM `guild` WHERE `guild_id` = "+.@guild_id,.@storage_pass$);
			if (.@storage_pass$ != ""){
				message strcharinfo(0),"Please enter your Guild Storage Password:";
				input .@pass$;
					if (.@storage_pass$ != .@pass$) {
						message strcharinfo(0),"That's an Invalid Password!";
						close;
					}
				}
			if (guildopenstorage()) {
				message strcharinfo(0),"I'm sorry but another Guild Member is using the Guild Storage.";
				close;
			}
			message strcharinfo(0),"Guild Storage Opened.";
			close;
			guildopenstorage();
		}
	end;
}
