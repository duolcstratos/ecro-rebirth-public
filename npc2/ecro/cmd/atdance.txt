-	script	Atcmd_Dance	-1,{

OnInit:
	bindatcmd "dance", strnpcinfo(0)+"::OnAtcommand",60,98;
	end;

OnAtcommand:
	if (.@atcmd_numparameters < 1 || !(.@n = atoi(.@atcmd_parameters$))) {
		message(strcharinfo(0),"Usage: @dance <1-9>");
		message(strcharinfo(0),"Usage: @dance failed.");
		end;
	}
	switch(.@n) {
		case 1:
			specialeffect2(EF_PRESSEDBODY); break;
		case 2:
			specialeffect2(EF_SPINEDBODY); break;
		case 3:
			specialeffect2(EF_KICKEDBODY); break;
		case 4:
			specialeffect2(EF_QUAKEBODY); break;
		case 5:
			specialeffect2(EF_QUAKEBODY2); break;
		case 6:
			specialeffect2(EF_SPINEDBODY2); break;
		case 7:
			specialeffect2(EF_CASTSPIN); break;
		case 8:
			specialeffect2(EF_BABYBODY_BACK); break;
		case 9:
			specialeffect2(EF_QUAKEBODY3); break;
		default:
			message(strcharinfo(0),"Usage: @dance <1-9>");
			message(strcharinfo(0),"Usage: @dance failed.");
			break;
	}
	end;
}