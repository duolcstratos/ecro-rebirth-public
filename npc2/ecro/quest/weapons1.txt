//==== DarkRO Scripts ====================================
//= Icarus Quests
//===== By: ================================================== 
//= [GM] Brenth
//===== Current Version: ===================================== 
//= 1.1
//===== Description: ========================================= 
//= Quest NPC for Icarus Weapons
//===== Changelog: =========================================== 
//= Not yet tested for bugs
//= 1.0 Implementation
//= 1.1 Fixed wrong variable
//============================================================

aldebaran,221,135,4	script	Icarus Doppelganger#WP1	4_M_SAGE_A,{

	set @header$,"[^FF8000 Icarus Doppelganger ^000000]";	
	if(#WING3_Q == 2){
		mes @header$;
		mes "So? Yeah, You are right.. The real me..... Correct!";
		next;
		mes @header$;
		mes "You already possessed the power of elements, Only the real Icarus can create something like that....!";
		next;
		mes @header$;
		mes "Well I have my specialties too, I can make you one of Icarus weapon, But this offer is very limited you can only do this one time! So you have to choose wisely...";
		next;
		switch(select("Icarus Sword:Icarus Bow:Icarus Staff:Give me more time!")){
		case 1:
			mes @header$;
			mes "Uhmm.... for ^22AA22Icarus Sword^000000? you have to bring me this:";
			mes " ";
			mes "- 1 ^FF0000Ruber^000000,";
			mes "- 1 ^FF0000Katzbalger^000000,";
			mes "- 1 ^FF0000Vecer Axe^000000";
			mes "and also ^FF000030,000,000 Zeny^000000.";
			next;
			mes @header$;
			mes "Where is the items? Do you already have it?";
			next;
			switch(select("Yep!:Nope.")){
			case 1:
				if((countitem(13421) < 1) || (countitem(1170) < 1) || (countitem(1311) < 1) || (Zeny < 30000000)){
					mes @header$;
					mes "There is something wrong.. Are you sure you have the list I gave you?";
					close;
				}
				F_CheckInventoryCount;
				next;
				mes @header$;
				mes "Congratulation!";
				mes " ";
				mes "I know you can do it... See you in afterlife...";
				delitem 13421,1;
				delitem 1170,1;
				delitem 1311,1;
				Zeny -= 30000000;
				getitem 3500,1; // Icarus Sword
				announce strcharinfo(0) +" has created Icarus Sword!",0,0xFF6868;
				close;
			case 2: 
				mes @header$;
				mes "Okay!";
				close;
			}
		case 2:
			mes @header$;
			mes "Uhmm.... for ^22AA22Icarus Bow^000000? you have to bring me this:";
			mes " ";
			mes "- 1 ^FF0000Falken Blitz^000000,";
			mes "- 1 ^FF0000Krishna^000000,";
			mes "- 1 ^FF0000Cowardice Blade^000000";
			mes "and also ^FF000030,000,000 Zeny^000000.";
			next;
			mes @header$;
			mes "Where is the items? Do you already have it?";
			next;
			switch(select("Yep!:Nope.")){
			case 1:
				if((countitem(1745) < 1) || (countitem(1284) < 1) || (countitem(13004) < 1) || (Zeny < 30000000)){
					mes @header$;
					mes "There is something wrong.. Are you sure you have the list I gave you?";
					close;
				}
				F_CheckInventoryCount;
				next;
				mes @header$;
				mes "Congratulation!";
				mes " ";
				mes "I know you can do it... See you in afterlife...";
				delitem 1745,1;
				delitem 1284,1;
				delitem 13004,1;
				Zeny -= 30000000;
				getitem 3501,1; // Icarus Bow
				announce strcharinfo(0) +" has created Icarus Bow!",0,0xFF6868;
				close;
			case 2: 
				mes @header$;
				mes "Okay!";
				close;
			}
		case 3:
			mes @header$;
			mes "Uhmm.... for ^22AA22Icarus Staff^000000? you have to bring me this:";
			mes " ";
			mes "- 1 ^FF0000Croce Staff^000000,";
			mes "- 1 ^FF0000Lacryma Stick^000000,";
			mes "- 1 ^FF0000Wing Staff^000000,";
			mes "- 1 ^FF0000Spike^000000";
			mes "and also ^FF000030,000,000 Zeny^000000.";
			next;
			mes @header$;
			mes "Where is the items? Do you already have it?";
			next;
			switch(select("Yep!:Nope.")){
			case 1:
				if((countitem(1647) < 1) || (countitem(1646) < 1) || (countitem(1616) < 1) || (countitem(1523) < 1) || (Zeny < 30000000)){
					mes @header$;
					mes "There is something wrong.. Are you sure you have the list I gave you?";
					close;
				}
				F_CheckInventoryCount;
				next;
				mes @header$;
				mes "Congratulation!";
				mes " ";
				mes "I know you can do it... See you in afterlife...";
				delitem 1647,1;
				delitem 1646,1;
				delitem 1616,1;
				delitem 1523,1;
				Zeny -= 30000000;
				getitem 3502,1; // Icarus Staff
				announce strcharinfo(0) +" has created Icarus Staff!",0,0xFF6868;
				close;
			case 2: 
				mes @header$;
				mes "Okay!";
				close;
			}
		case 4: 
			mes @header$;
			mes "Sure!";
			close;
		}
	}

	mes @header$;
	mes "Uhmmmm? I think.... I misplaced myself...";
	close;
}