//===== rAthena Script =======================================
//= Paid Script
//===== Compatible With: =====================================
//= rAthena Project
//===== Script: =============-================================
//= Blueprint System
//============================================================

//F_BluePrint(<BluePrint ID>,<Result Item ID>,<Zeny>,<Require item id1>,<require item count1>,<Require item id1>,<require item count1>);
/*
	The <Result Item ID> is required!
	If the <BluePrint ID> is 0 , it wont consome the blueprint
	if the Zeny is 0 , it wonr require zeny
	if the item requirment id is 0 , it wont consume items
*/
function	script	F_BluePrint	{
	.@br = getarg(0);
	.@reward = getarg(1);
	if(getarg(2)){
		if(Zeny < getarg(2)){
			dispbottom "[Blueprint System] You need [ " + (getarg(2) - Zeny) + " ] Zeny.";
			.@close = true;
		}
	}
	for(.@i=3;.@i<getargcount();.@i+=2){
		if(getarg(.@i) && countitem(getarg(.@i)) < getarg(.@i+1)){
			//dispbottom "[Blueprint System] You don't have (" + getarg(.@i+1) + ") " + getitemname(getarg(.@i));
			dispbottom "[Blueprint System] You need [ " + (getarg(.@i+1) - countitem(getarg(.@i))) + " ] " + getitemname(getarg(.@i)) + ".";
			.@close = true;
		}
	}
	if(getarg(0)){
		if(!countitem(getarg(0))){
			dispbottom "[Blueprint System] You don't have (1) " + getitemname(getarg(0));
			.@close = true;
		}
	}
	if(.@close) end;
	if(getarg(2)) Zeny -= getarg(2);
	for(.@i=3;.@i<getargcount();.@i+=2){
		if(getarg(.@i)){
			delitem(getarg(.@i),getarg(.@i+1));
		}
	}
	if(getarg(0))delitem(getarg(0),1);
	getitem(getarg(1),1);
	message strcharinfo(0),"[Blueprint System] Congratulations! You have made a " + getitemname(getarg(1));
	specialeffect2 100;
	return;
}

//Example:
//100pcs jellopy 909 , 100 apple 512 for I love China 5423

/*
Item DB
  - Id: 45000
    AegisName: ILC_BluePrint
    Name: I Love China BluePrint
    Type: Delayconsume
    Script: |
      callfunc("F_BluePrint",45000,5423,0,909,100,512,100);
*/
/*
Item Info
	[45000] = {	
		unidentifiedDisplayName = "ILC_BluePrint",
		unidentifiedResourceName = "젤로피",
		unidentifiedDescriptionName = { "" },
		identifiedDisplayName = "I Love China BluePrint",
		identifiedResourceName = "젤로피",
		identifiedDescriptionName = {
			"I Love China BluePrint",
			"combain it with 100 Apple and",
			"100 jellopy to get I Love China"
		},
		slotCount = 0,
		ClassNum = 0,
		costume = false
	},
*/

// @cdroprate
-	script	CustomDrop	-1,{
	OnNPCKillEvent:
		for (.@i = 0; .@i < .size; .@i += 3) {
			if (killedrid == .Cards[.@i]) {
				if (rand(100) < .Cards[.@i+2])
					getitem .Cards[.@i+1], 1;
			}
		}
		end;
	OnCheckDrop:
		//dispbottom "================================================";
		dispbottom "Blueprint System Custom Drops";
		for (.@i = 0; .@i < .size; .@i += 3) {
			//dispbottom "================================================";
			dispbottom "------------------------------------------------";
			dispbottom "Monster Name 	: " + getmonsterinfo(.Cards[.@i], MOB_NAME);
			dispbottom "Item			: " + getitemname(.Cards[.@i+1]);
			dispbottom "Rate			: " + .Cards[.@i+2] + "%";
		}
		//dispbottom "================================================";
		end;
	OnInit:
		bindatcmd "cdroprate", strnpcinfo(1) + "::OnCheckDrop";
		setarray .Cards[0], // <mob id>, <card>, <rate> 	
			1002, 38001, 5,
			1004, 38002, 5;
		.size = getarraysize(.Cards);
		end;
}

// Custom Mob Drop