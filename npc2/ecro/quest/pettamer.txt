//==== DarkRO Scripts ====================================
//= Universal Pet Tamer Seller
//===== By: ================================================== 
//= [GM] Brenth
//===== Current Version: ===================================== 
//= 1.0
//===== Description: ========================================= 
//= NPC that sell Taming Items for Beast Pet
//===== Changelog: =========================================== 
//= Not yet tested for bugs
//= 1.0 Implementation
//============================================================

prontera,183,193,1	script	Pet Hunter	4_M_JOB_HUNTER,{

	set @header$,"[^FF8000 Pet Hunter ^000000]";
	if ((countitem(643) >= 1) && (countitem(537) >= 1)) {
		mes @header$;
		mes "I am selling a very rare stone that can tame large beasts...";
		mes " ";
		mes "Do you want some?!";
		next;
		switch(select("Sell me some!:Not interested.")){
		case 1:
			callshop "Tame_Shop",1; 
			end;
		case 2: 
			mes @header$;
			mes "Okay, no problem..";
			close;
		}
	}
	mes @header$;
	mes "I don't see you are capable of having a PET, so get out of my sight!";
	close;
}

-	shop	Tame_Shop	-1,3106:5000000