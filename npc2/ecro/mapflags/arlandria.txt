//==== DarkRO Scripts ========================================//
//= Arlandria
//===== By: ==================================================//
//= [GM] Mirage
//===== Current Version: =====================================//
//= 1.0
//===== Description: =========================================//
//= Arlandria NPCs
//===== Changelog: ===========================================//
//= Not yet tested for bugs
//= 1.0 Implementation
//============================================================//
arlandria,159,191,4	script	Kafra Employee::arlandria	4_F_KAFRA6,{
	cutin "kafra_06",2;
	callfunc "F_KafSet";
	mes "[Kafra Employee]";
	mes "Welcome to the";
	mes "Kafra Corporation~";
	mes "The Kafra Services are";
	mes "always here to support";
	mes "you. So how can I be";
	mes "of service today?";
	callfunc "F_Kafra",5,1,0,40,800;
	savepoint "umbala",137,149;
	callfunc "F_KafEnd",0,1,"in this place.";
}

arlandria,163,191,4	duplicate(Warper)	Warper#bertan	4_F_AGENTKAFRA
arlandria,150,186,5	duplicate(Healer)	Healer#bertan	4_NECORING

arlandria	mapflag	nowarp
arlandria	mapflag	nowarpto
arlandria	mapflag	nobranch
arlandria	mapflag	nomemo
arlandria	mapflag	nosave	SavePoint
arlandria	mapflag	noteleport
//============================================================//