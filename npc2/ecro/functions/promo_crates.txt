//==== ecRO Scripts ==========================================
//= Donation Promo Boxes
//===== Current Version: ===================================== 
//= 1.0
//===== Description: ========================================= 
//= Function for Donation Boxes
//===== Changelog: =========================================== 
//= Not yet tested for bugs
//= 1.0 Implementation
//============================================================

//================= GM Mikazuki Edits ==============================	
function	script	20_Donation_Box	{
set @header$,"[^FF8000 20USD Donation Promo ^000000]";
	mes @header$;
	mes "Please choose your Promotional Item (1).";
	mes " ";
	switch(select("Aura Box (L):Aura Box (M):Designer's Collection Box:Visor Scroll:Gacha Tickets (10)")){
			case 1:	if (!checkweight(32002,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 32002,1; delitem getarg(0),getarg(1); goto Promo_End; //Aura Box (L)
			case 2:	if (!checkweight(32003,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close;  end;} 
							getitem 32003,1; delitem getarg(0),getarg(1); goto Promo_End; //Aura Box (M)
			case 3:	if (!checkweight(3164,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 3164,1; delitem getarg(0),getarg(1); goto Promo_End; //Designers Collection Box
			case 4:	if (!checkweight(3174,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 3174,1; delitem getarg(0),getarg(1); goto Promo_End; //Visor Scroll
			case 5:	if (!checkweight(30565,10)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 30565,10; delitem getarg(0),getarg(1); goto Promo_End; //Gacha Tickets (10)
	}
Promo_End:
	next;
	mes @header$;
	mes "Thank you for considering donating to "+$server_name$+", ^0000FF"+ strcharinfo(0) +"^000000.";
	mes "Good luck and have fun playing!";
	dispbottom "[Donation Promo] You have received your Donation Promo.",0x00ff00;
	close;
	end;
}

//============================================================
function	script	50_Donation_Box	{
	set @header$,"[^FF8000 50USD Donation Promo ^000000]";
	mes @header$;
	mes "Please choose your Promotional Item (1).";
	mes " ";
	switch(select("Fallen Angel Wings:Fallen Angel Halo [1]:Valkyrie Shoes [1]:Valkyrie Shield [1]:Gacha Tickets (20)")){
	
			case 1:	if (!checkweight(32101,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 32101,1; delitem getarg(0),getarg(1); goto Promo_End; //Fallen Angel Wings
			case 2:	if (!checkweight(32102,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close;  end;} 
							getitem 32102,1; delitem getarg(0),getarg(1); goto Promo_End; //Fallen Angel Halo
			case 3:	if (!checkweight(2421,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 2421,1; delitem getarg(0),getarg(1); goto Promo_End; //Valkyrie Shoes
			case 4:	if (!checkweight(2115,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 2115,1; delitem getarg(0),getarg(1); goto Promo_End; //Valkyrie Shield
			case 5:	if (!checkweight(30565,20)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 30565,20; delitem getarg(0),getarg(1); goto Promo_End; //Gacha Tickets (20)
	}
Promo_End:
	next;
	mes @header$;
	mes "Thank you for considering donating to "+$server_name$+", ^0000FF"+ strcharinfo(0) +"^000000.";
	mes "Good luck and have fun playing!";
	dispbottom "[Donation Promo] You have received your Donation Promo.",0x00ff00;
	close;
	end;
	}
//============================================================
function	script	75_Donation_Box	{
	set @header$,"[^FF8000 75USD Donation Promo ^000000]";
	mes @header$;
	mes "Please choose your Promotional Item (1).";
	switch(select("+10 Icarus Weapon (1):+10 Super Novice Zeta Weapon (1):Valkyrie Armor [1]:Valkyrie Manteau [1]:Gacha Tickets (30)")){
		case 1:	goto Icarus_Promo; // +10 Icarus Weapons
		case 2:	goto SN_Wep_Promo; // +10 Super Novice Weapons
			case 3:	if (!checkweight(2357,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 2357,1; delitem getarg(0),getarg(1); goto Promo_End; //Valkyrie Shoes
			case 4:	if (!checkweight(2524,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 2524,1; delitem getarg(0),getarg(1); goto Promo_End; //Valkyrie Shield
			case 5:	if (!checkweight(30565,30)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 30565,30; delitem getarg(0),getarg(1); goto Promo_End; //Gacha Tickets (30)
	}
Icarus_Promo:
	next;
	mes @header$;
	mes "Please choose your Icarus Weapon (1).";
	switch(select("Icarus Revolver:Icarus Mace:Icarus Fist:Icarus Dagger:Icarus Book:Icarus Staff:Icarus Bow:Icarus Sword:Icarus Whip:Icarus Violin:Icarus Spear:Icarus Shuriken:Icarus Axe:Icarus Rod of Survival:Icarus Katar:Icarus Claymore:Icarus Lance:Icarus Lightsaber (Red):Icarus Lightsaber (Green)")) {
		case 1: if (!checkweight(32120,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32120,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 1 Icarus Revolver
		case 2: if (!checkweight(32043,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32043,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 2 Icarus Mace
		case 3: if (!checkweight(32042,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32042,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 3 Icarus Fist
		case 4: if (!checkweight(32041,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32041,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 4 Icarus Dagger
		case 5: if (!checkweight(32040,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32040,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 5 Icarus Book
		case 6: if (!checkweight(3502,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 3502,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 6 Icarus Staff
		case 7: if (!checkweight(3501,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 3501,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 7 Icarus Bow
		case 8: if (!checkweight(3500,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 3500,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 8 Icarus Sword
		case 9: if (!checkweight(32035,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32035,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 9 Icarus Whip
		case 10: if (!checkweight(32036,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32036,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 10 Icarus Violin
		case 11: if (!checkweight(32037,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32037,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 11 Icarus Spear
		case 12: if (!checkweight(32038,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32038,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 12 Icarus Shuriken
		case 13: if (!checkweight(32039,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32039,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 13 Icarus Axe
		case 14: if (!checkweight(32044,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32044,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 14 Icarus Rod of Survival
		case 15: if (!checkweight(32045,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32045,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 15 Icarus Katar
		case 16: if (!checkweight(32046,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32046,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 16 Icarus Claymore
		case 17: if (!checkweight(32047,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32047,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 17 Icarus Lance
		case 18: if (!checkweight(3515,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 3515,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 18 Icarus Lightsaber (Red)
		case 19: if (!checkweight(3516,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 3516,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;				// 19 Icarus Lightsaber (Green)
	}
SN_Wep_Promo:
	next;
	mes @header$;
	mes "Please choose your Super Novice Zeta Weapon (1).";
	switch(select("Zeta Empress Mace:Zeta Empress Casting Wand:Zeta Empress Rapier")) {
			case 1: if (!checkweight(32555,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32555,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Mace
			case 2: if (!checkweight(32556,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32556,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Casting Wand
			case 3: if (!checkweight(32557,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32557,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Rapier
	}
Promo_End:
	next;
	mes @header$;
	mes "Thank you for considering donating to "+$server_name$+", ^0000FF"+ strcharinfo(0) +"^000000.";
	mes "Good luck and have fun playing!";
	dispbottom "[Donation Promo] You have received your Donation Promo.",0x00ff00;
	close;
	end;
	}
//============================================================
function	script	100_Donation_Box	{
	set @header$,"[^FF8000 100USD Donation Promo ^000000]";
	mes @header$;
	mes "Please choose your Promotional Item (1).";
	switch(select("+7 Refine Ticket (1):Empress Gear (1):+10 Banryu (1):Gacha Tickets (50)")){
		case 1:	goto Refine_Promo; // Refine Tickets
		
		case 2:	goto Empress_Promo;
		
		case 3:	if (!checkweight(3505,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
					getitem2 3505,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Rapier
		
		case 4:	if (!checkweight(30565,50)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
							getitem 30565,50; delitem getarg(0),getarg(1); goto Promo_End; //Gacha Tickets (50)
	}
Refine_Promo:
	next;
	mes @header$;
	mes "Please choose your +7 Refine Ticket.";
	switch(select("+7 Refine Ticket (Armor):+7 Refine Ticket (Garment):+7 Refine Ticket (Shoes):+7 Refine Ticket (Upper):+7 Refine Ticket (Left Hand):+7 Refine Ticket (Right Hand)")){
		case 1:	if (!checkweight(32105,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32105,1; delitem getarg(0),getarg(1); goto Promo_End; // Armor
		case 2:	if (!checkweight(32106,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32106,1; delitem getarg(0),getarg(1); goto Promo_End; // Garment
		case 3:	if (!checkweight(32107,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32107,1; delitem getarg(0),getarg(1); goto Promo_End; // Shoes
		case 4:	if (!checkweight(32108,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32108,1; delitem getarg(0),getarg(1); goto Promo_End; // Upper Headgear
		case 5:	if (!checkweight(32109,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32109,1; delitem getarg(0),getarg(1); goto Promo_End; // Left Hand
		case 6:	if (!checkweight(32110,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32110,1; delitem getarg(0),getarg(1); goto Promo_End; // Right Hand
	}
Empress_Promo:
	next;
	mes @header$;
	mes "Please choose your Empress Gear (1).";
	switch(select("Zeta Empress Armor:Zeta Empress Manteau:Zeta Empress Boots:Zeta Empress Shield")) {
			case 1: if (!checkweight(32551,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32551,1; delitem getarg(0),getarg(1); goto Promo_End; //Empress Armor
			case 2: if (!checkweight(32552,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32552,1; delitem getarg(0),getarg(1); goto Promo_End; //Empress Manteau
			case 3: if (!checkweight(32553,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32553,1; delitem getarg(0),getarg(1); goto Promo_End; //Empress Boots
			case 4: if (!checkweight(32554,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32554,1; delitem getarg(0),getarg(1); goto Promo_End; //Empress Shield
	}
Promo_End:
	next;
	mes @header$;
	mes "Thank you for considering donating to "+$server_name$+", ^0000FF"+ strcharinfo(0) +"^000000.";
	mes "Good luck and have fun playing!";
	dispbottom "[Donation Promo] You have received your Donation Promo.",0x00ff00;
	close;
	}
//============================================================
function	script	150_Donation_Box	{
	set @header$,"[^FF8000 150USD Donation Promo ^000000]";
	mes @header$;
	mes "Please choose your (1) Promotional Item.";
	switch(select("+10 Zeta Empress Weapon (1):Empress Gear (1):Gacha Tickets (75)")){
	case 1:	goto Zeta_Empress_Weapons;
	case 2:	goto Empress_Promo;
	case 3:	if (!checkweight(30565,75)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
				getitem 30565,75; delitem getarg(0),getarg(1); goto Promo_End; //Gacha Tickets (75)
	}
Zeta_Empress_Weapons:
	next;
	mes @header$;
	mes "Please choose your +10 Zeta Empress Weapon (1).";
	switch(select("+10 Zeta Empress Dagger:+10 Zeta Empress Spear:+10 Zeta Empress Lance:+10 Zeta Empress Scepter:+10 Zeta Empress Elemental Staff:+10 Zeta Empress Judgment Staff:+10 Zeta Empress Sword:+10 Zeta Empress Excalibur:+10 Zeta Empress Axe:+10 Zeta Empress Ballista:+10 Zeta Empress Bow:+10 Zeta Empress Fist:+10 Zeta Empress Violin:+10 Zeta Empress Whip:+10 Zeta Empress Book of Elements:+10 Zeta Empress Bible:+10 Zeta Empress Shuriken:+10 Zeta Empress Katar:+10 Zeta Empress Pistol:+10 Zeta Empress Rifle:+10 Zeta Empress Shotgun:+10 Zeta Empress Grenade Launcher:+10 Zeta Empress Gatling Cannon:+10 Zeta Empress Training Manual:+10 Zeta Empress Book of Shadow:+10 Zeta Empress Book of the Moon")) {
		
		case 1: if (!checkweight(32528,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32528,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Dagger
		case 2: if (!checkweight(32529,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32529,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Spear
		case 3: if (!checkweight(32530,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32530,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Lance
		case 4: if (!checkweight(32531,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32531,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Scepter
		case 5: if (!checkweight(32532,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32532,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Elemental Staff
		case 6: if (!checkweight(32533,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32533,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Judgment Staff
		case 7: if (!checkweight(32534,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32534,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Sword
		case 8: if (!checkweight(32535,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32535,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Excalibur
		case 9: if (!checkweight(32536,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32536,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Axe
		case 10: if (!checkweight(32537,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32537,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Ballista
		case 11: if (!checkweight(32538,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32538,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Bow
		case 12: if (!checkweight(32539,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32539,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Fist
		case 13: if (!checkweight(32540,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32540,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Violin
		case 14: if (!checkweight(32541,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32541,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Whip
		case 15: if (!checkweight(32542,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32542,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Book of Elements
		case 16: if (!checkweight(32543,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32543,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Bible
		case 17: if (!checkweight(32544,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32544,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Shuriken
		case 18: if (!checkweight(32545,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32545,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Katar
		case 19: if (!checkweight(32546,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32546,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Pistol
		case 20: if (!checkweight(32547,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32547,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Rifle
		case 21: if (!checkweight(32548,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32548,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Shotgun
		case 22: if (!checkweight(32549,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32549,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Grenade Launcher
		case 23: if (!checkweight(32550,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32550,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Gatling Cannon
		case 24: if (!checkweight(32560,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32560,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Training Manual
		case 25: if (!checkweight(32561,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32561,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Book of Shadow
		case 26: if (!checkweight(32562,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem2 32562,1,1,10,0,0,0,0,0; delitem getarg(0),getarg(1); goto Promo_End;	// Zeta Empress Book of the Moon
	}
Empress_Promo:
	next;
	mes @header$;
	mes "Please choose your Zeta Empress Gear (1).";
	switch(select("Zeta Empress Armor:Zeta Empress Manteau:Zeta Empress Boots:Zeta Empress Shield")) {
			case 1: if (!checkweight(32551,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32551,1; delitem getarg(0),getarg(1); goto Promo_End; //Empress Armor
			case 2: if (!checkweight(32552,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32552,1; delitem getarg(0),getarg(1); goto Promo_End; //Empress Manteau
			case 3: if (!checkweight(32553,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32553,1; delitem getarg(0),getarg(1); goto Promo_End; //Empress Boots
			case 4: if (!checkweight(32554,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32554,1; delitem getarg(0),getarg(1); goto Promo_End; //Empress Shield
	}
Promo_End:
	next;
	mes @header$;
	mes "Thank you for considering donating to "+$server_name$+", ^0000FF"+ strcharinfo(0) +"^000000.";
	mes "Good luck and have fun playing!";
	dispbottom "[Donation Promo] You have received your Donation Promo.",0x00ff00;
	close;
}
//============================================================
function	script	200_Donation_Box	{
	set @header$,"[^FF8000 200USD Donation Promo ^000000]";
	mes @header$;
	mes "Please choose your (1) Promotional Item.";
	switch(select("Flames of Valhalla and Wings of Thrud:Shadow of Valhalla and Wings of Hrist:Mysteries of Valhalla and Wings of Gondul:Guardian of Valhalla and Wings of Randgrid:Rage of Valhalla and Wings of Sigurd:Gacha Tickets (100)")){
	case 1: if (!checkweight(32518,1,32519,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32518,1; getitem 32519,1; delitem getarg(0),getarg(1); goto Promo_End; //Flames of Valhalla and Wings of Thrud
	case 2: if (!checkweight(32520,1,32521,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32520,1; getitem 32521,1; delitem getarg(0),getarg(1); goto Promo_End; //Shadows of Valhalla and Wings of Hrist
	case 3: if (!checkweight(32522,1,32523,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32522,1; getitem 32523,1; delitem getarg(0),getarg(1); goto Promo_End; //Mysteries of Valhalla and Wings of Gondul
	case 4: if (!checkweight(32524,1,32525,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32524,1; getitem 32525,1; delitem getarg(0),getarg(1); goto Promo_End; //Guardian of Valhalla and Wings of Randgrid
	case 5: if (!checkweight(32526,1,32527,1)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;}
					getitem 32526,1; getitem 32527,1; delitem getarg(0),getarg(1); goto Promo_End; //Rage of Valhalla and Wings of Sigurd
	case 6:	if (!checkweight(30565,100)){ next; mes @header$,"You are Over-weight please store some of your items"; close; end;} 
				getitem 30565,100; delitem getarg(0),getarg(1); goto Promo_End; //Gacha Tickets (100)
	}
Promo_End:
	next;
	mes @header$;
	mes "Thank you for considering donating to "+$server_name$+", ^0000FF"+ strcharinfo(0) +"^000000.";
	mes "Good luck and have fun playing!";
	dispbottom "[Donation Promo] You have received your Donation Promo.",0x00ff00;
	close;
}
//============================================================