//==== DarkRO Scripts ====================================
//= Acitivity Points
//===== By: ================================================== 
//= [GM] Brenth
//===== Current Version: ===================================== 
//= 1.1
//===== Description: ========================================= 
//= Activity & Daily Points
//===== Changelog: =========================================== 
//= Not yet tested for bugs
//= 1.0 Implementation
//= 1.1 Activty/Daily Points Shop [TODO]
//============================================================

-	script	HourlyPoints	-1,{

OnInit:
	.restricted_map$ = "#guild_vs3#guild_vs2#guild_vs1#";
	.npc_name$ = strnpcinfo(0);
	end;
		
OnCheck:
	#online_minute++;
	if (compare("#"+strcharinfo(3)+"#","#"+.restricted_map$+"#")) {
		dispbottom "You cannot earn activity points on this map, Please relog to continue getting activity points. ";
	} else {
		if (checkidle() >= 600) {
			dispbottom "You have been idle for 10 minutes, Please relog to continue getting activity points.";
			end;
		}
		if (#online_minute && #online_minute % 30 == 0) {
			#ACTIVITYPOINTS++;
			dispbottom "You received 1 Activity Points for staying active for 30 minutes continuously.";
			dispbottom "You have total of "+#ACTIVITYPOINTS+" Activity Points.";
		}
	}

OnPCLoginEvent:
	addtimer 60000,.npc_name$+"::OnCheck";
	end;
}
