
-	script	ClawGame::ClawGame	FAKE_NPC,{

L_Retry:
	deletearray(.@rewardSet);
	deletearray(.@rewardQty);
	mes "[ ^FF8000"+strnpcinfo(1)+"^000000 ]";
	mes "In order to play, you must have at least ^009900"+F_InsertComma(.zenyReq)+" Z^000000.";
	if (select("Pay ^009900"+F_InsertComma(.zenyReq)+" Z^000000:Cancel") == 2) close;
	if (Zeny < .zenyReq) {
		next;
		mes "[ ^FF8000"+strnpcinfo(1)+"^000000 ]";
		mes "It seems that you don't have enough zeny to play the game.";
		close;
	}
	if (!checkweight(Knife, 1)) {
		next;
		mes "[ ^FF8000"+strnpcinfo(1)+"^000000 ]";
		mes "It seems you have a";
		mes "fully-packed inventory.";
		mes " ";
		mes "Clean it up first.";
		close;
	}
	next;
	Zeny -= .zenyReq;
	for (.@k=0; .@k<5; .@k++)
		setd("$CLAW_QUOTA_"+.@k, getd("$CLAW_QUOTA_"+.@k) + 1);
	mes "[ ^FF8000"+strnpcinfo(1)+"^000000 ]";
	mes "Control the ^FF0000joystick^000000, good luck and enjoy!";
	for (.@i=0; .@i<.maxAttempts; .@i++) {
		.@j = select("Front:Back:Right:Left");
		if (.@j == rand(1,4))
			.@ClawRate++;
	}
	if (.@ClawRate >= .minScore) {
		copyarray .@rewardSet[0], .rewardSet0[0], getarraysize(.rewardSet0);
		for (.@i=1; .@i<5; .@i++) {
			if (getd("$CLAW_QUOTA_"+.@i) >= .rewardQuota[.@i]) {
				copyarray .@rewardSet[getarraysize(.@rewardSet)], getd(".rewardSet"+.@i+"[0]"), getarraysize(getd(".rewardSet"+.@i+"[0]"));
			}
		}
	} else {
		next;
		emotion e_bzz, 1;
		mes "[ ^FF8000"+strnpcinfo(1)+"^000000 ]";
		mes "The claw failed and dropped";
		if (rand(100) < 66) {
			for (.@i=1; .@i<5; .@i++) {
				copyarray .@rewardSet[getarraysize(.@rewardSet)], getd(".rewardSet"+.@i+"[0]"), getarraysize(getd(".rewardSet"+.@i+"[0]"));
			}
		} else
			copyarray .@rewardSet[0], .rewardSet0[0], getarraysize(.rewardSet0);
		.@idx = rand(getarraysize(.@rewardSet)) / 3;
		.@amt = rand(.@rewardSet[.@idx * 3 + 1], .@rewardSet[.@idx * 3 + 2]);
		mes "the item: ^0000FF"+ (.@amt != 1 ? .@amt+"x ":"") + getitemname(.@rewardSet[.@idx * 3])+"^000000";
		if (select("Try again...:I'm done!") == 2) close;
		next; goto L_Retry;
	}
	if (.@ClawRate >= .minScore) {
		.@idx = rand(getarraysize(.@rewardSet)) / 3;
		.@amt = rand(.@rewardSet[.@idx * 3 + 1], .@rewardSet[.@idx * 3 + 2]);
		.@found = 0;
		for (.@i=0; .@i<5; .@i++) {
			if (!.@found) {
				for (.@j=0; .@j<getarraysize(getd(".rewardSet"+.@i)); .@j+=3) {
					if (getd(".rewardSet"+.@i+"["+.@j+"]") == .@rewardSet[.@idx * 3]) {
						.@found = 1;
						if (.@i > 0) {
							if (.@i > 2) {
								.@rareAnnounce = 1;
								.@rewardDisp$ =  strtolower((.@i == 3 ? "an ":"a ") + .rewardLabel$[.@i] + " item");
							}
							setd("$CLAW_QUOTA_"+.@i, 0);
							break;
						}
					}
				}
			} else break;
		}
		if (.@rareAnnounce) announce "Congratulations, "+strcharinfo(0)+"! You just won "+.@rewardDisp$+", "+ (.@amt != 1 ? .@amt+" ":"") + getitemname(.@rewardSet[.@idx * 3]) + "!", bc_area, 0x00ffff;
		else npctalk strcharinfo(0) + " just won "+ (.@amt != 1 ? .@amt+" ":"") + getitemname(.@rewardSet[.@idx * 3]) + ".";
		if (.@emoteWin) emotion .emoticonWin[rand(getarraysize(.emoticonWin))], 1;
		.@rareAnnounce = .@ClawRate = 0;
		getitem .@rewardSet[.@idx * 3], .@amt;
		next;
		mes "[ ^FF8000"+strnpcinfo(1)+"^000000 ]";
		mes "You just won ^3366FF"+(.@amt != 1 ? .@amt+" ":"") + getitemname(.@rewardSet[.@idx * 3])+"^000000";
		mes "Do you want to try again?";
		if (select("Try again...:I'm done!") == 2) close;
		next; goto L_Retry;
	}

OnWhisperGlobal:
	if (getgroupid() >= 90) {
		dispbottom "Total 'Claw Game' Played: " + $CLAW_QUOTA_0;
		for (.@l=1; .@l<5; .@l++)
		dispbottom "  > " +.rewardPool$[.@l]+ " Reward Pool : " +getd("$CLAW_QUOTA_"+.@l)+ " / " +.rewardQuota[.@l];
	}
	end;

OnInit:
	.zenyReq = 3000000;		// Zeny requirement to play.
	.maxAttempts = 4;		// Max attempts
	.minScore = 3;			// Minimum score

	// Winning Emoticon
	setarray .emoticonWin[0], e_no1, e_heh, e_grat;

	// Reward Set Syntax
	// <item_id>, <min_reward>, <max_reward>
	
	// Reward Set : Normal
	setarray .rewardSet0[0],607, 1, 15,		// Yggdrasilberry
							608, 1, 15,		// Seed_Of_Yggdrasil
							662, 1, 15,		// Inspector_Certificate
							663, 1, 15,		// Korea_Rice_Cake
							678, 1, 15,		// Poison_Bottle
							12028, 1, 10,	// Box_Of_Thunder
							12030, 1, 10,	// Box_Of_Grudge
							12031, 1, 10,	// Sleepy_Box
							12033, 1, 10,	// Box_Of_Sunlight
							12034, 1, 10,	// Painting_Box
							12073, 1, 5,	// Str_Dish08
							12074, 1, 5,	// Str_Dish09
							12078, 1, 5,	// Int_Dish08
							12079, 1, 5,	// Int_Dish09
							12083, 1, 5,	// Vit_Dish08
							12084, 1, 5,	// Vit_Dish09
							12088, 1, 5,	// Agi_Dish08
							12089, 1, 5,	// Agi_Dish09
							12093, 1, 5,	// Dex_Dish08
							12094, 1, 5,	// Dex_Dish09
							12098, 1, 5,	// Luk_Dish08
							12099, 1, 5,	// Luk_Dish09
							12112, 1, 3,	// Tropical_Sograt
							12113, 1, 3,	// Vermilion_The_Beach
							12118, 1, 7,	// Resist_Fire
							12119, 1, 7,	// Resist_Water
							12120, 1, 7,	// Resist_Earth
							12121, 1, 7,	// Resist_Wind
							12215, 1, 7,	// Blessing_10_Scroll
							12216, 1, 7,	// Inc_Agi_10_Scroll
							12122, 1, 5,	// Sesame_Pastry
							12124, 1, 5,	// Rainbow_Cake
							12142, 1, 5,	// Magic_Book
							12320, 1, 5,	// Pineapple_Juice
							12321, 1, 5,	// Spicy_Sandwich
							12303, 1, 5,	// Water_Of_Blessing
							12338, 1, 5,	// Grilled_Corn
							14509, 1, 7,	// Light_Center_Pot
							14510, 1, 7,	// Light_Awakening_Pot
							14511, 1, 7,	// Light_Berserk_Pot
							14525, 1, 5,	// Chewy_Ricecake
							14526, 1, 5,	// Oriental_Pastry
							14546, 1, 10,	// Fire_Cracker_Love
							14549, 1, 10;	// Fire_Cracker_Bday

	// Reward Set : Rare
	setarray .rewardSet1[0],
							12075, 1, 3,	// Str_Dish10
							12080, 1, 3,	// Int_Dish10
							12085, 1, 3,	// Vit_Dish10
							12090, 1, 3,	// Agi_Dish10
							12095, 1, 3,	// Dex_Dish10
							12100, 1, 3,	// Luk_Dish10
							12103, 1, 15,	// Bloody_Dead_Branch
							12210, 1, 5,	// Bubble_Gum
							12412, 1, 3,	// HE_Bubble_Gum
							12354, 1, 7,	// Buche_De_Noel
							12414, 1, 10,	// Guarana_Candy
							12424, 1, 7,	// HP_Increase_PotionL
							12427, 1, 7,	// SP_Increase_PotionL
							12436, 1, 10,	// Vitata500
							12437, 1, 10,	// Enrich_Celermine_Juice
							14517, 1, 5,	// CP_Helm_Scroll
							14518, 1, 5,	// CP_Shield_Scroll
							14519, 1, 5,	// CP_Armor_Scroll
							14520, 1, 5,	// CP_Weapon_Scroll
							14521, 1, 5;	// Repair_Scroll

	// Reward Set : Unique
	setarray .rewardSet2[0],12279, 1, 5,	// Undead_Element_Scroll
							12280, 1, 5,	// Holy_Element_Scroll
							12299, 1, 5,	// Mega_Resist_Potion
							14538, 1, 5,	// Glass_Of_Illusion
							14542, 1, 15,	// B_Def_Potion
							14544, 1, 15,	// B_Mdef_Potion
							3100, 1, 10,	// Credit
							3106, 1, 2,		// Strange_Magic_Stone
							3107, 1, 2;		// Costume_Ticket

	// Reward Set : Epic
	setarray .rewardSet3[0],18913, 1, 1,	// Gossip_Raven
							18938, 1, 1,	// Astro_Circle
							18654, 1, 1,	// Hood_Of_Society
							18663, 1, 1,	// Sunglasses_Bball_Hat
							18666, 1, 1;	// CD_In_Mouth

	// Reward Set : Legenadary
	setarray .rewardSet4[0],3103, 1, 1,		// Bronze_ECoin
							2357, 1, 1,		// Valkyrie_Armor
							2115, 1, 1,		// Valkyrja's_Shield
							2421, 1, 1;		// Valkyrie_Shoes

	// Quota for reward sets.
	setarray .rewardQuota[1], 300, 500, 15000, 25000;
	setarray .rewardLabel$[1], "Rare", "Unique", "Epic", "Legenadary";
	end;
}

prontera,146,232,5	duplicate(ClawGame)	Claw Machine#prt	2_DROP_MACHINE
//prt_in_b,146,95,71,1	duplicate(ClawGame)	Claw Machine#prt_in_b	2_DROP_MACHINE
//prt_in_b,100,95,72,5	duplicate(ClawGame)	Claw Machine#prt_in_b	2_DROP_MACHINE
