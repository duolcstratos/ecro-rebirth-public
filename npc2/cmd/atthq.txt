//==== DarkRO Commands =======================================
//= @thq
//===== By: ==================================================
//= Project Herc [vBrenth/fTakano]
//===== Current Version: =====================================
//= 1.0
//===== Description: =========================================
//= Displays current Treasure Hunting Quest.
//===== Changelog: ===========================================
//= Not yet tested for bugs
//= 1.0 - Implementation
//============================================================

-	script	AtTHQ	FAKE_NPC,{
	end;

OnInit:
	setarray .thqset_1$[1], "Lost Old Man", "Master needs his Bow", "The Hit List", "The Sad Widow";
	setarray .thqset_2$[1], "The Strange Letter", "Jur for Jeramiah", "Bee Keepers Hunny", "The Wander Man";
	setarray .thqset_3$[1], "Damn Pixies!", "Package Delivery", "Prontera Culvert", "Trouble at the Coal Mine";
	setarray .thqset_4$[1], "Zombie Attack", "Mystic Wizard", "Aww shoot!", "Emperium";
	setarray .thqset_5$[1], "Savage Land", "Pyramid's Part 1", "Thinking First", "The not so friendly ghost";
	setarray .thqset_6$[1], "Package for thieves", "Pyramid's Part 2", "Special Delivery", "Geffenia";
	setarray .thqset_7$[1], "Apple Juice", "Delivery", "Golden Thief Bug", "Evil Pirates";
	setarray .thqset_8$[1], "Banana Juice", "Another Delivery", "Baphomet!", "My lost Beeds";
	setarray .thqset_9$[1], "Smelly Box", "Sohee's Everywere!", "Moonlight Flower", "Payon Cave";
	setarray .thqset_10$[1], "The Blank Box", "Eddga", "Phreeoni", "Maya";

	setarray .thq_var$[0], "one_qset", "two_qset", "three_qset", "four_qset", "five_qset", "six_qset", "seven_qset", "eight_qset", "nine_qset", "ten_qset";

	bindatcmd("thq", strnpcinfo(NPC_NAME)+"::OnExecute",0,99);
	end;

OnExecute:
	if (#THQ_DELAY > (gettime(GETTIME_YEAR)*12*31*24 + gettime(GETTIME_MONTH)*31*24 + gettime(GETTIME_DAYOFMONTH)*24 + gettime(GETTIME_HOUR)) && !On_Quest) {
		.@hr = #THQ_DELAY - (gettime(GETTIME_YEAR)*12*31*24+gettime(GETTIME_MONTH)*31*24+gettime(GETTIME_DAYOFMONTH)*24+gettime(GETTIME_HOUR));
		message(strcharinfo(PC_NAME), "[Treasure Hunt Quest] You can not get any quest right now! Please wait for "+F_InsertPlural(.@hr, "hour")+".");
		end;
	}

	.@thq = -1;
	for (.@i=0; .@i<getarraysize(.thq_var$); .@i++) {
		if (getd(.thq_var$[.@i]) == 0) continue;
		else {
			.@thq = getd(.thq_var$[.@i]);
			@thq_quest$ = getd(".thqset_"+(.@i+1)+"$["+.@thq+"]");
			break;
		}
	}

	if (.@thq == -1) {
		message(strcharinfo(PC_NAME), "No active Treasure Hunt Quest right now.");
		end;
	}

	if (@thq_quest$ != "") {
		message(strcharinfo(PC_NAME), "[Treasure Hunt Quest] Your current quest is '"+@thq_quest$+"'.");
		end;
	}
	end;
}
