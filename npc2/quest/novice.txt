//==== DarkRO Scripts ========================================
//= Getting Started
//===== By: ==================================================
//= Project Herc [vBrenth/fTakano]
//===== Current Version: =====================================
//= 1.0
//===== Description: =========================================
//= Entrance exam before entering the world of DarkRO.
//===== Changelog: ===========================================
//= Not yet tested for bugs
//= 1.0 - Implementation
//============================================================

new_1-4,20,180,3	script	Master Yoda#N	MASTER_YODA,{
	if (#NOVICE_Q == 1) {
		if (BaseJob == Job_Padawan) {
			setparam SkillPoint, 9;
			getitembound(Blue_Lightsaber, 1, IBT_CHARACTER);
		}
		mes .header$;
		mes "It seems that you already passed the test once! May the Force be with you on your journey!";
		close2;
		warp "prontera",156,180;
		savepoint "prontera",156,190;
		end;
	}

	mes .header$;
	mes "Welcome to " + $server_name$ + "!";
	mes " ";
	mes "To venture into the darkside of Ragnarok, I need you to understand some of our rules.";
	next;
	mes .header$;
	mes "First you have to visit our forum and read our rules, After that i will test your knowledge regarding the DarkRO regulations.";
	next;
	switch(select("Start the Test!:I need to check the Rules first.")) {
		case 1:
			mes .header$;
			mes "Learn the Force and your choice will be right..";
			mes " ";
			mes "Let's begin with the set of questions..";
				.@correct = 0;
			next;
			mes .header$;
			mes "^FF00001st Question:^000000";
			mes "We have broadcasters in major towns. How do people use them?";
			if (select("^777777Anyway they like?^000000", "^777777Trashtalks other people^000000", "For buying, selling & trading", "^777777Advertise another game^000000") == 3)
				.@correct += 1;
			next;
			mes .header$;
			mes "^FF00002nd Question:^000000";
			mes "What are you going todo if you get scammed, hacked or cheated by someone?";
			if (select("^777777Spam the staff!^000000", "Submit a Ticket", "^777777Ask a GM to get your stuff back^000000", "^777777Scam someone too!^000000") == 2)
				.@correct += 1;
			next;
			mes .header$;
			mes "^FF00003rd Question:^000000";
			mes "Which thing is the right to do?";
			if (select("^777777Create Pub near NPC^000000", "^777777Spam foul language^000000", "^777777Spam random chats^000000", "Set-up Vend in the Market") == 4)
				.@correct += 1;
			next;
			mes .header$;
			mes "^FF00004th Question:^000000";
			mes "What is allowed during War of Emperium?";
			if (select("Trading friends", "^777777Dealing enemy players^000000", "^777777Sending friend requests^000000", "^777777Copy a guild name^000000") == 1)
				.@correct += 1;
			next;
			mes .header$;
			mes "^FF00005th Question:^000000";
			mes "What is your purpose in DarkRO?";
			if (select("^777777Annoy other people^000000", "^777777Try to be a GM^000000", "Enjoying the game", "^777777Advertising another server^000000") == 3)
				.@correct += 1;
			next;
			if (.@correct == 5) {
				mes .header$;
				mes "You got all the answer correct, May the force be with you!";
				set #NOVICE_Q, 1;
				close2;
				if (BaseJob == Job_Padawan) {
					setparam SkillPoint, 9;
					getitembound(Blue_Lightsaber, 1, IBT_CHARACTER);
				}
				warp "prontera",156,180;
				savepoint "prontera",156,190;
				announce strcharinfo(0) + " has entered to "+$server_name$+"!",bc_all;
				end;
			}
			mes .header$;
			mes "You didn't answer the questions correctly.";
			mes " ";
			mes "You should read our rules to prevent getting punishments. Anyway good luck!";
			close;
			break;

		case 2:
			mes .header$;
			mes "Wish you luck in your training may the Force be with you.";
			close;
			break;
	}

OnInit:
	.header$ = "[ Master Yoda ]";
	end;
}

prontera	mapflag	nowarpto
prontera	mapflag	nowarp
prontera	mapflag	noteleport
prontera	mapflag	pvp	off
