//==== DarkRO Automated Events ===============================
//= Dice Event
//===== By: ==================================================
//= Project Herc [vBrenth/fTakano]
//===== Current Version: =====================================
//= 1.0
//===== Description: =========================================
//= The last player will receive the reward!
//===== Changelog: ===========================================
//= Not yet tested for bugs
//= 1.0 - Implementation
//============================================================

-	script	DICE	FAKE_NPC,{

OnInit:
	.GRP_ID = 70;			// GroupID to by-pass `nocommand` mapflag.
	.evtname$ = "Dice";		// Event Name
	disablenpc("Box 1#dice");
	disablenpc("Box 2#dice");
	disablenpc("Box 3#dice");
	disablenpc("Box 4#dice");
	end;

OnDiceStart:
	setmapflag("e_dice",MF_NOCOMMAND,.GRP_ID);
	.cnt = 3;
	$EVT_DICE = 2;
	mapannounce("e_dice", "[ "+.evtname$+" ] Welcome to the Dice Event.",bc_all|bc_blue);
	sleep(2000);
	mapannounce("e_dice", "[ "+.evtname$+" ] The rules are simple",bc_all|bc_blue);
	sleep(2000);
	mapannounce("e_dice", "[ "+.evtname$+" ] I'm going to randomly pick a number from 1 to 4.",bc_all|bc_blue);
	sleep(2000);
	mapannounce("e_dice", "[ "+.evtname$+" ] All you need to do is to go to a box that you want.",bc_all|bc_blue);
	sleep(2000);
	mapannounce("e_dice", "[ "+.evtname$+" ] For example, If I rolled a number 4, all players from 1 ~ 3 will be warped out.",bc_all|bc_blue);
	sleep(2000);
	mapannounce("e_dice", "[ "+.evtname$+" ] This event will continue until 1 player is left.",bc_all|bc_blue);
	sleep(2000);
	mapannounce("e_dice", "[ "+.evtname$+" ] Just a reminder, if you're not inside the numbered boxes, you will also be warped out.",bc_all|bc_blue);
	sleep(2000);
	mapannounce("e_dice", "[ "+.evtname$+" ] Let's begin the event...",bc_all|bc_blue);
	sleep(2000);
	donpcevent(strnpcinfo(0)+"::OnDiceCountdown");
	end;

OnDiceCountdown:
	if (getmapusers("e_dice") <= 1) {
		switch($EVT_DICE_ROLL) {
			case 1: .@cnt = getunits(BL_PC, .@unit, false, "e_dice",33,52,41,73); break;
			case 2: .@cnt = getunits(BL_PC, .@unit, false, "e_dice",45,52,53,73); break;
			case 3: .@cnt = getunits(BL_PC, .@unit, false, "e_dice",57,52,65,73); break;
			case 4: .@cnt = getunits(BL_PC, .@unit, false, "e_dice",69,52,77,73); break;
		}
		if (.@cnt == 0) {
			announce("[ "+.evtname$+" ] No winners for this event. Better luck next time.", bc_all|bc_blue);
			if (getmapusers("e_dice") > 0)
				mapwarp("e_dice", "prontera", 155, 165);
		} else {
			sleep(2000);
			if (attachrid(.@unit)) {
				if ($EVT_DICE_POINT > 0) {
					EVENTWINS += $EVT_DICE_POINT;
					#EVENTPOINTS += $EVT_DICE_POINT;
					F_EventPoints($EVT_DICE_POINT);
				}
				if ($EVT_DICE_PRIZE > 0 && $EVT_DICE_PRIZE_AMT > 0)
					getitem $EVT_DICE_PRIZE, $EVT_DICE_PRIZE_AMT;
				$EVT_DICE_WINNER$ = strcharinfo(PC_NAME);
				announce("[ "+.evtname$+" ] "+strcharinfo(PC_NAME)+" has won the event!",bc_all|bc_blue);
				sleep2(2000);
				warp("SavePoint",0,0);
			}
			detachrid();
		}
		stopnpctimer();
		donpcevent(strnpcinfo(0)+"::OnDiceStop");
		end;
	} else
		donpcevent(strnpcinfo(0)+"::OnBoxRegroup");
	enablenpc("Box 1#dice");
	enablenpc("Box 2#dice");
	enablenpc("Box 3#dice");
	enablenpc("Box 4#dice");
	setnpctimer(300000);
	startnpctimer();

OnTimer300000:
	mapannounce("e_dice", "[ "+.evtname$+" ] Generating the number... Please go to the box of the number you want!",bc_all|bc_blue);
	end;

OnTimer320000:
	mapannounce("e_dice", "[ "+.evtname$+" ] Revealing the number in 5.....",bc_all|bc_blue);
	end;

OnTimer321000:
	mapannounce("e_dice", "[ "+.evtname$+" ] Revealing the number in 4....",bc_all|bc_blue);
	end;

OnTimer322000:
	mapannounce("e_dice", "[ "+.evtname$+" ] Revealing the number in 3...",bc_all|bc_blue);
	end;

OnTimer323000:
	mapannounce("e_dice", "[ "+.evtname$+" ] Revealing the number in 2..",bc_all|bc_blue);
	end;

OnTimer324000:
	mapannounce("e_dice", "[ "+.evtname$+" ] Revealing the number in 1.",bc_all|bc_blue);
	end;

OnTimer325000:
	stopnpctimer();
	disablenpc("Box 1#dice");
	disablenpc("Box 2#dice");
	disablenpc("Box 3#dice");
	disablenpc("Box 4#dice");
	donpcevent(strnpcinfo(0)+"::OnRevealDiceRoll");
	end;

OnRevealDiceRoll:
	if (getmapusers("e_dice") > 1) {
		$EVT_DICE_ROLL = rand(1,4);
		mapannounce("e_dice", "[ "+.evtname$+" ] I rolled the number... [ "+$EVT_DICE_ROLL+" ]",bc_all|bc_blue);
		for(.@i=1; .@i<5; .@i++) {
			if (.@i != $EVT_DICE_ROLL)
				donpcevent(strnpcinfo(0)+"::OnBox"+.@i+"Warp");
		}
		areawarp("e_dice",31,76,80,95,"prontera",155,165);
		donpcevent(strnpcinfo(0)+"::OnDiceCountdown");
	} else
		donpcevent(strnpcinfo(0)+"::OnDiceStop");
	end;
	
OnBox1Warp:
	areawarp("e_dice",33,52,41,73,"prontera",155,165);
	end;

OnBox2Warp:
	areawarp("e_dice",45,52,53,73,"prontera",155,165);
	end;

OnBox3Warp:
	areawarp("e_dice",57,52,65,73,"prontera",155,165);
	end;

OnBox4Warp:
	areawarp("e_dice",69,52,77,73,"prontera",155,165);
	end;

OnBoxRegroup:
	mapwarp("e_dice", "e_dice", 54, 85);
	end;

OnDiceStop:
	if (getmapusers("e_dice") > 0)
		mapwarp("e_dice", "prontera", 155, 165);
	$EVT_DICE = 0;
	$EVT_DICE_ROLL = 0;
	removemapflag("e_dice",MF_NOCOMMAND);
	end;
}

e_dice,37,80,4	script	Box 1#dice	HIDDEN_NPC,{
	warp("e_dice", 37, 70);
	close;
}

e_dice,49,80,4	script	Box 2#dice	HIDDEN_NPC,{
	warp("e_dice", 49, 70);
	close;
}

e_dice,61,80,4	script	Box 3#dice	HIDDEN_NPC,{
	warp("e_dice", 61, 70);
	close;
}

e_dice,73,80,4	script	Box 4#dice	HIDDEN_NPC,{
	warp("e_dice", 73, 70);
	close;
}

e_dice	mapflag	nowarp
e_dice	mapflag	nowarpto
e_dice	mapflag	noteleport
e_dice	mapflag	nomemo
e_dice	mapflag	nosave	SavePoint
e_dice	mapflag	noicewall
e_dice	mapflag	noloot
e_dice	mapflag	noexp
e_dice	mapflag	pvp	off
e_dice	mapflag	nodrop
e_dice	mapflag	nobranch
