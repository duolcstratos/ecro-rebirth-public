//==== DarkRO Scripts ========================================
//= Emperium Test Break
//===== By: ==================================================
//= Project Herc [vBrenth/fTakano]
//===== Current Version: =====================================
//= 1.0
//===== Description: =========================================
//= Practice your emperium breaking here.
//===== Changelog: ===========================================
//= Not yet tested for bugs
//= 1.0 - Implementation
//============================================================

function	script	F_EmpBreakTime	{
	.@sec = getarg(0) / 1000;
	.@mil = getarg(0) % 1000;
	
	.@str$ = .@sec + "." + .@mil + "s";
	return .@str$;
}

prontera,162,135,3	script	Emperium Breaker Test	8W_SOLDIER,{

	.@pid = getcharid(CHAR_ID_PARTY);
	.@md_name$ = _("Emperium Break Room");
	
	if (has_instance2("1@rev") >= 0)
		instance_destroy();

	mes .header$;
	mes "Do you want to test how fast you can break the Emperium?";
	if (!instance_check_party(.@pid, 1) && getcharid(CHAR_ID_CHAR) == getpartyleader(.@pid, 2)) {
		mes "You need a party to enter the room.";
	}
	next;
	switch(select(
		(getcharid(CHAR_ID_CHAR) == getpartyleader(.@pid, 2) && has_instance2("1@rev") < 0 && instance_check_party(.@pid, 1)) ? sprintf(_$("Enter ^6b9c00<%s>^000000"), .@md_name$) : "",
		"Top ^FF000010^000000 Breaker Ladder",
		"Cancel"
	)) {
		default:
			F_CloseCutin();
			break;
		
		case 1:
			.@instance = instance_create(.@md_name$, .@pid);
			if (.@instance >= 0) {
				if (instance_attachmap("1@rev", .@instance) == "") {
					instance_destroy(.@instance);
					F_CloseCutin();
				}
				instance_set_timeout(0, 0, .@instance);
				instance_init(.@instance);
			}
			next;
			mes .header$;
			mes "You will be warped inside.";
			close2();
			warp("1@rev", 112, 45);
			break;
			
		case 2:
			.@qry$ = "SELECT ch.`name`, p_break_time "
				+ "FROM `empbreak_test` ebt INNER JOIN `char` ch ON ch.`char_id`=ebt.`char_id` "
				+ "ORDER BY p_break_time ASC LIMIT 0, 10";
			.@rs = query_sql(.@qry$, .@name_r$, .@break_r);
			if (!.@rs) {
				mes .header$;
				mes "No records found.";
				close();
			} else {
				mes .header$;
				for (.@i=0; .@i<.@rs; .@i++)
					mes sprintf("^777777%d) ^0000FF%s ^777777(^FF0000%s^777777)^000000", .@i + 1, .@name_r$[.@i], F_EmpBreakTime(.@break_r[.@i]));
				close();
			}
			break;
	}
	
OnInit:
	.header$ = "[ Emperium Breaker ]";
	bindatcmd("removeempbreak", strnpcinfo(NPC_NAME)+"::OnRemoveBreak",90,99);
	bindatcmd("empbreakreset", strnpcinfo(NPC_NAME)+"::OnEmpBreakRankReset",97,99);
	
	.@qry$ = "CREATE TABLE IF NOT EXISTS `empbreak_test` ( "
		+ "`char_id` INT(11) NOT NULL DEFAULT 0, "
		+ "`p_break_time` INT(11) NOT NULL DEFAULT 0, "
		+ "PRIMARY KEY (`char_id`) "
		+ ") COLLATE='utf8mb4_general_ci' ENGINE=InnoDB;";
		
	query_sql(.@qry$);
	end;

OnRemoveBreak:
	instance_destroy();
	end;
	
OnEmpBreakRankReset:
	query_sql("TRUNCATE empbreak_test");
	message(strcharinfo(PC_NAME), "Emperium Break Test Ranking has been reset.");
	end;
}

1@rev,112,45,0	script	#EmpBreak_Intro	FAKE_NPC,1,1,{
	end;
	
OnTouch:
	disablenpc(instance_npcname("#EmpBreak_Intro"));
	query_sql("SELECT `p_break_time` FROM empbreak_test WHERE `char_id`="+getcharid(CHAR_ID_CHAR), @p_break_time);
	mapannounce(instance_mapname("1@rev"), _("<Emperium Break> Buff Now!"), bc_npc|bc_map, C_YELLOW);
	initnpctimer();
	end;
	
OnTimer6000:
	mapannounce(instance_mapname("1@rev"), _("<Emperium Break> Here it comes!"), bc_npc|bc_map, C_YELLOW);
	end;
	
OnTimer7000:
	mapannounce(instance_mapname("1@rev"), _("<Emperium Break> 3!!!"), bc_npc|bc_map, C_YELLOW);
	end;
	
OnTimer8000:
	mapannounce(instance_mapname("1@rev"), _("<Emperium Break> 2!!!"), bc_npc|bc_map, C_YELLOW);
	end;
	
OnTimer9000:
	mapannounce(instance_mapname("1@rev"), _("<Emperium Break> 1!!!"), bc_npc|bc_map, C_YELLOW);
	end;
	
OnTimer10000:
	stopnpctimer();
	mapannounce(instance_mapname("1@rev"), _("<Emperium Break> GO!"), bc_npc|bc_map, C_YELLOW);
	donpcevent(instance_npcname("#EmpBreak_Main")+"::OnEmpSpawn");
	end;
}

1@rev,0,0,0	script	#EmpBreak_Main	FAKE_NPC,{
	end;
	
OnEmpSpawn:
	monster(instance_mapname("1@rev"), 111, 47, _("--en--"), EMPELIUM_, 1, instance_npcname(strnpcinfo(NPC_NAME))+"::OnMyMobDead");
	initnpctimer();
	end;
	
OnMyMobDead:
	'time = @last_break_time = getnpctimer(0, instance_npcname("#EmpBreak_Main"));
	stopnpctimer();
	if ( !@p_break_time ) {
		@p_break_time = 'time;
		.@qry$ = "INSERT INTO empbreak_test(`char_id`, `p_break_time`) VALUES('"+getcharid(CHAR_ID_CHAR)+"', '"+@p_break_time+"')";
		query_sql(.@qry$);
		message(strcharinfo(PC_NAME), "You have set your first personal record!");
	}
	if ( 'time < @p_break_time ) {
		@p_break_time = 'time;
		.@qry$ = "UPDATE empbreak_test SET `p_break_time` = "+@p_break_time+" WHERE `char_id`="+getcharid(CHAR_ID_CHAR);
		query_sql(.@qry$);
		message(strcharinfo(PC_NAME), "You have broken your personal record!");
	}
	mapannounce(instance_mapname("1@rev"), _("You have broken the emperium in ["+F_EmpBreakTime(@last_break_time)+"]."), bc_npc|bc_map, C_YELLOW);
	sleep2(2000);
	@last_break_time = @p_break_time = 0;
	instance_destroy();
	warp("SavePoint",0,0);
	end;

OnPCLogoutEvent:
	instance_destroy();
	end;
}

// Mapflags
1@rev	mapflag	pvp	off
1@rev	mapflag	gvg
1@rev	mapflag	nobranch
1@rev	mapflag	nowarpto
1@rev	mapflag	nowarp
1@rev	mapflag	nomemo