//==== DarkRO Scripts ========================================
//= Gold Room NPC
//===== By: ==================================================
//= Project Herc [vBrenth/fTakano]
//===== Current Version: =====================================
//= 1.3
//===== Description: =========================================
//= Zeny farming grounds for newbies.
//===== Changelog: ===========================================
//= Not yet tested for bugs
//= 1.0  - Implementation
//= 1.1  - Fixed variables on OnInit label.
//= 1.2  - Golden Birds will depend on users inside the room.
//= 1.3  - Added Bot Check System
//============================================================

prontera,165,143,4	script	Gold Room	1_M_SIGN1,{

	cutin("mets_alpha", 2);
	mes .header$;
	mes "Golds. There are plenty back in my days. Ehem~ I have a place where you can find great amount of golds from those rare Golden Birds.";
	next;
	mes .header$;
	mes "Killing those birds will have you gain a random amount of Gold Points or GP which you may trade into a real Gold!";
	next;
	mes .header$;
	mes "Just a heads up though, there are guardians that are lurking around the map and they are strong!";
	mes " ";
	if (.penaltyRate == 0)
		mes "^FF0000All of your points will lose when you die!^000000";
	else
		mes "^FF0000You will lose "+(100 - .penaltyRate)+"% of your total earned points.^000000.";
	next;
	switch(select("Enter Gold Room:Exchange Points:Check Points")){
	case 1:
		mes .header$;
		if (#ENTRY_GOLD > 2)
			mes "I need ^FF0000"+F_InsertComma(.entryFee)+" Zeny^000000, So I will be able to bring you into the sacred place.";
		else
			mes "You still have ^FF3030"+(3-#ENTRY_GOLD)+"^000000 free entry pass, are you going to enter to this sacred place?";
		switch(select((#ENTRY_GOLD < 3 ? "Yes, I will.":"Here, I got your Zeny.")+":Cancel")){
		case 1:
			next;
			if (gethominfo(2) != "null") {
				mes .header$;
				mes "Please make your homunculus rest first before entering the gold room.";
				F_CloseCutin();
			}
			if (Zeny < .entryFee && #ENTRY_GOLD > 2) {
				mes .header$;
				mes "You need to pay "+F_InsertComma(.entryFee)+" zeny.";
				F_CloseCutin();
			}
			if (#ENTRY_GOLD > 2) Zeny -= .entryFee;
			else #ENTRY_GOLD++;
			undisguise;
			unfakename;
			sc_end SC_SACRIFICE;
			sc_end SC_EDP;
			sc_end SC_AUTOSPELL;
			warp .mapName$, rand(152, 154), rand(152, 154);
			end;
		case 2:
			next;
			mes .header$;
			mes "Okay, If you ever think of being rich, talk to me again.";
			F_CloseCutin();
		}
	case 2:
		if (getsecurity()) {
			F_CloseCutin();
			F_GetSecurity();
			break;
		}
		getinventorylist();
		mes .header$;
		mes "You have "+#GoldPoints+" Gold Points!";
		mes " ";
		mes "How much of your Gold Points do you wish to exchange in for real "+getitemname(.goldId)+"?";
		mes " ";
		mes "1x GP is equivalent to "+.goldRate+" "+getitemname(.goldId)+".";
		next;
		if (#GoldPoints >= .goldRate) {
			input .@Amount,0, #GoldPoints;
			if (.@Amount == 0) {
				mes .header$;
				mes "I'm willing to exchange your gold points next time you talk to me.";
				F_CloseCutin();
			}
			set .@Calculation,(.@Amount/.goldRate);
			if (!checkweight(.goldId, .@Calculation) || (@inventorylist_count == 100 && !countitem(.goldId))) {
				mes .header$;
				mes "Sorry, you have exceeded the weight limit.";
				F_CloseCutin();
			}
			if ((countitem(.goldId) + .@Calculation) > 30000) {
				mes .header$;
				mes "Sorry, you can't exceed from the maximum gold bar count.";
				F_CloseCutin();
			}
			mes .header$;
			set #GoldPoints,#GoldPoints - (.@Calculation * .goldRate);
			getitem .goldId, .@Calculation;
			mes "Gained "+.@Calculation+" x "+getitemname(.goldId)+"!";
			mes " ";
			mes "There you go!";
		}
		F_CloseCutin();
		case 3:
			next;
			mes .header$;
			mes "You currently have "+#GoldPoints+" Gold Points!";
			F_CloseCutin();
	}

OnInit:
	.header$ = "[ Gold Room ]";
	.entryFee = 250000;		// Entry fee.
	.mapName$ = "goldroom";	// Map for the Gold Room
	.penaltyRate = 25; 		// Set this to zero to erase all earned gold points
	.goldId = Gold;			// Self-explanatory : Item ID of Gold.
	.goldRate = 1;			// Exchange rate of Gold Points
	.idleTime = 1; 			// Number in minutes;
	.maxPartyCount = 5;		// Max Party count for the party to get share points. Set this to 0 to disable.
	.ptSuccessRate = 5;		// Success rate to get the points for party sharing.
	.birdSpawn = 4;			// Amount of bird spawn depending on users inside the gold room.
	.minSpawnCap = 500;		// Minimum spawn cap of Golden Birds.
	.maxSpawnCap = 750;		// Maximum spawn cap of Golden Birds.
	.userCntTrig = 5;		// Trigger check for the increment of Golden Birds.
	.hasZeroPoints = true;	// Gives zero points to players who killed Golden Birds.
	setcell .mapName$, 144, 163, 163, 144, cell_basilica, 1;
	donpcevent(strnpcinfo(0)+"::OnSpawnBirds");
	end;

OnSpawnBirds:
	if (.GOLD_MOBS != .minSpawnCap) .GOLD_MOBS = .minSpawnCap;
	.GOLD_USERS = getmapusers(.mapName$) - getareausers(.mapName$, 144, 163, 163, 144);
	.GOLD_MOBS_ADD = ((.GOLD_USERS - .userCntTrig > 0)  ? (.GOLD_USERS - .userCntTrig) : 0) * .birdSpawn;
	.GOLD_MOBS_LIMIT = .GOLD_MOBS + .GOLD_MOBS_ADD;
	if (.GOLD_MOBS_LIMIT > .maxSpawnCap) .GOLD_MOBS_LIMIT = .maxSpawnCap;

	for (.@i=0; .@i<.GOLD_MOBS; .@i++)
		monster .mapName$, 0, 0, "Golden Bird", GOLDENBIRD, 1, strnpcinfo(0)+"::OnMobKilled";

	for (.@i=.GOLD_MOBS; .@i<.GOLD_MOBS_LIMIT; .@i++)
		monster .mapName$, 0, 0, "Golden Bird", GOLDENBIRD, 1, strnpcinfo(0)+"::OnMobKilled";

	.GOLD_MOBS_TOTAL = mobcount(.mapName$, strnpcinfo(0)+"::OnMobKilled");
	initnpctimer();
	end;

OnTimer10000:
	stopnpctimer();
	.GOLD_USERS = getmapusers(.mapName$) - getareausers(.mapName$, 144, 163, 163, 144);
	.GOLD_MOBS_ADD = ((.GOLD_USERS - .userCntTrig > 0)  ? (.GOLD_USERS - .userCntTrig) : 0) * .birdSpawn;
	.GOLD_MOBS_LIMIT = .GOLD_MOBS + .GOLD_MOBS_ADD;
	if (.GOLD_MOBS_LIMIT > .maxSpawnCap) .GOLD_MOBS_LIMIT = .maxSpawnCap;

	if (.GOLD_MOBS_LIMIT >= .GOLD_MOBS_TOTAL) {
		for (.@i=.GOLD_MOBS_TOTAL; .@i<.GOLD_MOBS_LIMIT; .@i++)
			monster .mapName$, 0, 0, "Golden Bird", GOLDENBIRD, 1, strnpcinfo(0)+"::OnMobKilled";
	}

	.GOLD_MOBS_TOTAL = mobcount(.mapName$, strnpcinfo(0)+"::OnMobKilled");
	initnpctimer();
	end;

OnMobKilled:
	if (playerattached()) {
		if (rand(100) < 20 && .hasZeroPoints) {
			set #GoldPoints,#GoldPoints + 0;
			dispbottom "[Gold Room] You have killed a Golden Bird but no points were rewarded.";
		} else {
			set #GoldPoints,#GoldPoints + (rand(1,3) * ($BONUSPT_GOLD ? 2:1));
			dispbottom "[Gold Room] You currently have "+F_CommaPlural(#GoldPoints, "Gold Point")+".";
			if (.maxPartyCount > 0) {
				getpartymember(getcharid(CHAR_ID_PARTY), 2);
				.@src = getcharid(CHAR_ID_ACCOUNT);
				for (.@i=0; .@i<$@partymembercount; .@i++) {
					if (attachrid($@partymemberaid[.@i])) {
						if (strcharinfo(PC_MAP) == .mapName$
							&& isloggedin(getcharid(CHAR_ID_ACCOUNT), getcharid(CHAR_ID_CHAR)))
						.@partyCount++;
					}
				}
				if (.@partyCount <= .maxPartyCount) {
					for (.@i=0; .@i<$@partymembercount; .@i++) {
						if (attachrid($@partymemberaid[.@i])) {
							if (strcharinfo(PC_MAP) == .mapName$
								&& .@src != getcharid(CHAR_ID_ACCOUNT)
								&& isloggedin(getcharid(CHAR_ID_ACCOUNT), getcharid(CHAR_ID_CHAR))
								&& Hp > 0 && checkidle() < .idleTime * 60) {
								.@rate = rand(100);
								if (.@rate < .ptSuccessRate) .@pt = 1 * ($BONUSPT_GOLD ? 2:1);
								else .@pt = 0;
								if (.@pt > 0) {
									#GoldPoints += .@pt;
									dispbottom "[Gold Room] "+rid2name(.@src)+" killed a Golden Bird. You currently have " +#GoldPoints +" gold points.";
								}
							}
						}
					}
				}
			}
		}
	}
	if (.GOLD_MOBS_LIMIT > mobcount(.mapName$, strnpcinfo(0)+"::OnMobKilled"))
		monster .mapName$, 0, 0, "Golden Bird", GOLDENBIRD, 1, strnpcinfo(0)+"::OnMobKilled";
	/* Commented for Bot Checker
		if (@BOTCHECK_CONFIRM) end;
		if (#BOT_MOB_LIMIT_20001 == 0)
			#BOT_MOB_LIMIT_20001 = 500 + rand(1, 25);
		if (#BOT_MOB_KILL_20001 < #BOT_MOB_LIMIT_20001) {
			#BOT_MOB_KILL_20001++;
			end;
		} else if (#BOT_MOB_KILL_20001 == #BOT_MOB_LIMIT_20001 && !#BOTCHECK) {
			#BOTCHECK = true;
			#BOT_MOB_CLEAR = GOLDENBIRD;
			//doevent("Sys_BotChecker::OnBotCheckTrigger");
			doevent("Sys_BotCheck::OnBotCheckTrigger");
			end;
		} else end;
	*/
	end;

OnPCDieEvent:
	if (strcharinfo(3) == .mapName$) {
		set #GoldPoints, #GoldPoints * .penaltyRate / 100;
		if (.penaltyRate > 0) {
			dispbottom "[Gold Room] You lose "+(100 - .penaltyRate)+"% of your gold points.";
			dispbottom "[Gold Room] Total points left : "+#GoldPoints+" gold points";
		} else
			dispbottom "[Gold Room] You lose all of your gold points.";
		#BOT_MOB_KILL_20001 = #BOT_MOB_LIMIT_20001 = #BOT_MOB_CLEAR = 0;
	}
	end;
}

// Guardians
goldroom,0,0,0,0	monster	Mine Guardian	20002,15,0,0,0

// Warps
goldroom,128,154,0	warp	goldroom-1	1,1,goldroom,23,142
goldroom,136,136,0	warp	goldroom-2	1,1,goldroom,24,24
goldroom,136,172,0	warp	goldroom-3	1,1,goldroom,24,284
goldroom,153,128,0	warp	goldroom-4	1,1,goldroom,144,23
goldroom,153,180,0	warp	goldroom-5	1,1,goldroom,144,284
goldroom,172,135,0	warp	goldroom-6	1,1,goldroom,284,24
goldroom,172,172,0	warp	goldroom-7	1,1,goldroom,284,284
goldroom,180,154,0	warp	goldroom-8	1,1,goldroom,284,164

goldroom,23,164,0	warp	goldroom-13	3,3,goldroom,154,154
goldroom,43,24,0	warp	goldroom-14	3,3,goldroom,154,154
goldroom,43,284,0	warp	goldroom-15	3,3,goldroom,154,154
goldroom,164,23,0	warp	goldroom-16	3,3,goldroom,154,154
goldroom,144,262,0	warp	goldroom-17	3,3,goldroom,154,154
goldroom,263,24,0	warp	goldroom-18	3,3,goldroom,154,154
goldroom,263,284,0	warp	goldroom-19	3,3,goldroom,154,154
goldroom,263,164,0	warp	goldroom-20	3,3,goldroom,154,154

// Exit
goldroom,158,158,3	script	Guard#GR1::GREXIT	8W_SOLDIER,{

	set .header$,"[ Guard ]";
	mes .header$;
	mes "Do you want to leave Gold Room now?";
	next;
	switch(select("Yes:Cancel")) {
	case 1:
		warp "SavePoint",0,0;
		break;
	case 2:
		mes .header$;
		mes "Goodbye!";
		F_CloseCutin();
	}
}

goldroom,66,25,3	duplicate(GREXIT)	Guard#GR2	8W_SOLDIER
goldroom,106,154,3	duplicate(GREXIT)	Guard#GR3	8W_SOLDIER
goldroom,201,129,3	duplicate(GREXIT)	Guard#GR4	8W_SOLDIER
goldroom,290,129,3	duplicate(GREXIT)	Guard#GR5	8W_SOLDIER
goldroom,74,265,3	duplicate(GREXIT)	Guard#GR6	8W_SOLDIER
goldroom,177,193,3	duplicate(GREXIT)	Guard#GR7	8W_SOLDIER
goldroom,241,281,3	duplicate(GREXIT)	Guard#GR8	8W_SOLDIER
goldroom,130,193,3	duplicate(GREXIT)	Guard#GR9	8W_SOLDIER

// Mapflags
goldroom	mapflag	pk_area
goldroom	mapflag	allowks
goldroom	mapflag	nomemo
goldroom	mapflag	nomobloot
goldroom	mapflag	nobranch
goldroom	mapflag	noexp
goldroom	mapflag	nodrop
goldroom	mapflag	nochat
goldroom	mapflag	novending
goldroom	mapflag	nowarpto
goldroom	mapflag	nowarp
goldroom	mapflag	noskill
goldroom	mapflag	nopenalty
goldroom	mapflag	nobranch
goldroom	mapflag	noreturn
goldroom	mapflag	pvp_noguild
goldroom	mapflag	pvp_noparty
